(function() {
  'use strict';

  var buildReportsPath = '../buildreports/';

  module.exports = function(karma) {
    karma.set({

      basePath: '',

      files: [
        '../node_modules/chai/chai.js',
        '../node_modules/chai-as-promised/lib/chai-as-promised.js',
        '../node_modules/sinon/pkg/sinon.js',
        '../node_modules/sinon-chai/lib/sinon-chai.js',
        '../node_modules/timekeeper/lib/timekeeper.js',

        // bower:js
        '../bower_components/jquery/dist/jquery.js',
        '../bower_components/modernizr/modernizr.js',
        '../bower_components/lodash/dist/lodash.compat.js',
        '../bower_components/angular/angular.js',
        '../bower_components/angular-resource/angular-resource.js',
        '../bower_components/angular-cookies/angular-cookies.js',
        '../bower_components/angular-sanitize/angular-sanitize.js',
        '../bower_components/angular-route/angular-route.js',
        '../bower_components/angular-animate/angular-animate.js',
        '../bower_components/angular-filter/dist/angular-filter.min.js',
        '../bower_components/bootstrap/dist/js/bootstrap.js',
        '../bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        '../bower_components/jquery.ui/ui/jquery.ui.core.js',
        '../bower_components/jquery.ui/ui/jquery.ui.widget.js',
        '../bower_components/jquery.ui/ui/jquery.ui.mouse.js',
        '../bower_components/jquery.ui/ui/jquery.ui.draggable.js',
        '../bower_components/jquery.easing/js/jquery.easing.js',
        '../bower_components/angular-toggle-switch/angular-toggle-switch.js',
        '../bower_components/angular-xeditable/dist/js/xeditable.js',
        '../bower_components/iCheck/icheck.min.js',
        '../bower_components/bootbox.js/bootbox.js',
        '../bower_components/stepy/lib/jquery.stepy.js',
        '../bower_components/moment/moment.js',
        '../bower_components/enquire/dist/enquire.js',
        '../bower_components/restangular/dist/restangular.js',
        '../bower_components/angular-ui-router/release/angular-ui-router.js',
        '../bower_components/angular-poller/angular-poller.min.js',
        '../bower_components/javascript-equal-height-responsive-rows/grids.min.js',
        '../bower_components/fullcalendar/dist/fullcalendar.js',
        '../bower_components/jscrollpane/script/jquery.mousewheel.js',
        '../bower_components/jscrollpane/script/mwheelIntent.js',
        '../bower_components/jscrollpane/script/jquery.jscrollpane.js',
        '../bower_components/signature_pad/signature_pad.js',
        '../bower_components/raven-js/dist/raven.js',
        '../bower_components/angular-raven/angular-raven.js',
        '../bower_components/ua-parser-js/src/ua-parser.js',
        '../bower_components/ng-file-upload/ng-file-upload.js',
        '../bower_components/intl-tel-input/build/js/intlTelInput.min.js',
        '../bower_components/intl-tel-input/lib/libphonenumber/build/utils.js',
        '../bower_components/Jcrop/js/Jcrop.js',
        '../bower_components/ng-tags-input/ng-tags-input.min.js',
        '../bower_components/angular-aria/angular-aria.js',
        '../bower_components/angular-messages/angular-messages.js',
        '../bower_components/angular-material/angular-material.js',
        '../bower_components/highlightjs/highlight.pack.js',
        '../bower_components/angular-highlightjs/build/angular-highlightjs.js',
        // endbower

        '../bower_components/angular-mocks/angular-mocks.js',

        '../app/assets/plugins/angular-ui-calendar/calendar.js',

        '../.tmp/templates/templates.js',
        '../app/src/**/module.js',
        '../app/src/**/!(*.spec).js',
        'unit/**/*.spec.js',
        'unit/mocks/*.js',
      ],

      exclude: [
        '../app/src/theme/_unused/**/*.js'
      ],

      preprocessors: {
        '../app/src/**/!(*.spec).js': 'coverage'
      },

      coverageReporter: {
        dir : buildReportsPath + 'coverage/',
        includeAllSources: true,
        reporters: [
          {type: 'html'},
          {type: 'text-summary'},
          //{type: 'text'}
        ],
        watermarks: {
          statements: [75, 90],
          functions: [75, 90],
          branches: [75, 90],
          lines: [75, 90],
        },
        //check: {
        //  global: {
        //    statements: 90,
        //    branches: 90,
        //    functions: 90,
        //    lines: 90,
        //  },
        //  each: {
        //    statements: 90,
        //    branches: 90,
        //    functions: 90,
        //    lines: 90,
        //  },
        //  overrides: {
        //    // file : { statements: %, branches: %, functions: %, lines: %},
        //  }
        //}
      },

      htmlReporter: {
        outputFile: buildReportsPath + 'unit_tests/index.html'
      },

      junitReporter: {
        outputFile: '../' + buildReportsPath + 'unit_tests/test-results.xml',
        suite: ''
      },

      autoWatch: false,

      frameworks: ['mocha'],

      port: 9876,
      runnerPort: 9100,

      colors: true,

      logLevel: karma.LOG_INFO,

      browsers: ['PhantomJS'],

      plugins: [
        'karma-mocha',
        'karma-phantomjs-launcher',
        'karma-coverage',
        'karma-htmlfile-reporter',
        'karma-junit-reporter'
      ],

      reporters: ['progress', 'coverage', 'html', 'junit'],

      client: {
        captureConsole: true
      }
    });
  };
})();
