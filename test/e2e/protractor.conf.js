(function() {
  'use strict';
  var moment = require('../../bower_components/moment/moment.js');
  var fs = require('node-fs');
  var path = require('path');
  var SpecReporter = require('jasmine-spec-reporter');

  exports.config = {
    seleniumServerJar: '../../node_modules/protractor/selenium/selenium-server-standalone-2.47.1.jar',
    chromeDriver: '../../node_modules/protractor/selenium/chromedriver',
    suites: {
      all: []
    },
    framework: 'jasmine2',
    capabilities: {
      browserName: 'chrome',
      // uncomment to set up default chrome download folder
      chromeOptions: {
        // Get rid of --ignore-certificate yellow warning
        args: ['--no-sandbox', '--test-type=browser'],
        prefs: {
          download: {
            prompt_for_download: false,
            default_directory: 'buildreports/e2e/downloads'
          },
        },
      }
    },
    jasmineNodeOpts: {
      defaultTimeoutInterval: 600000,
      isVerbose: true
    },
    allScriptsTimeout: 360000,
    onCleanUp: function() {
      if (process.argv.indexOf('--keepDownloads') === -1) {
        if (fs.existsSync('buildreports/e2e/downloads')) {
          fs.readdirSync('buildreports/e2e/downloads').forEach(function(file) {
            var filePath = 'buildreports/e2e/downloads/' + file;
            console.log('Deleting file: ' + filePath);
            fs.unlinkSync(filePath);
          });
        }
      }
    },
    onPrepare: function() {
      jasmine.getEnv().addReporter(new SpecReporter({
        displaySpecDuration: true, // display duration for each spec, failed or not
        displayFailuresSummary: false, // display summary of all failures after execution
        displayPendingSummary: false,  // display summary of all pending specs after execution
      }));
      browser.manage().window().setSize(1200, 800);
      /* The following two lines are temporarily removed until some way to bail fast only on
         the "ensure not running on prod" test
      */
      // require('jasmine-bail-fast');
      // jasmine.getEnv().bailFast();
      // Uncomment the below section to have the browser pause on failure rather than
      // close immediately.

      // jasmine.getEnv().addReporter({
      //   reportSpecResults: function(spec) {
      //     if (!spec.results().passed()) {
      //       printBrowserLogs();
      //       // browser.pause();
      //     }
      //   }
      // });

      //Sets up a screen shots folder that will store any explicitly invoked screen shots.
      if (!fs.existsSync('buildreports/e2e/screenshots')) {
        fs.mkdirSync('buildreports/e2e/screenshots/', '0777', true);
      }
      //Sets up a download folder
      if (!fs.existsSync('buildreports/e2e/downloads')) {
        fs.mkdirSync('buildreports/e2e/downloads/', '0777', true);
      }

      // Global Objects / Variables
      global.userStore = {};

      global.requireE2E = function(exportPath) {
        return require('./exports/' + exportPath + '.export.js');
      };
      global.getAssetPath = function(assetPath) {
        return path.resolve(__dirname, './assets/' + assetPath);
      };
      // Global Methods
      global.moment = moment;
      global.validateDisplay = function(element, displayed) {
        expect(element.isDisplayed()).toEqual(displayed);
      };
      global.validateText = function(element, displayedText) {
        expect(element.getText()).toEqual(displayedText);
      };
      global.saveData = function(btnName, parent) {
        btnName = btnName || 'btnSave';
        if (!parent) {
          return element(by.name(btnName)).click();
        }
        return parent.element(by.name(btnName)).click();
      };
      global.login = function(username, password) {
        browser.get('/');
        element(by.model('userLogin.username')).sendKeys(username);
        element(by.model('userLogin.password')).sendKeys(password);
        element(by.xpath('//button[@type="submit"]')).click();
      };
      global.logout = function(logoutConfirm) {
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.elementToBeClickable(element(by.className('fa-power-off')).element(by.xpath('..'))), 5000)
        .then(function() {
          element(by.className('fa-power-off')).element(by.xpath('..')).click()
          .then(function() {
            if (typeof logoutConfirm === 'function') {
              logoutConfirm();
            }
            expect(browser.getCurrentUrl()).toMatch(/login/);
          });
        });
      };
      global.generatePassword = function(len) {
        var genLength = len || 16;
        var idx = 0;
        var passwd = '';
        while (idx < genLength) {
          var character = 33 + Math.floor(Math.random() * 93);
          passwd += String.fromCharCode(character);
          idx++;
        }
        passwd += 'aB3!';
        return passwd;
      };
      global.generateUser = function(store, id) {
        var usernameTemplate = 'employiiTestUser-';
        var timestamp = moment.utc().valueOf();
        var username = usernameTemplate + timestamp;
        var user = {
          username: username,
          password: generatePassword()
        };
        if (store) {
          global.userStore[id] = user;
        }

        return user;
      };
      global.returnToDashboard = function() {
        var dashboardIcon = element(by.className('fa-home'));
        var dashboardButton = dashboardIcon.element(by.xpath('..'));
        dashboardButton.click();
        browser.waitForAngular();
        expect(element(by.xpath('//h1')).getText()).toEqual('Dashboard');
      };
      global.signupCompany = function(store, id, payrollEnabled, timeEnabled) {
        browser.get('/#/signup');
        var user = generateUser(store, id);
        console.log('Created user \'' + id + '\':');
        console.log('userStore = {');
        console.log('    \'' + id + '\': {');
        console.log('        username: \'' + user['username'] + '\',');
        console.log('        password: \'' + user['password'] + '\'');
        console.log('    }');
        console.log('};');
        element(by.model('signupData.user.first_name')).sendKeys('Kode41');
        element(by.model('signupData.user.last_name')).sendKeys('Test-User-One');
        element(by.model('signupData.user.email')).sendKeys('eto@kode41.com');
        element(by.model('signupData.user.username')).sendKeys(user.username);
        element(by.model('signupData.user.password')).sendKeys(user.password);
        element(by.model('signupData.user.password_confirmation')).sendKeys(user.password);
        element(by.model('signupData.legal_name')).sendKeys('Kode41 Test Company One');
        if (payrollEnabled) {
          element(by.name('payrollLabel')).click();
        }
        if (timeEnabled) {
          element(by.name('timeLabel')).click();
        }
        element(by.xpath('//form[@name="signup_form"]//button[@type="submit"]')).click();
        expect(browser.getCurrentUrl()).toMatch(/#\/admin\/dashboard/);
        return user;
      };
      global.waitForNotification = function() {
        var pNotify = element(by.className('ui-pnotify'));
        browser.wait(function() {
          return pNotify.isPresent();
        }, 60000, 'No notification was created');
      };
      global.clearNotification = function(removeFromDom, timeout) {
        var pNotify = element(by.className('ui-pnotify'));
        timeout = timeout || 1000;

        browser.wait(function() {
          return pNotify.isPresent();
        }, timeout, 'There was no notification.');

        if (removeFromDom) {
          browser.executeScript('arguments[0].remove()', pNotify.getWebElement());
          expect(pNotify.isPresent()).toEqual(false);
        } else {
          pNotify.allowAnimations(false);
          var el = pNotify.element(by.className('ui-pnotify-closer'));
          browser.actions().mouseMove(el).perform()
          .then(el.click);
        }

        browser.wait(function() {
          return pNotify.isPresent().then(function(present) {
            return !present;
          });
        }, timeout, 'The notification was not removed.');
      };
      global.takeScreenshot = function(filename) {
        var file = path.resolve('buildreports/e2e/screenshots/' + filename + '.png');
        browser.takeScreenshot().then(function(png) {
          console.log('Writing file ' + file);
          fs.writeFileSync(file, png, {encoding: 'base64'}, console.log);
        });
      };
      global.printServerLogs = function() {
        browser.manage().logs().get('server').then(function(logs) {
          console.log('Server Logs: ', require('util').inspect(logs));
        });
      };
      global.printDriverLogs = function() {
        browser.manage().logs().get('driver').then(function(logs) {
          console.log('Driver Logs: ', require('util').inspect(logs));
        });
      };
      global.printClientLogs = function() {
        browser.manage().logs().get('client').then(function(logs) {
          console.log('Client Logs: ', require('util').inspect(logs));
        });
      };
      global.printBrowserLogs = function() {
        browser.manage().logs().get('browser').then(function(logs) {
          console.log('Incoming logs: ', require('util').inspect(logs));
        });
      };
      global.patchCompany = function(patchData) {
        return browser.executeAsyncScript(function(patchData, callback) {
          var rest = angular.element('body').injector().get('rest');
          var profile = angular.element('body').injector().get('profile');

          var company = profile.get().company.id;
          return rest.companies.patch(company, patchData)
            .then(function(response) {
              callback(response);
            });
        }, patchData);
      };
    }
  };
})();
