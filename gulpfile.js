'use strict';

var gulp    = require('gulp');
var concat  = require('gulp-concat');
var debug   = require('gulp-debug');
var jshint  = require('gulp-jshint');
var jshintStylish = require('jshint-stylish');
var jscs    = require('gulp-jscs');
var jscsStylish = require('gulp-jscs-stylish');
var wiredep = require('wiredep').stream;
var ngtemplate = require('gulp-ng-templates');
var karma   = require('karma').Server;
var htmlmin = require('gulp-htmlmin');
var es      = require('event-stream');
var sourcemaps = require('gulp-sourcemaps');
var usemin = require('gulp-usemin');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var ngmin = require('gulp-ngmin');
var rev   = require('gulp-rev');
var annotate = require('gulp-ng-annotate');
var fs    = require('fs');
var replace = require('gulp-replace');
var gulpRegexp = require('gulp-regexp-sourcemaps');
var less = require('gulp-less');
var path = require('path');
var copy = require('gulp-copy');
var del = require('del');
var prefixer = require('gulp-autoprefixer');
var watch = require('gulp-watch');
var batch = require('gulp-batch');
var livereload = require('gulp-livereload');
var http = require('http');
var st = require('st');
var protractor = require('gulp-protractor').protractor;
var exit = require('gulp-exit');
var taskRevisionNumber = 'v1.0.1.json';
var validateSMap = require('sourcemap-validator');
var assert = require('assert');

var appConfig = {
  src: 'app/src/',
  assets: 'app/assets/',
  dist: 'dist',
  reports: 'buildreports',
  test: 'test'
};

var argv = require('yargs').argv;
var markers = {
  env: /https:\/\/staging\.payenroll\.com/,
  sentry: {
    dsn: /%SENTRY_DSN%/,
    bypass: /["']%SENTRY_BYPASS%["']/
  },
  maps: /%GM_API_KEY%/,
  features: /["']%FEATURE_SETUP%["']/,
  taskDependencyURI: /%TASK_DEPENDENCIES%/,
  thirdPartyProduction: /<div ng-if="false" production-only-scripts>/
};
var replacements = {};

setReplacements(argv.prod ? 'prod' : argv.dev ? 'dev' : argv.demo ? 'demo' : 'staging');

// HELPER FUNCTIONS

function generateExternalReferences(scripts) {
  var referenceLinks = '';
  for (var i = 0; i < scripts.length; i++) {
    referenceLinks += '<script src="scripts/' + scripts[i] + '"></script>'
  }
  return referenceLinks
}

function setReplacements(env) {
  switch (env) {
    case 'dev':
      // dev environment
      replacements.env = 'http://localhost:9000';
      replacements.maps = '';
      replacements.features = fs.readFileSync('config/features/dev.json', 'utf-8');
      replacements.e2eBase = 'http://localhost:9000';
      replacements.thirdPartyProduction = '';
      break;
    default:
      // staging
      replacements.env = 'http://localhost:9000';
      replacements.maps = '&key=AIzaSyCf85bZWebgzbGFXLjzOYjEOVtj3fX8Bk8';
      replacements.features = fs.readFileSync('config/features/staging.json', 'utf-8');
      replacements.e2eBase = 'http://localhost:9000';
      replacements.thirdPartyProduction = '';
  }
}

function getOurJavaScriptStream() {
  return gulp.src([appConfig.src + '/**/*.js', '!' + appConfig.src + 'extras{,/**}', '!' + appConfig.src + 'theme{,/**}', appConfig.test + '/**/*.js']);
}

function get3rdPartyJavaScriptStream() {
  return gulp.src([appConfig.src + 'theme{,/**}', '!' + appConfig.src + 'theme/_unused{,/**}']);
}

function getTemplateStream() {
  return gulp.src(['app/**/*.html', '!' + appConfig.src + 'theme{,/**}', '!app/*.html', '!app/assets/**/*.html'])
  .pipe(htmlmin({collapseWhitespace: false, collapseBooleanAttributes: true, removeComments: false,
    removeCommentsFromCDATA: false, removeOptionalTags: false}))
  .pipe(ngtemplate({module: 'k41-templates', standalone: true, filename: 'templates.js'}))
}

function getCompiledLessStream() {
  return gulp.src('app/assets/less/styles.less')
  .pipe(less({
    paths: [
      path.join(__dirname, 'app/assets/less', 'app/assets/less/bootstrap')
    ]
  }));
}

function updateKarmaConf() {
  gulp.src(['test/karma.conf.js'])
  .pipe(wiredep({
    exclude: [
      'requirejs',
      'mocha',
      'es5-shim',
      'json3'
    ],
    fileTypes: {
      js: {
        block: /(([\s\t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
        detect: {
          js: /'(.*\.js)'/gi
        },
        replace: {
          js: '\'{{filePath}}\','
        }
      }
    }
  }))
  .pipe(gulp.dest('test/'));
}

function updateIndexFile() {
  return gulp.src(['app/index.html'])
  .pipe(wiredep({
    exclude: ['requirejs', 'mocha', 'es5-shim', 'json3'],
    ignorePath: 'app/'
  }))
  .pipe(gulp.dest('app/'));
}

// END: HELPER FUNCTIONS

// TASKS

gulp.task('clean', function(cb) {
  del(['dist', 'buildreports', '.tmp'], cb);
});

gulp.task('lint', function() {
  if (argv.skipLint) {
    return;
  }
  return getOurJavaScriptStream()
    .pipe(jshint())
    .pipe(jscs())
    .on('error', function() {})
    .pipe(jscsStylish.combineWithHintResults())
    .pipe(jshint.reporter(jshintStylish))
  // .pipe(jshint.reporter('fail'));
});

gulp.task('writeTemplates', function() {
  return getTemplateStream().pipe(gulp.dest('.tmp/templates/'));
});

gulp.task('test', ['writeTemplates'], function(done) {
  if (argv.skipTests) {
    console.log('Skipping tests....');
    done();
  } else {
    console.log('Testing...');
    updateKarmaConf();
    var karmaOpts = {
      configFile: __dirname + '/test/karma.conf.js',
      singleRun: true
    };
    if (argv.noCoverage) {
      karmaOpts.reporters = ['progress', 'html', 'junit'];
    } else if (argv.simpleTests) {
      karmaOpts.reporters = ['dots'];
    }
    new karma(karmaOpts, done).start();
  }
})

gulp.task('compileIE8', function() {
  return gulp.src([
    './app/assets/plugins/charts-flot/excanvas.min.js',
    './app/assets/plugins/misc/placeholdr.js',
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/es5-shim/es5-shim.min.js',
    'bower_components/json3/lib/json3.min.js'])
  .pipe(concat('scripts/ie8.js'))
  .pipe(uglify())
  .pipe(gulp.dest('dist'));
});

gulp.task('compileScripts', ['test', 'compileIE8'], function() {
  return updateIndexFile()
  // All replacements performed on the index.html file have to be completed before the usemin action.
  .pipe(usemin({
    css: [minifyCss(), prefixer({browsers: ['last 1 version']}), 'concat', rev()],
    ourCss: [
      less({paths: [path.join(__dirname, 'app/assets/less', 'app/assets/less/bootstrap')]}),
      minifyCss(),
      prefixer({browsers: ['last 1 version']}),
      'concat',
      rev()
    ],
    js: ['concat', /*annotate(), uglify(),*/ rev()],
    employiiJS: [
      // Annotate has to form the initial sourcemaps, so that concat would be aware of mapping columns.
      // The set order is strict.
      sourcemaps.init(),
      // annotate(),
      'concat',
      // uglify(),
      rev(),
      sourcemaps.write('./')]
  }))
  // Replacements on the compiled files that take into account the generated sourcemaps.
  .pipe(gulpRegexp(markers.env, replacements.env))
  .pipe(gulpRegexp(markers.maps, replacements.maps))
  .pipe(gulpRegexp(markers.features, replacements.features))
  .pipe(gulpRegexp(markers.taskDependencyURI, replacements.taskDependencyURI))
  .pipe(gulpRegexp(markers.thirdPartyProduction, replacements.thirdPartyProduction))
  .pipe(gulp.dest('dist'));
});

gulp.task('validateSourcemaps', ['compileScripts'], function(done) {
  var mapRegex = /(.*\.js)\.map/;
  fs.readdir('dist/scripts/', function(err, filenames) {
    for (var idx in filenames) {
      var mapFilename = filenames[idx];
      if (mapRegex.test(mapFilename)) {
        var scriptFilename = mapFilename.match(mapRegex)[1];
        console.log('Validating:', mapFilename, 'for', scriptFilename);

        var scriptFile = fs.readFileSync(path.join('dist/scripts', scriptFilename));
        var mapJSON = fs.readFileSync(path.join('dist/scripts', mapFilename));
        assert.doesNotThrow(function() {
          validateSMap(scriptFile, mapJSON.toString());
          console.log('Valid!');
        }, 'The sourcemap is not valid');
      }
    }
  });
  done();
});

gulp.task('copyFiles', function() {
  gulp.src([
    //'app/assets/fonts/font-awesome/fonts/**',
    'app/assets/fonts/glyphicons/font/**',
    'app/assets/{css,img}/**',
    'app/.htaccess',
  ], {base: './app/'})
  .pipe(gulp.dest('dist/'));
  gulp.src([
    'bower_components/font-awesome/fonts/**',
  ], {base: './bower_components/font-awesome'})
  .pipe(gulp.dest('dist/assets/'));
  gulp.src(['app/*.{ico,png,txt,htaccess,html}'], {base: 'app/'})
  .pipe(gulp.dest('dist/'));
  // Copy the phone number util library files
  gulp.src(['bower_components/intl-tel-input/lib/libphonenumber/build/utils.js'],
    {base: 'bower_components/intl-tel-input/lib/libphonenumber/build/'})
  .pipe(gulp.dest('dist/scripts/libphonenumber'));
  gulp.src(['bower_components/intl-tel-input/build/img/*.png'],
    {base: 'bower_components/intl-tel-input/build/'})
  .pipe(gulp.dest('dist/assets'));
  // Copy task dependencies for dev
  if (argv.dev) {
    gulp.src(['config/tasks/devTaskDependencies-' + taskRevisionNumber])
    .pipe(gulp.dest('dist/'));
  }
  // Copy any prod extras.
  if (argv.prod) {
    gulp.src(['app/src/extras/*']).pipe(gulp.dest('dist/scripts/'));
  }
});

gulp.task('help', function() {
  console.log();
  console.log('Employii Web Application Compilation Help');
  console.log('=========================================');
  console.log('Usage: ');
  console.log('gulp [version] [skipTests]');
  console.log();
  console.log('Where:');
  console.log('\t[version] is one of:  --dev, --prod or --staging');
  console.log('\t\tIf none of these are provided, --staging will be used');
  console.log('\t[skipTests] If --skipTests is provided, no tests will be run');
  console.log();
});

gulp.task('default', ['clean'], function() {
  console.log('Executing default task');
  gulp.start(['lint', 'test', 'validateSourcemaps', 'copyFiles']);
});

gulp.task('monitor', function() {
  if (argv.unit) {
    gulp.start(['lint', 'test']);
    gulp.watch(['app/src/**/*.js', 'test/unit/**/*.spec.js'], ['test']);
  }
});

gulp.task('server', ['default'], function(done) {
  if (argv.prod) {
    console.error('`server` is NOT supported against production. Exiting!');
    return -1;
  }
  http.createServer(
    st({path: __dirname + '/dist', index: 'index.html', cache: false})
  ).listen(9000, '0.0.0.0', done);
});

gulp.task('serveCurrent', function(done) {
  http.createServer(
    st({path: __dirname + '/dist', index: 'index.html', cache: false})
  ).listen(9000, '0.0.0.0', done);
});

gulp.task('serve', ['server'], function() {
  if (argv.prod) {
    console.error('`serve` is NOT supported against production. Exiting!');
    return -1;
  }
  gulp.watch(['app/src/**/*.{js,html}', 'app/assets/**/*.{less,css}'], ['default']);
  livereload.listen({basepath: 'dist'});
  gulp.watch(['dist/**']).on('changed', livereload.changed);
});

gulp.task('e2e', function() {
  if (argv.prod) {
    console.error('E2E is NOT supported against production.  Exiting!');
    return -1;
  }
  var args = ['--baseUrl', replacements.e2eBase];
  if (argv.keepDownloads) {
    args.push('--keepDownloads');
  }
  var suite = 'all';
  if (argv.suites) {
    suite = argv.suites;
  }
  args.push('--suite', suite);
  console.log('Protractor arguments: ', args);
  gulp.src([])
  .pipe(protractor({
    configFile: 'test/e2e/protractor.conf.js',
    args: args
  }))
  .on('error', function(e) { throw e; })
  .pipe(exit());
});

// END: TASKS

module.exports = gulp;
