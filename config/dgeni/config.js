var path = require('canonical-path');

var Package = require('dgeni').Package;

module.exports = new Package('Employii', [
  require('dgeni-packages/ngdoc')
])

.config(function(log, readFilesProcessor, templateFinder, writeFilesProcessor) {
  log.level = 'warn';
  readFilesProcessor.basePath = path.resolve(__dirname, '../../');
  readFilesProcessor.sourceFiles = [
    {
      include: 'app/src/**/*.js',
      exclude: 'app/src/theme/_unused/**/*.js',
      basePath: 'app/src'
    }
  ];

  templateFinder.templateFolders.unshift(path.resolve(__dirname, 'templates'));
  //templateFinder.templatePatterns.unshift('common.template.html');
  writeFilesProcessor.outputFolder = 'buildreports/docs';
})

.config(function(getLinkInfo) {
  getLinkInfo.relativeLinks = true;
})

.config(function(computeIdsProcessor, createDocMessage, getAliases) {
  computeIdsProcessor.idTemplates.push({
    docTypes: ['controller' ],
    idTemplate: 'module:${module}.${docType}:${name}',
    getAliases: getAliases
  });
})

.config(function(computePathsProcessor, createDocMessage) {
  computePathsProcessor.pathTemplates.push({
    docTypes: ['controller'],
    pathTemplate: '${area}/${module}/${docType}/${name}',
    outputPathTemplate: 'partials/${area}/${module}/${docType}/${name}.html'
  });
});
