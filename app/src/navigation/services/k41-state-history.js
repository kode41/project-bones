(function() {
  'use strict';

  var mod = angular.module('k41-navigation');

  mod.factory('k41StateHistory', ['$state', '$stateParams', function($state, $stateParams) {
    var stateHistory = {};
    var stateStack = JSON.parse(sessionStorage.getItem('k41.stateStack'));
    if (!stateStack) {
      stateStack = [{state: $state.current.name, params: $stateParams, data: {}}];
    }

    /**
     * @ngdoc method
     * @name  k41StateHistory#commitToSession
     * @description
     * The function will save the current stateStack to the sessionStorage.
     */
    function commitToSession() {
      sessionStorage.setItem('k41.stateStack', JSON.stringify(stateStack));
    }

    stateHistory.commitToSession = commitToSession;

    /**
     * @ngdoc method
     * @name  k41StateHistory#go
     * @param  {string} state   - Name of the state that's being navigated to.
     * @param  {Object} params  - State parameters.
     * @param  {Object} data    - Miscellaneous state data that needs to be persisted to sessionStorage for the future
     * state.
     * @param  {Object} options - Additional arguments for the $state.go method.
     * @description
     * The function will push a new state to the stack, save the stack to sessionStorage, and navigate to the state.
     */
    stateHistory.go = function(state, params, data, options) {
      stateStack.push({state: state, params: params, data: data || {}});
      commitToSession();
      $state.go(state, params, options);
    };

    /**
     * @ngdoc method
     * @name  k41StateHistory#back
     * @param  {Object} options - Additional arguments for the $state.go method.
     * @description
     * The function will remove the current state from the stack. Navigate to the one before it, and update
     * sessionStorage.
     */
    stateHistory.back = function(options) {
      stateStack.pop();
      var oldState = stateHistory.currentState();
      if (oldState) {
        commitToSession();
        $state.go(oldState.state, oldState.params, options);
      }
    };

    /**
     * @ngdoc method
     * @name  k41StateHistory#currentState
     * @return {Object} - current state object.
     * @description
     * The function will return the current state object, or null if the stack is empty;
     */
    stateHistory.currentState = function() {
      if (stateStack.length > 0) {
        return stateStack[stateStack.length - 1];
      } else {
        return null;
      }
    };

    /**
     * @ngdoc method
     * @name  k41StateHistory#initStack
     * @param  {Object} data - Miscellaneous data for the current state.
     * @description
     * The function will initiate the state stack with the current state and it's parameters.
     */
    stateHistory.initStack = function(data) {
      stateStack = [{state: $state.current.name, params: $stateParams, data: data || {}}];
      commitToSession();
    };

    /**
     * @ngdoc method
     * @name k41StateHistory#clearStack
     * @description
     * The function will save an empty stack to sessionStorage.
     */
    stateHistory.clearStack = function() {
      stateStack = [];
      commitToSession();
    };

    return stateHistory;
  }]);
})();
