(function() {
  'use strict';
  /**
   * @ngdoc overview
   * @name  k41Navigation
   * @description
   * The k41-navigation module is responsible for functionality regarding page navigation.
   */
  angular.module('k41-navigation', []);
})();
