'use strict';
/**
 * @ngdoc controller
 * @name DashboardController
 * @module k41Admin
 * @description
 * A controller for user's dashboard, both employee and admin side.
 */
angular.module('k41-admin').controller('DashboardController',
['$scope', '$q', '$timeout', '$state', 'profile', 'calendar', '$rootScope',
'errorManager', '$controller',
function($scope, $q, $timeout, $state, profile, calendar, $rootScope,
  errorManager, $controller) {
  var self = this;
  self.modalMethods = {};
  angular.extend(this, $controller('ErrorWatchingController',
    {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

  $scope.admin = !_.contains($state.current.data.userRoles, 'user');

  // Calendar
  // --------
  function loadCalendar() {
    $('#calendar').fullCalendar($scope.calendarConfig);
  }


  // Calls the loadCalendar function, and creates a new calendar object once the document is ready.
  calendar.createCalendarCallback($(document), loadCalendar);
  // calendarDataLoaded event gets fired every time one of the promises returned by the EventSourceFunctions
  // get resolved.

  var calendarDataLoaded = {
    timeOffRequest : false,
    payrollSchedule : false
  };

  $rootScope.$on('calendarDataLoaded', function(data, event) {
    calendarDataLoaded[event] = true;
    if (calendarDataLoaded.timeOffRequest && calendarDataLoaded.payrollSchedule) {
      $scope.context.isLoading = false;
    }
  });

}]);
