'use strict';

/**
 * @ngdoc module
 * @name emAdmin
 * @description
 *
 * The `k41-admin` module provides code for admin level users. This includes features such as:
 * - Staff Management
 * - Payroll
 * - Time Management
 * - Account Setting
 * - etc.
 */
angular.module('k41-admin', ['angular.filter']);
