(function() {
  'use strict';
  var module = angular.module('k41-bootstrap');

  module.controller('FormsController',
    ['$scope', '$rootScope', '$controller', 'patterns', 'errorManager', 'utils', 'editable', 'enums', 'collections',
    '$bootbox',
    function($scope, $rootScope, $controller, patterns, errorManager, utils, editable, enums, collections, $bootbox) {
      var self = this; //Controller as syntax
      angular.extend(this, $controller('ErrorWatchingController',
        {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));
      $scope.data = {};
      $scope.enums = enums;
      $scope.selectData = [{id: 1, name: 'First Val'}, {id: 2, name: 'First Val'}];
      $scope.patterns = patterns;
      $scope.data.date_time = new Date();
      self.client = {};
      var data = [{id: 1, name: 'Client 1'}, {id: 2, name: 'Client 2'}];
      self.clients = [];
      $scope.totalItems = self.clients.length;
      $scope.itemsPerPage = 3;
      $scope.currentPage = 1;
      editable.initErrors($scope, 'hasError');

      $scope.initForm = function($form, status) {
        editable.initEditableForm($scope.data, $scope.hasError, $form, status);
      };
      self.save = function(form) {
        $scope.submitted = true;
        if (form.$valid) {
          utils.successMsg('Successfully saved form data.');
          form.$setPristine();
        } else {
          utils.errorMsg('Please fill form correct.');
        }
      };
      self.reset = function(form) {
        $scope.data = {};
        form.$setPristine();
      };


      collections.initPaging(self);

      self.getClientsList = function() {
        self.clients = data.slice(3 * ($scope.currentPage - 1), 3 * ($scope.currentPage));
      };

      self.getClientsList();


      self.addClient = function() {
        self.client.id = self.clients.length + 1;
        var check = _.any(data, function(cl) { return cl.name === self.client.name; });
        if (!check) {
          data.push(self.client);
          self.client = {};
          $scope.clientError = null;
        } else {
          $scope.clientError = 'Client with name ' + self.client.name + ' already exist!';
        }
        $scope.clientsForm.$setPristine();
        $scope.totalItems = data.length;
        self.getClientsList();
      };

      self.clearClient = function() {
        self.client = {};
        $scope.clientError = null;
        $scope.clientsForm.$setPristine();
      };

      self.removeClient = function(item, idx) {
        $bootbox.confirm({
          title: 'Remove Client',
          message: '<p>Do you really want to remove client <strong>' +
          item.name + '</strong>?</p>',
          buttons: {
            cancel: {
              label: 'Cancel',
              className: 'btn-lg btn-danger'
            },
            confirm: {
              label: 'Yes Remove',
              className: 'btn-lg btn-success'
            }
          },
          callback: function(confirmed) {
            if (confirmed) {
              data.splice(idx, 1);
              $scope.totalItems = data.length;
              self.getClientsList();
              utils.successMsg('Client ' + item.name + 'removed');
            }
          }
        });
      };

    }]);
})();
