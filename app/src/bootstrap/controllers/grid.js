(function() {
  'use strict';
  var module = angular.module('k41-bootstrap');

  module.controller('BootstrapGridController',
    ['$scope', '$rootScope',
    function($scope) {

      $scope.toggle = function(position) {
        $scope['show' + position] = !$scope['show' + position];
      };

    }]);
})();
