(function() {
  'use strict';
  var module = angular.module('k41-bootstrap');

  module.controller('InlineController',
    ['$scope', '$rootScope', '$controller', 'patterns', 'errorManager', 'utils',
    function($scope, $rootScope, $controller, patterns, errorManager, utils) {
      var self = this; //Controller as syntax
      angular.extend(this, $controller('ErrorWatchingController',
        {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

      self.getList = function() {
        self.items = [
          {id: 1, name: 'Item1', description: 'Description 1'},
          {id: 2, name: 'Item2', description: 'Description 2'}
         ];
        return self.items;
      };
      self.getList();

      self.addItem = function() {
        var item  = {
          _edit: true,
          edit: {}
        };
        self.items.push(item);
      };

      self.editItem = function(item) {
        item.edit = angular.copy(item);
        item._edit = true;
      };
      self.cancelItem = function(item, idx) {
        if (!item.id) {
          self.items.splice(idx, 1);
        }
        item._edit = false;
      };

      self.saveItem = function(item, form) {
        item._submitted = true;
        if (form.$valid) {
          item._edit = false;
          form.$setPristine();
          item.description = item.edit.description;
          item.name = item.edit.name;
          utils.successMsg('Successfully saved item!');
        }
      };

    }]);
})();
