(function() {
  'use strict';
  var mod = angular.module('k41-error-handling');

  /**
   * @ngdoc directive
   * @name  emServerErrorDisplay
   * @module emErrorHandling
   * @description
   * The k41-server-error-display directive provides the error display div for
   * server errors to the user.
   */
  mod.directive('emServerErrorDisplay', function() {
    return {
      restrict: 'E',
      templateUrl: 'src/error-handling/directives/server-error-display.html',
      scope: {
        serverError: '=',
        title: '@'
      }
    };
  });

  mod.directive('emCustomServerErrorDisplay', function() {
    return {
      restrict: 'E',
      templateUrl: 'src/error-handling/directives/custom-server-error-display.html',
      transclude: true,
      scope: {
        serverError: '=',
        title: '@'
      }
    };
  });

  /**
   * @ngdoc directive
   * @name  emDataErrorDisplay
   * @module emErrorHandling
   * @description
   * The em-data-error-display directive provides the error display div for
   * errors caused by data issues to the user.
   */
  mod.directive('emDataErrorDisplay', function() {
    return {
      restrict: 'E',
      templateUrl: 'src/error-handling/directives/data-error-display.html',
      scope: {
        errors: '=',
        warnings: '=',
        errorTitle: '@errorTitle',
        warningTitle: '@warningTitle'
      }
    };
  });
})();
