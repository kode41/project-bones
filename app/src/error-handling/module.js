(function() {
  'use strict';

  /**
   * @ngdoc module
   * @name emErrorHandling
   * @description
   *
   * The k41-error-handling module provides error handling capabilities and hooks for the
   * remainder of the web application.  These capabilities include various services,
   * controllers, directives, etc. to present the user with pertinent information when
   * an error occurs and/or to direct information to the Kode41 Sentry.
   *
   */
  angular.module('k41-error-handling', ['ngRaven']);
})();
