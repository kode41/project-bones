/* globals Raven */
(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name  errorManager
   * @module emErrorHandling
   * @description
   * The errorManager is the central point for handling errors in the Kode41 web application.  The
   * functionality provided by this service includes the tracking of the current user, error event ID
   * reports and enabling the logging of individual events to multiple services (e.g. console and Sentry).
   *
   * @return {errorManager} - The singleton errorManager instance for the Kode41 Web Application.
   */
  angular.module('k41-error-handling').factory('errorManager', ['$raven', '$log', '$rootScope',
    function($raven, $log, $rootScope) {
    var em = {
      userSet: false,
      defaultUser: {
        company: 'Unknown Company',
        email: 'Unknown E-mail',
        id: 'Unknown User ID'
      },
      ignoredExceptions: [400, 401, 403, 404]
    };

    /**
     * @ngdoc method
     * @name  errorManager#captureServerError
     * @param  {Dict} errorData - The pertinent information for a server error.
     * @description
     * Captures the information for an error from the backing server and transmits
     * the result as an event with the payload from `errorData`.
     *
     * The result will be emitted from $rootScope using the event id comprised
     * of the post hashbang server location, up to the last slash before an id
     * value if one exists, plus ServerError.
     *
     * For example, if the post-hashbang url was:
     * _employee/details/5/_, the key would be _employee/details/ServerError_.
     */
    em.captureServerError = function(errorData) {
      if (_.indexOf(em.ignoredExceptions, errorData.status) < 0) {
        $raven.captureException('Kode41 REST Exception:', {extra: errorData});
      }
      if (errorData.status > 500) {
        errorData.incidentNumber = Raven.lastEventId();
      }
      var regex = /^.*\/v2\/+((?:[a-zA-Z_]+\d*\/*)+)\d*.*/g;
      var route = errorData.url.replace(regex, '$1');
      var broadcastKey = route + 'ServerError';
      $rootScope.$emit(broadcastKey, errorData);
    };

    /**
     * @ngdoc method
     * @name  errorManager#clearErrors
     * @param  {string} path - The post-hashbang path to use as a key.
     * @description
     * Broadcasts a false value to the registered listeners for the _`path`ServerError_
     * event, clearing the error values.
     */
    em.clearErrors = function(path) {
      var broadcastKey = path + 'ServerError';
      $rootScope.$emit(broadcastKey, false);
    };

    /**
     * @ngdoc method
     * @name  errorManager#broadcastListener
     * @param  {Event} event - The AngularJS event triggering the broadcast.
     * @param  {profile} newProfile - The updated profile which caused _event_ to be fired.
     *
     * @description
     * Listens for updates to the user who is logged in and sets the user context for
     * error reporting.
     */
    em.broadcastListener = function(event, newProfile) {
      if (event.name === 'profileChanged') {
        em.setUser(newProfile);
      }
    };

    /**
     * @ngdoc method
     * @name  errorManager#setUser
     * @description
     * Sets the relevant user information for error tracking / logging to facilitate debugging and maintenance.
     * @param {UserProfile} profile - The profile of the user to set information on.
     */
    em.setUser = function(profile) {
      var user = em.defaultUser;
      if (profile && profile.loggedIn) {
        user = {
          company: profile.company.legal_name,
          email: profile.user.email,
          id: profile.user.id
        };
        em.userSet = true;
      }
      $raven.setUserContext(user);
    };

    /**
     * @ngdoc method
     * @name  errorManager#clearUser
     * @description
     * Clears the user information for future error logs.
     */
    em.clearUser = function() {
      $raven.setUserContext();
      em.userSet = false;
    };

    return em;
  }]);
})();
