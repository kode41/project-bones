(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name  responseHandler
   * @module emErrorHandling
   * @param  {$rootScope} $rootScope - The root scope for the application
   * @param  {$raven} $raven - The angular-raven object.
   * @description
   * The responesHandler service is responsible for handling error responses from the Kode41
   * back-end and dispatching error messages to the Sentry system.
   * @return {Service} Returns the responseHandler service singleton.
   */
  angular.module('k41-error-handling').factory('responseHandler', ['$rootScope', '$raven', '$location', 'errorManager',
  '$log',
  function($rootScope, $raven, $location, errorManager, $log) {
    var responseHandlerService = {};

    /**
     * @ngdoc method
     * @name  responseHandler#restangularErrorInterceptor
     * @param  {HttpResponse} response        - The HttpResponse to log information about.
     * @description
     * Receives incoming HTTP error responses and dispatches the necessary information to Sentry
     * when appropriate.
     * @return {boolean}                 - True if the error should continue to percolate up, false otherwise.
     */
    responseHandlerService.restangularErrorInterceptor = function(response) {
      $log.debug('Incoming REST Error Response:', response);
      var errorData = {
        method: response.config ? response.config.method : 'unknown',
        url: response.config ? response.config.url : 'unknown',
        status: response.status,
        statusText: response.statusText
        //data: response.data.non_field_errors
      };
      if (response.data && response.status < 500) {
        var errorInfo = [];
        for (var idx in response.data) {
          if (idx !== 'non_field_errors' && idx !== 'detail') {
            for (var errIdx in response.data[idx]) {
              errorInfo.push(idx + ': ' + response.data[idx][errIdx]);
            }
          }
        }
        if (response.data.non_field_errors) {
          for (var nfeIdx in response.data.non_field_errors) {
            errorInfo.push(response.data.non_field_errors[nfeIdx]);
          }
        } else if (response.data.detail) {
          if (_.isArray(response.data.detail) || _.isObject(response.data.detail)) {
            for (var detIdx in response.data.detail) {
              errorInfo.push(response.data.detail[detIdx]);
            }
          } else {
            errorInfo.push(response.data.detail);
          }
        }
        errorData.data = errorInfo;
      }
      errorManager.captureServerError(errorData);
      if ($rootScope.logOutIfUnauthorized(response)) {
        return false; // User was not authorized & was logged out.
      }
      return true; // Unknown exception reason... logged and allowed to bubble up.
    };

    /**
     * @ngdoc method
     * @name  responseHandler#sendToSentry
     * @param  {response} response - The incoming HTTP response.
     * @return {boolean} - True if the response data should be logged to Sentry, false otherwise.
     */
    responseHandlerService.sendToSentry = function(response) {
      return response.status > 299;
    };

    return responseHandlerService;
  }]);
})();
