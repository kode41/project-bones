(function() {
  'use strict';

  // Create an exception handler decorator which logs to Sentry on an AngularJS error.
  angular.module('k41-error-handling').config(function($provide) {
    $provide.decorator('$exceptionHandler', ['$raven', '$delegate',
     function($raven, $delegate) {
      return function(exception, cause) {
        $raven.captureException(exception);
        $delegate(exception, cause);
      };
    }]);
  });
})();
