(function() {
  'use strict';
  /* global intlTelInputUtils */
  angular.module('k41-data-entry')
  .filter('k41PhoneNumber', ['$log', function($log) {
    return function(val) {
      var toReturn = val;
      if (!intlTelInputUtils) {
        $log.warn('International Telephone Input Utils are unavailable.  Returning unformatted phone number');
      } else {
        // 0 is the enum value for phone type which will return +xxxxxxxxxxx format.
        // 1 is the enum value for phone type which will return +x xxx-xxx-xxxx format.
        // 2 is the enum value for phone type which will return (xxx) xxx-xxxx format.
        // 3 returns: tel:+x-xxx-xxx-xxxx
        toReturn = intlTelInputUtils.formatNumberByType(val, 'us', 2);
      }
      return toReturn;
    };
  }]);
})();
