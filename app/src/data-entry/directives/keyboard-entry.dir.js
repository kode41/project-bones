(function() {
  'use strict';

  var mod = angular.module('k41-data-entry');

  /**
   * @ngdoc directive
   * @name  module:emShared:emRedirectEnter
   * @description
   * The k41-redirect-enter directive can be used to redirect the press of the enter key from an individual element
   * to a button on a form.  This directive is **only** to be used to either block enter key presses on a form
   * element OR to redirect that enter press to a button other than the form default button.
   *
   * ### Blocking <kbd>Enter</kbd> Presses
   *
   *  If there is no redirect desired, and the enter key is to be ignored on the field, then simply adding the
   * k41-redirect-enter directive to the element will be sufficient to prevent the enter event being propogated to the
   * form.
   *
   * ### Re-directing <kbd>Enter</kbd> Presses
   *
   * If it is desirable to have the <kbd>enter</kbd> key click on a button on the page, it is possible to achieve this
   * by passing the button id as the value to the k41-redirect-enter directive.  For example, given the HTML:
   *
   * ```html
   * <form>
   *   <input name="helloText" ng-model="hello.text" k41-redirect-enter="hiButton" type="text">
   *   <button id="hiButton" ng-click="sayHi()">Say Hi!</button>
   * </form>
   * ```
   * pressing the enter key while inside the helloText field will result in a jQuery call to the
   * .click() method of the _hiButton_.
   */
  mod.directive('k41RedirectEnter', function() {
    return {
      restrict: 'A',    // Attribute directive
      link: function($scope, $element, $attr) {
        $element.bind('keydown keypress', function(event) {
          if (event.which === 13) {
            event.preventDefault(); // Prevent the default enter behaviour.
            if ($attr['k41RedirectEnter']) {
              var button = $('#' + $attr['k41RedirectEnter']);
              button.click();
            }
          }
        });
      }
    };
  });
})();
