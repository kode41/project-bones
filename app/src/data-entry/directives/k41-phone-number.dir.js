(function() {
  'use strict';
  angular.module('k41-data-entry')
  .directive('k41PhoneNumber',
    ['$log',
    function() {
      return {
        restrict: 'A',
        controller: 'PhoneNumberController',
        controllerAs: 'pnCtrl',
        scope: {
          currentValue: '='
        },
        require: ['ngModel', 'k41PhoneNumber'],
        link: function($scope, element, attrs, controllers) {
          var ngModel = controllers[0];
          var phoneNumberController = controllers[1];
          phoneNumberController.init(element, ngModel);
        }
      };
    }]);
})();
