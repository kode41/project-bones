(function() {
  'use strict';

  angular.module('k41-data-entry')
  .directive('k41SsnField',
    ['$controller',
    function($controller) {
      return {
        restrict: 'A',
        controller: 'AutoFormatController',
        controllerAs: 'afctrl',
        require: ['ngModel', 'k41SsnField'],
        link: function($scope, element, attrs, controllers) {
          var ngModel = controllers[0];
          var ctrl = controllers[1];
          ctrl.init(element, ngModel);
          angular.extend(ctrl, $controller('SsnFormattingController'));
        }
      };
    }])
  .directive('k41EinField',
    ['$controller',
    function($controller) {
      return {
        restrict: 'A',
        controller: 'AutoFormatController',
        controllerAs: 'afctrl',
        require: ['ngModel', 'k41EinField'],
        link: function($scope, element, attrs, controllers) {
          var ngModel = controllers[0];
          var ctrl = controllers[1];
          ctrl.init(element, ngModel);
          angular.extend(ctrl, $controller('EinFormattingController'));
        }
      };
    }]);
})();
