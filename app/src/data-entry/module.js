/**
 * @ngdoc module
 * @name module:k41DataEntry
 * @description
 *
 * The k41-data-entry module is used to provide data entry functionality to users of the
 * Kode41 web application.
 *
 * This includes but is not limited to directives dealing with handling keyboard input,
 * form field layouts which are not specialized, etc.
 */
(function() {
  'use strict';
  angular.module('k41-data-entry', []);
})();
