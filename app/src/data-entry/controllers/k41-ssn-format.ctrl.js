(function() {
  'use strict';

  var mod = angular.module('k41-data-entry');

  /**
   * @ngdoc controller
   * @module k41-data-entry
   * @name SsnFormattingController
   * @description
   * Provides the US Social Security Number formatting functionality
   * for the auto-formatting controller.
   */
  mod.controller('SsnFormattingController', [
    function() {
      var self = this;
      self.fieldType = 'ssn';

      self.shouldObscure = function() {
        return false;
      };

      self.format = function(value) {
        var toReturn = '';
        var idx = 0;
        var numericRegex = /^\d$/;
        var obscureRegex = /^\*$/;
        var secondDashPoint = 6;
        while (idx < value.length) {
          if ((toReturn.length === 3 || toReturn.length === secondDashPoint) && value.charAt(idx) === '-') {
            toReturn += value.charAt(idx);
            if (toReturn.length === 3) {
              secondDashPoint++;
            }
          } else {
            if (numericRegex.test(value.charAt(idx)) || (obscureRegex.test(value.charAt(idx)) && toReturn.length < 7)) {
              if (toReturn.length === 3 || toReturn.length === secondDashPoint) {
                toReturn += '-';
              }
              toReturn += value.charAt(idx);
            }
          }
          idx++;
        }
        toReturn = toReturn === '' ? null : toReturn;
        return toReturn;
      };
    }]);
})();
