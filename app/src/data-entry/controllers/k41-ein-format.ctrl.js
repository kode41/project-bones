(function() {
  'use strict';

  var mod = angular.module('k41-data-entry');

  /**
   * @ngdoc controller
   * @module k41-data-entry
   * @name EinFormattingController
   * @description
   * Provides the US Social Security Number formatting functionality
   * for the auto-formatting controller.
   */
  mod.controller('EinFormattingController', [
    function() {
      var self = this;
      self.fieldType = 'ein';
      self.numericRegex = /^\d$/;

      self.shouldObscure = function() {
        return false;
      };

      self.format = function(value) {
        var toReturn = '';
        var idx = 0;
        while (idx < value.length) {
          if (toReturn.length === 2 && value.charAt(idx) === '-') {
            toReturn += value.charAt(idx);
          } else {
            if (self.numericRegex.test(value.charAt(idx))) {
              if (toReturn.length === 2) {
                toReturn += '-';
              }
              toReturn += value.charAt(idx);
            }
          }
          idx++;
        }
        toReturn = toReturn === '' ? null : toReturn;
        return toReturn;
      };
    }]);
})();
