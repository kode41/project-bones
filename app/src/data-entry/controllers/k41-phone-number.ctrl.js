(function() {
  'use strict';
  /* global intlTelInputUtils */
  angular.module('k41-data-entry')
  .controller('PhoneNumberController',  ['$log', '$scope', 'patterns', function($log, $scope, patterns) {
    var self = this;
    self.id = 'pnCtrl';
    self.read = function() {
      var input = self.$element.val();
      if (!!input && self.$model.$value) {
        self.$element.value = self.$model.$value;
      }
      if (input && input.length) {
        self.$model.$setViewValue(input);
      }
    };

    this.init = function(element, ngModel) {
      self.$element = element;
      self.$model = ngModel;
      self.$element.intlTelInput({
        onlyCountries: ['us'],
        defaultCountry: 'us',
        utilsScript: 'scripts/libphonenumber/utils.js'
      });

      self.$element.on('focus blur keyup change', function() {
        $scope.$apply(this.read);
      });

      if ($scope.currentValue && !self.$model.$value && $scope.currentValue !== '') {
        if (intlTelInputUtils) {
          var tmp = intlTelInputUtils.formatNumberByType($scope.currentValue, 'us', 2);
          self.$element.val(tmp);
        } else {
          $log.warn('intlTelInputUtils could not be found.  Returning straight number');
          self.$element.val($scope.currentValue);
        }
      } else if (self.$model.$value) {
        var digitRegex = /\d{10,15}/;
        if (digitRegex.test(self.$model.$value)) {
          var tmp2 = self.$model.$value;
          if (intlTelInputUtils) {
            tmp2 = intlTelInputUtils.formatNumberByType(self.$model.$value, 'us', 2);
          }
          self.$element.val(tmp2);
        } else if (patterns.phone.test(self.$model.$value)) {
          self.$element.val(self.$model.$value);
        }
      }
      self.read();
    };
  }]);
})();
