(function() {
  'use strict';
  var mod = angular.module('k41-data-entry');
  /**
   * @ngdoc controller
   * @module k41-data-entry
   * @name AutoFormatController
   * @description
   * The AutoFormatController is an abstract controller which provides
   * the base functionality for field auto-formatting in the Kode41
   * web application.
   *
   * This includes initialization, data reading from the directives element,
   * obscuring information when necessary (e.g. SSNs) and setting up the appropriate
   * watches to format the data entry on any key press / change, etc.
   *
   * Any sub-class of this controller must provide the following functions:
   *
   * - self.shouldObscure():  Returns true if the data should be obscured, false otherwise.
   * - self.format(val): Returns the formatted value for the field.
   * - self.fieldType:  The type of field this controller is attached to, only required if
   *      self.shouldObscure() returns true.
   */
  mod.controller('AutoFormatController',
    ['$log', '$scope', '$filter',
    function($log, $scope, $filter) {
      var self = this;

      self.obscure = function(origVal) {
        var val = origVal;
        val = $filter('obscure')(origVal, self.fieldType);
        self.$model.$viewValue = val;
        self.$model.$render();
      };

      self.read = function() {
        if (!self.$element) {
          return;
        }

        if (self.$model.$pristine && self.shouldObscure()) {
          self.obscure(self.$model.$viewValue);
        } else {
          var val = self.$element.val();
          self.$model.$viewValue = self.format(val);
          self.$model.$render();
        }
      };

      self.init = function(element, model) {
        self.$element = element;
        self.$model = model;
        self.$element.on('focus blur keyup change', function() {
          $scope.$apply(self.read);
        });

        $scope.$on(function() {self.$model.$modelValue;}, function(val) {
          if (val) {
            self.read();
          }
        });
      };

    }]);
})();
