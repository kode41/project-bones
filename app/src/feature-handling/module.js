(function() {
  'use strict';

  /**
   * @ngdoc Overview
   * @name module:k41FeatureHandling
   * @description
   * The k41-feature-handling module is responsible for the detection of features
   * required for the smooth operation of the Kode41 web application, as well as
   * the status of features of the site itself.  Currently, this module is limited
   * to browser detection and static feature enabling / disabling,
   * but will be fleshed out later.
   */
  angular.module('k41-feature-handling', []);
})();
