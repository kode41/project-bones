(function() {
  'use strict';

  angular.module('k41-feature-handling').factory('browser-detection',
  function() {
    var browserDetection = {};

    /**
     * @ngdoc property
     * @name  unsupportedBrowsers
     * @propertyOf module:k41FeatureDetection:browser-detection
     * @type {Dict}
     * @description
     * Provides a dictionary of browsers and the lowest supported version for
     * that particular browser.  If a browser does not exist in this dictionary,
     * or it's _major_ version is greater than or equal to the value, that browser is supported.
     *
     * Entries are of the format _browser name_: _first supported major version_. As
     * an example, if Internet Explorer 10 and higher is supported, the entry would be:
     * `Explorer: 10`.
     */
    browserDetection.unsupportedBrowsers = {
      Explorer: 10,
      IE: 10,
      MSIE: 10
    };

    /**
     * @ngdoc property
     * @name  updateLinks
     * @propertyOf module:k41FeatureDetection:browser-detection
     * @type {Dict}
     * @description
     * Provides a dictionary of update links for popular browsers to help users easily
     * meet the minimum application requirements.  If there is no entry here
     * for a browser name, then no download link can be provided automatically.
     *
     * Entries are of the format _browser name_: _update link_.  As an example, if Internet
     * Explorer were to be updated from http://bob.com, the entry would be:
     * `Explorer: 'http://bob.com'`.
     *
     */
    browserDetection.updateLinks = {
      Explorer: 'http://windows.microsoft.com/en-us/internet-explorer/download-ie',
      IE: 'http://windows.microsoft.com/en-us/internet-explorer/download-ie',
      MSIE: 'http://windows.microsoft.com/en-us/internet-explorer/download-ie'
    };

    /**
     * @ngdoc property
     * @name  browserNames
     * @propertyOf module:k41FeatureDetection:browser-detection
     * @description
     * Provides a dictionary of full browser names to display to the end user rather
     * than abbreviations (e.g. IE to Internet Explorer).
     */
    browserDetection.browserNames = {
      Explorer: 'Internet Explorer',
      IE: 'Internet Explorer',
      MSIE: 'Internet Explorer'
    };

    /**
     * @ngdoc function
     * @name  isBrowserSupported
     * @methodOf module:k41FeatureDetection:browser-detection
     * @description
     * Determines if the current user browser is supported in general.  This functionality
     * depends on the user having not modified their User-Agent string, as well as
     * the dictionary of unsupported browsers being maintained properly.
     */
    browserDetection.isBrowserSupported = function() {
      var browser = $.ua.browser.name;
      var major = $.ua.browser.major;
      var dictValue = browserDetection.unsupportedBrowsers[browser];

      return (!dictValue || dictValue <= major);
    };

    /**
     * @ngdoc function
     * @name  getBrowserUpdateLink
     * @methodOf module:k41FeatureDetection:browser-detection
     * @description
     * Retrieves the update link for the current browser if one exists.
     */
    browserDetection.getBrowserUpdateLink = function() {
      return browserDetection.updateLinks[$.ua.browser.name];
    };

    /**
     * @ngdoc function
     * @name  getBrowserName
     * @methodOf module:k41FeatureDetection:browser-detection
     * @description
     * Retrieves the name of the current browser from the UserAgent string.
     * @return {string} The current browser name.
     */
    browserDetection.getBrowserName = function() {
      return browserDetection.browserNames[$.ua.browser.name] || $.ua.browser.name;
    };

    /**
     * @ngdoc function
     * @name  getBrowserVersion
     * @methodOf module:k41FeatureDetection:browser-detection
     * @description
     * Retrieves the current major version of the browser from the UserAgent string.
     * @return {int} The browser major version.
     */
    browserDetection.getBrowserVersion = function() {
      return $.ua.browser.version;
    };

    /**
     * @ngdoc function
     * @name  getMinimumSupportedVersion
     * @methodOf module:k41FeatureDetection:browser-detection
     * @description
     * Retrieves the minimum version of a specific browser which is supported.
     * @return {int} The minimum major version of a browser which is supported.
     */
    browserDetection.getMinimumSupportedVersion = function() {
      return browserDetection.unsupportedBrowsers[$.ua.browser.name];
    };

    return browserDetection;
  });
})();
