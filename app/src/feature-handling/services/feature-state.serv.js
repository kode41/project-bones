(function() {
  'use strict';

  angular.module('k41-feature-handling').factory('feature-state',
  function() {
    var featureStateService = {states: '%FEATURE_SETUP%'};

    featureStateService.getFeatureStates = function() {
      return featureStateService.states;
    };

    return featureStateService;
  });
})();
