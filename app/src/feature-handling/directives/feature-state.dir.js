(function() {
  'use strict';

  angular.module('k41-feature-handling').directive('k41Feature', function() {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
        var curIf = $attr['ngIf'];
        if (curIf) {
          curIf +=  ' && featureDisabledState["' + $attr['k41Feature'] + '"] !== "hidden"';
        } else {
          curIf =  'featureDisabledState["' + $attr['k41Feature'] + '"] !== "hidden"';
        }
        if (!$scope.$eval(curIf)) {
          $element.hide();
        }
      }
    };
  });
})();
