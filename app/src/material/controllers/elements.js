 (function() {
   'use strict';

   angular.module('k41-material')
  .controller('MaterialElementsCtrl', function($scope, $log, $timeout, $q) {
    $scope.vol = Math.floor(Math.random() * 100);
    $scope.bass = Math.floor(Math.random() * 100);
    $scope.master = Math.floor(Math.random() * 100);
    $scope.heading = 'Material Design Elements';
    this.readonly = false;
    var tabs = [
          {title: 'One', content: 'Tabs will become paginated if there isn\'t enough room for them.'},
          {title: 'Two', content: 'You can swipe left and right on a mobile device to change tabs.'},
          {title: 'Three', content: 'You can bind the selected tab via the selected attribute on the md-tabs element.'},
          {title: 'Four', content: 'If you set the selected tab binding to -1, it will leave no tab selected.'},
          {title: 'Five', content: 'If you remove a tab, it will try to select a new one.'},
          {title: 'Six', content: 'There\'s an ink bar that follows the selected tab'},
          {title: 'Seven', content: 'If you set ng-disabled on a tab, it becomes unselectable.'},
          {title: 'Eight', content: 'If you look at the source, you\'re using tabs to look at a demo for tabs.'},
          {title: 'Nine', content: 'If you set md-theme=\\\'green\\\' on the md-tabs element, you\'ll get green tabs.'},
          {title: 'Ten', content: 'If you\'re still reading this, you should just go check out the API docs for tabs!'}
        ],
    selected = null,
    previous = null;
    $scope.tabs = tabs;
    $scope.selectedIndex = 2;
    $scope.$watch('selectedIndex', function(current, old) {
      previous = selected;
      selected = tabs[current];
      if (old + 1 && (old !== current)) {
        $log.debug('Goodbye ' + previous.title + '!');
      }
      if (current + 1) {
        $log.debug('Hello ' + selected.title + '!');
      }
    });
    $scope.addTab = function(title, view) {
      view = view || title + ' Content View';
      tabs.push({title: title, content: view, disabled: false});
    };
    $scope.removeTab = function(tab) {
      var index = tabs.indexOf(tab);
      tabs.splice(index, 1);
    };

    $scope.data = {
      cb1: true,
      cb4: true,
      cb5: false
    };
    $scope.message = 'false';
    $scope.onChange = function(cbState) {
      $scope.message = cbState;
    };

    var self = this;
    self.simulateQuery = false;
    self.isDisabled    = false;
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for repos... use $timeout to simulate
     * remote dataservice call.
     */
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(item) {
        return (item.value.indexOf(lowercaseQuery) === 0);
      };
    }
    function querySearch (query) {
      var results = query ? self.repos.filter(createFilterFor(query)) : self.repos,
          deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function() { deferred.resolve(results); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }
    function searchTextChange(text) {
      $log.info('Text changed to ' + text);
    }
    function selectedItemChange(item) {
      $log.info('Item changed to ' + JSON.stringify(item));
    }
    /**
     * Build `components` list of key/value pairs
     */
    function loadAll() {
      var repos = [
        {
          'name'      : 'Angular 1',
          'url'       : 'https://github.com/angular/angular.js',
          'watchers'  : '3,623',
          'forks'     : '16,175',
        },
        {
          'name'      : 'Angular 2',
          'url'       : 'https://github.com/angular/angular',
          'watchers'  : '469',
          'forks'     : '760',
        },
        {
          'name'      : 'Angular Material',
          'url'       : 'https://github.com/angular/material',
          'watchers'  : '727',
          'forks'     : '1,241',
        },
        {
          'name'      : 'Bower Material',
          'url'       : 'https://github.com/angular/bower-material',
          'watchers'  : '42',
          'forks'     : '84',
        },
        {
          'name'      : 'Material Start',
          'url'       : 'https://github.com/angular/material-start',
          'watchers'  : '81',
          'forks'     : '303',
        }
      ];
      return repos.map(function(repo) {
        repo.value = repo.name.toLowerCase();
        return repo;
      });
    }
    self.repos         = loadAll();
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;
  });
 })();
