(function() {
  'use strict';
  /**
   * @ngdoc service
   * @name instructional-text
   * @module k41Shared
   *
   * @description
   *
   * The `instructional-text` service is an Kode41 service which facilitates the retrieval of
   * instructional text for various Kode41 pages throughout the web front end.
   *
   * ## General usage
   * The `instructional-text` service provides a single method, `getText(location)`, which will
   * provide the correct instructional text for a page based on the current location the user is
   * at.
   */
  angular.module('k41-shared').factory('instructional-text', function() {
    var instructionalService = {text: []};
    instructionalService.text['default'] = null;

    /**
     * @ngdoc method
     * @name instructional-text#getText
     * @description
     * The `getText(location)` method will retrieve the instructional text for the page specified
     * by _location_.  If there is no instructional text specified, the default instructional
     * text ('') will be returned.
     *
     * @param {string} location The current, post-hashbang, location. E.g. /settings/locations
     * @returns {string} The instructional text for the page specified by `location`.
     */
    instructionalService.getText = function(location) {
      return instructionalService.text[location] ?
        instructionalService.text[location] : instructionalService.text['default'];
    };

    /**
     * @ngdoc method
     * @name instructional-text#init
     * @description
     * **INTERNAL**
     *
     * Initializes the instructional text array with the appropriate instructional text for the
     * various pages which require it.
     */
    function init() {
      instructionalService.text['/staff/users'] = [{text:
        'This page demonstrates how wizards and tables and how those can be used for user administration. Click on ' +
        'the Add New User button to go through the wizzard and add a new user. Clicking on a row in the user table ' +
        'will bring up a simple modify user dialog. If you want to delete a user from the system you can click on - ' +
        'sign in the table row (we intentionally disabled that button for one of the rows so there is always at least' +
        'one user in the table). Finally, you can use search bar on the top of the page to search for a specific user' +
        ' based on the any column in the user table.'}];
      instructionalService.text['holidays'] = [
        {text: 'You can find the list of holidays your employer provides and the day it is observed on below.'}
      ];
      instructionalService.text['forms'] = [
        {text: 'The following example demonstrates custom Bootstrap forms created by Kode41 team.'}
      ];

      instructionalService.text['addEmployee'] = [
        {text: 'Wizard Example'},
        {text: 'Custom wizard example. Fill all steps to add a new user.'}
      ];

      instructionalService.text['materialForms'] = [
        {text: 'Below are some examples of Material Design forms inside of Angular webapp. Integration is done using' +
        ' directives further explained <a href="https://material.angularjs.org/latest/">here</a>.'}
      ];

      instructionalService.text['materialElements'] = [
        {text: 'Below are some examples of Material Design elements inside of Angular webapp. Integration is done ' +
        'using directives further explained <a href="https://material.angularjs.org/latest/">here</a>.'}
      ];

      instructionalService.text['materialGridColor'] = [
        {text: 'Below is an example of Material Design color palette and responsive grid inside of Angular webapp.' +
        ' Click on the various colors to see the theme preview change. Also resize the window to see how the grid ' +
        'adjusts its layout. Integration is done using directives further explained ' +
        '<a href="https://material.angularjs.org/latest/">here</a>.'}
      ];

      instructionalService.text['signature'] = [
        {text: 'This page demonstrates usage of canvas component for saving users signature. Save button below ' +
        'the canvas changes its state based on the changes on the canvas component.'}
      ];

      instructionalService.text['holidays'] = [
        {text: 'This page demonstrates usage of form controls in a table layout. The state of Save Changes button ' +
        'changes automatically when the selection of rows in the table changes.'}
      ];

      instructionalService.text['addEmployee'] = [
        {text: 'This page demonstrates how wizards and tables can be used for user administration. Click on the Add ' +
        'New User button to go through the wizzard and add a new user. Clicking on a row in the user table will ' +
        'bring up a simple modify user dialog. Finally, you can use search bar on the top of the page to search for ' +
        'a specific user based on the any column in the user table.'}
      ];

      instructionalService.text['grid'] = [
      {text: 'This page Bootstrap grid usage within Angular webapp. The first row contains three columns and the ' +
      'bottom row contains four columns. All columns are reodered when window width changes. For even more control ' +
      'over component positioning please check out Material Design pages. With Material Design components both their ' +
      'horizontal and vertical positioning can be controlled.'}];


    }

    init();
    return instructionalService;
  });
})();
