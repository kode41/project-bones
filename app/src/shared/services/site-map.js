(function() {
  'use strict';

  var rootTemplate = '<div ui-view class="mainview-animation"></div>';

  /**
   * @ngdoc object
   * @name siteMap
   * @module k41Shared
   * @description
   * The `siteMap` constant service is used to initialize ui-router and main navigation menu.
   *
   * Used `data` properties:
   * - `title` _{String}_: default title to use in menu, breadcrumbs and page heading
   * - `userRoles` _{String[]}_: available values are "all", "admin" and "employee"
   * - `nav` _{Boolean}_: rendered in main nav menu if `true`
   * - `icon` _{String}_: icon string to use without the `fa-` prefix
   * - `parent` _{String}_: overwrites the default `state.parent` for breadcrumb and nav menu rendering
   * - `breadcrumb` _{Boolean}_: hides breadcrumb if `false`
   * - `options` _{String[]}_: feature name without the `_enabled` suffix from `Company` resource
   *      to manage availability of the state, values: _benefits, hr, compliance, payroll, time_
   * - `disabled` _{Boolean}_: temporary, should disable the state if `true`
   *
   * @returns {Array} a list of states
   */
  angular.module('k41-shared').constant('siteMap', _.freeze([
    {
      state: 'login',
      url: '/login',
      templateUrl: 'src/shared/user-login.html',
      data: {
        title: 'Login',
        userRoles: ['all']
      }
    }, {
      state: 'signup',
      url: '/signup',
      templateUrl: 'src/shared/signupform.html',
      data: {
        title: 'Sign Up',
        userRoles: ['all']
      }
    }, {
      state: 'signup-confirm',
      url: '/signup-confirm?token',
      templateUrl: 'src/shared/signupform-confirm.html',
      data: {
        title: 'Sign Up',
        userRoles: ['all']
      }
    }, {
      state: 'resetPassword',
      url: '/resetpassword?token',
      templateUrl: 'src/shared/reset-password.html',
      data: {
        title: 'Reset Password',
        userRoles: ['all']
      }
    }, {
      state: 'admin',
      url: '/admin/dashboard',
      templateUrl: 'src/employee/dashboard.html',
      data: {
        title: 'Welcome Admin user! This is an example of your dashboard.',
        nav: true,
        icon: 'home',
        userRoles: ['admin']
      }
    }, {
      state: 'user',
      url: '/user/dashboard',
      templateUrl: 'src/employee/dashboard.html',
      data: {
        title: 'Welcome regular user! This is an example of your dashboard.',
        nav: true,
        icon: 'home',
        userRoles: ['admin', 'user']
      }
    },
    ///////////////Staff/////////////////////////
    {
      state: 'staffRoot',
      'abstract': true,
      url: '/staff',
      template: rootTemplate,
      data: {
        title: 'Staff',
        nav: true,
        icon: 'group',
        userRoles: ['admin']
      }
    }, {
      state: 'users',
      parent: 'staffRoot',
      url: '/users',
      templateUrl: 'src/shared/employee-directory.html',
      data: {
        title: 'Users',
        nav: true,
        userRoles: ['admin']
      }
    }, {
      state: 'addEmployee',
      parent: 'staffRoot',
      url: '/add/',
      templateUrl: 'src/employee/add-employee-quick.html',
      data: {
        title: 'User Administration',
        nav: false,
        parent: 'users',
        userRoles: ['admin']
      }
    }, {
      state: 'holidays',
      parent: 'staffRoot',
      url: '/holidays',
      templateUrl: 'src/employee/holidays.html',
      data: {
        title: 'Holidays',
        nav: true,
        userRoles: ['admin']
      }
    }, {
      state: 'signature',
      parent: 'staffRoot',
      url: '/signature',
      templateUrl: 'src/employee/signature.html',
      data: {
        title: 'Signature',
        nav: true,
        userRoles: ['admin']
      }
    },
    ///////////////End Staff/////////////////////////
    ////////////////// Settings ////////////////////////
    {
      state: 'settingsRoot',
      'abstract': true,
      url: '/settings',
      template: rootTemplate,
      data: {
        title: 'Settings',
        nav: true,
        icon: 'gear',
        userRoles: ['admin']
      }
    }, {
      state: 'controllers',
      parent: 'settingsRoot',
      url: '/controllers',
      templateUrl: 'src/settings/controller.html',
      data: {
        title: 'Controllers',
        nav: true
      }
    }, {
      state: 'states',
      parent: 'settingsRoot',
      url: '/state',
      templateUrl: 'src/settings/states.html',
      data: {
        title: 'States',
        nav: true
      }
    }, {
      state: 'rest',
      parent: 'settingsRoot',
      url: '/rest',
      templateUrl: 'src/settings/rest.html',
      data: {
        title: 'Rest API',
        nav: true
      }
    }, {
      state: 'collections',
      parent: 'settingsRoot',
      url: '/collections',
      templateUrl: 'src/settings/collections.html',
      data: {
        title: 'Collections',
        nav: true
      }
    }, {
      state: 'testing',
      parent: 'settingsRoot',
      url: '/testing',
      templateUrl: 'src/settings/testing.html',
      data: {
        title: 'Testing',
        nav: true
      }
    },
    ///////////////End Settings/////////////////////////
    /////////////// Bootstrap ////////////////////////
    {
      state: 'bootstrapRoot',
      'abstract': true,
      url: '/bootstrap',
      template: rootTemplate,
      data: {
        title: 'Bootsrap',
        nav: true,
        icon: 'btc',
        userRoles: ['admin']
      }
    }, {
      state: 'forms',
      parent: 'bootstrapRoot',
      url: '/forms',
      templateUrl: 'src/bootstrap/forms.html',
      data: {
        title: 'Forms',
        nav: true,
        parent: 'bootstrapRoot'
      }
    }, {
      state: 'grid',
      parent: 'bootstrapRoot',
      url: '/grid',
      templateUrl: 'src/bootstrap/grid.html',
      data: {
        title: 'Responsive Grid',
        nav: true,
        parent: 'bootstrapRoot'
      }
    },
    ///////////////End Settings/////////////////////////
    ///////////////Material Design/////////////////////////
    {
      state: 'materialRoot',
      'abstract': true,
      url: '/material',
      template: rootTemplate,
      data: {
        title: 'Material Design',
        nav: true,
        icon: 'maxcdn',
        userRoles: ['admin']
      }
    }, {
      state: 'materialForms',
      parent: 'materialRoot',
      url: '/forms',
      templateUrl: 'src/material/material.html',
      data: {
        title: 'Forms',
        nav: true,
        userRoles: ['admin']
      }
    }, {
      state: 'materialElements',
      parent: 'materialRoot',
      url: '/elements',
      templateUrl: 'src/material/elements.html',
      data: {
        title: 'Elements',
        nav: true,
        userRoles: ['admin']
      }
    }, {
      state: 'materialGridColor',
      parent: 'materialRoot',
      url: '/grid',
      templateUrl: 'src/material/grid-colors.html',
      data: {
        title: 'Colors & Grid',
        nav: true,
        userRoles: ['admin']
      }
    },
    /////////////////END MATERIAL////////////////////////////
    ////////////////////////////////////////////////////////
    {
      state: 'userSettingsRoot',
      'abstract': true,
      url: '/user/settings',
      template: rootTemplate,
      data: {
        title: 'Settings',
        nav: true,
        icon: 'gear',
        userRoles: ['admin', 'user']
      }
    }, {
      state: 'userControllers',
      parent: 'userSettingsRoot',
      url: '/controllers',
      templateUrl: 'src/settings/controller.html',
      data: {
        title: 'Controllers',
        nav: true
      }
    }, {
      state: 'userStates',
      parent: 'userSettingsRoot',
      url: '/state',
      templateUrl: 'src/settings/states.html',
      data: {
        title: 'States',
        nav: true
      }
    }, {
      state: 'userRest',
      parent: 'userSettingsRoot',
      url: '/rest',
      templateUrl: 'src/settings/rest.html',
      data: {
        title: 'Rest API',
        nav: true
      }
    }, {
      state: 'userCollections',
      parent: 'userSettingsRoot',
      url: '/collections',
      templateUrl: 'src/settings/collections.html',
      data: {
        title: 'Collections',
        nav: true
      }
    },
  ]));
})();
