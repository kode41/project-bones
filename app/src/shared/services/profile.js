(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name profile
   * @module k41Shared
   *
   * @description
   * Keeps users profile and logged in flag, handles updates and logging in/out.
   */
  angular.module('k41-shared').factory('profile', [
    '$rootScope', '$q', 'session',
    function($rootScope, $q, session) {

      var profile, publicProfile;

      function updatePublicProfile() {
        publicProfile = _.freeze(angular.copy(profile));
        $rootScope.$broadcast('profileChanged', publicProfile);
        return publicProfile;
      }

      function resetProfile() {
        profile = {loggedIn: false, user: {logo_url: '/assets/img/kode41.png'}, company: {}};
        return updatePublicProfile();
      }
      resetProfile();

      function transformUser(inc) {
        var res = angular.copy(inc);
        res.full_name = res.first_name + ' ' + res.last_name;
        res.role = res.admin ? 'admin' : 'user';
        res.isAdmin = res.admin;
        delete res.admin;
        res.isLoggedIn = true;

        return res;
      }

      function loadCompany(companyId) {
        console.log(companyId);
        return $q.when({id: 535}) //use rest.companies.get(companyId)
          .then(function(res) {
            profile.company = res;
            return updatePublicProfile();
          });
      }

      var profileServ = {};

      /**
       * @ngdoc method
       * @name profile#update
       *
       * @param {boolean} [reload=false] - flag to force user data to be reloaded
       *
       * @description
       * Refreshes profile data including user info as appropriate.
       * Resets the profile if user's session expired.
       *
       * If `maaId` is defined in current session uses MAA user endpoint, otherwise uses regular user endpoint.
       * Falls back to company od stored in session if it cannot be used from loaded resource.
       *
       * @return {Promise} resolved promise contains profile data
       */
      profileServ.update = function(reload) {
        var ls = session.get();
        if (ls.loggedIn) {
          if (!profile.loggedIn || reload) {
            var loadUser = $q.when({id: 1, first_name: 'Kode41', last_name: 'User', admin: true}); //rest.users.get(id)
            return loadUser
              .then(function(res) {
                var company_id = session.start(res.id, company_id || ls.companyId, ls.token);
                profile.loggedIn = true;
                profile.user = transformUser(res);
                return loadCompany(res.company || ls.companyId);
              });
          }
        } else {
          if (profile.loggedIn || reload) {
            return $q.when(resetProfile());
          }
        }
        return $q.when(publicProfile);
      };

      /**
       * @ngdoc method
       * @name profile#get
       *
       * @description
       * Gets a frozen copy of profile data so that the internal one cannot be modified.
       *
       * @return {Object} frozen copy of the profile data
       */
      profileServ.get = function() {
        return publicProfile;
      };

      /**
       * @ngdoc method
       * @name profile#updateCompany
       *
       * @description
       * Refreshes company data if its id is available.
       *
       * @return {Promise} resolved promise contains profile data
       */
      profileServ.updateCompany = function() {
        return profile.company.id ?
          loadCompany(profile.company.id) :
          $q.when(publicProfile);
      };

      /**
       * @ngdoc method
       * @name profile#logIn
       *
       * @param {string} username - username to log in with
       * @param {string} password - password to log in with
       *
       * @description
       * Attempts to log in user with provided credentials.
       * If successful starts a session, fills in profile data and broadcasts about it.
       *
       * @return {Promise} resolved promise contains profile data
       */
      profileServ.logIn = function(username, password) {
        console.log(username);
        password = 1;
        return $q.when({user: {id: 1, company: 535}, token: '02123456899'}).then(function(result) { //login call
            session.start(result.user.id, null, result.user.company, result.token);
            return $q.when({id: 1, first_name: 'Kode41', last_name: 'User', admin: true}) //replace with API get call
              .then(function(res) {
                profile.loggedIn = true;
                profile.user = transformUser(res);
                return loadCompany(profile.user.company);
              });
          }).then(function() {
            $rootScope.$broadcast('loggedIn');
            return publicProfile;
          });
      };

      /**
       * @ngdoc method
       * @name profile#logOut
       *
       * @description
       * Logs out user by endind session and resetting profile data.
       * After that broadcasts about it.
       */
      profileServ.logOut = function() {
        session.end();
        resetProfile();
        $rootScope.$broadcast('loggedOut');
      };

      /**
       * @ngdoc method
       * @name profile#hasRoleAccess
       *
       * @param {Object} state - a state from state-map to check
       *
       * @description
       * Checks whether user can access specified state based on his role.
       *
       * @return {boolean} true if access granted, false if access denied
       */
      profileServ.hasRoleAccess = function(state) {
        var roles = state.data.userRoles;
        return _.contains(roles, 'all') || (profile.loggedIn && _.contains(roles, profile.user.role));
      };

      return _.freeze(profileServ);
    }
  ]);
})();
