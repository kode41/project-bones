(function() {
  'use strict';

  angular.module('k41-shared').factory('rest',
  ['$rootScope', '$q', '$log', '$timeout', 'Restangular', 'session', 'config', 'responseHandler',
  function($rootScope, $q, $log, $timeout, Restangular, session, config, responseHandler) {

    var defaultHeaders = {
      'Content-Type': 'application/json;charset=UTF-8'
    };

    function restFactory(headers) {
      return Restangular
        .setErrorInterceptor(responseHandler.restangularErrorInterceptor)
        .withConfig(function(RestangularConfigurer) {
          RestangularConfigurer.setDefaultHeaders(headers || {});
        });
    }
    var noAuth = restFactory;

    function auth(headers) {
      // Set access token in header
      return restFactory(_.extend({Authorization: 'Token ' + session.get().token}, headers));
    }

    function polling(url) {
      return auth(defaultHeaders)
        .withConfig(function(RestangularConfigurer) {
          RestangularConfigurer.setBaseUrl(config.api.host);
          RestangularConfigurer.setRequestSuffix('');
        })
        .one(url);
    }

    function getResponse(response) {
      return response && response.plain();
    }

    var methods = {
      all: function(rest, route, params, replacements) {
        function results(response) {
          return response.results;
        }

        if (replacements) {
          for (var idx in replacements) {
            route = route.replace(idx, replacements[idx]);
          }
        }
        return rest(defaultHeaders).one(route)
          .get(params)
          .then(function(response) {
            response = getResponse(response);
            if (response.next && response.count > response.results.length) {
              var pageCount = Math.ceil(response.count / response.results.length),
                pages = [$q.when(response.results)], page = 2;

              while (page <= pageCount) {
                pages.push(rest().one(route).get(_.extend({page: page++}, params)).then(results));
              }
              return $q.all(pages);
            } else {
              return [response.results];
            }
          })
          .then(function(pages) {
            pages = _.flatten(pages);
            $log.debug('GET', route, params, pages);
            return pages;
          });
      },

      get: function(rest, route, id, params, replacements) {
        var finalRoute = route + (id || '');
        if (replacements) {
          for (var idx in replacements) {
            finalRoute = finalRoute.replace(idx, replacements[idx]);
          }
        }
        return rest(defaultHeaders).one(finalRoute)
          .get(params)
          .then(function(response) {
            response = getResponse(response);
            $log.debug('GET', route, id, params, response);
            return response;
          });
      },

      post: function(rest, route, data, path, params) {
        var isFile = data instanceof FormData,
          request = rest(!isFile ? defaultHeaders : undefined)
          .all(route + (path || ''));

        if (isFile) {
          request = request.withHttpConfig({transformRequest: angular.identity});
        }

        return request.customPOST(
            _.isNull(data) || _.isUndefined(data) ? {} : data,
            '',
            params,
            isFile ? {'Content-Type': undefined} : undefined
          )
          .then(function(response) {
            response = getResponse(response);
            $log.debug(isFile ? 'POST (multipart)' : 'POST', route, path, data, params, response);
            return response;
          });
      },

      put: function(rest, route, id, data, params) {
        return rest(defaultHeaders).all(route)
          .customOperation('put', id, params, null, data)
          .then(function(response) {
            response = getResponse(response);
            $log.debug('PUT', route, id, data, params, response);
            return response;
          });
      },

      patch: function(rest, route, id, data, params) {
        return rest(defaultHeaders).all(route)
          .customOperation('patch', id, params, null, data)
          .then(function(response) {
            response = getResponse(response);
            $log.debug('PATCH', route, id, data, params, response);
            return response;
          });
      },

      save: function(rest, route, data) {
        return data.id ?
          this.patch(rest, route, data.id, _.omit(data, 'id')) :
          this.post(rest, route, data);
      },

      remove: function(rest, route, id, params) {
        return rest(defaultHeaders).one(route + (id || ''))
          .remove(params)
          .then(function(response) {
            response = getResponse(response);
            $log.debug('DELETE', route, id, params);
            return response;
          });
      }
    };

    // use partial binding to generate specific rest interface from generic methods
    function createRest(rest, route) {
      var params = [methods, rest];
      if (route) {
        params.push(route);
      }
      return _.chain(methods).keys().reduce(function(memo, key) {
        memo[key] = methods[key].bind.apply(methods[key], params);
        return memo;
      }, {route: route || ''}).value();
    }

    var _interface = _.extend({polling: polling}, createRest(auth), {noAuth: createRest(noAuth)}),
      endpoints = [
        {name: 'login', route: 'auth/', noAuth: true},

        {name: 'users', route: 'users/'}
      ];
    // generate endpoint-specific rest interfaces
    _.each(endpoints, function(endpoint) {
      _interface[endpoint.name] = createRest(endpoint.noAuth ? noAuth : auth, endpoint.route);
    });

    return _.freeze(_interface);
  }]);
})();
