(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name enums
   * @module k41Shared
   *
   * @description
   * The `enums` service provides arrays and objects of enumerations.
   * Objects are named the same way as the base array with added 'ByValue' suffix and contain the same items.
   * Each item in the enumeration has at least two fields: `name` and `value`.
   */
  angular.module('k41-shared')
  .factory('enums', ['usStates', function(usStates) {

    // globally used enumerations
    var arrays = {
      // payment methods distribution types
      distributionTypes: [
        {name: '% Percentages', label: 'Percent', value: 'percent'},
        {name: '$ Dollars', label: 'Amount', value: 'dollars'}
      ],

      genders: [
        {name: 'Male', value: 'm', pluralName: 'Males'},
        {name: 'Female', value: 'f', pluralName: 'Females'},
        {name: 'Other', value: 'o', pluralName: 'Others'}
      ],

      maritalStatuses: [
        {value: 'single', name: 'Single'},
        {value: 'married', name: 'Married'},
        {value: 'married-higher-rate', name: 'Married, but WH Higher Rate'}
      ],

      jobClassifications: [
        {value: 'full_time', name: 'Full-time'},
        {value: 'part_time', name: 'Part-time'}
      ],

      compensationTypes: [
        {value: 'salary', name: 'Salary'},
        {value: 'hourly', name: 'Hourly'},
        {value: 'variable', name: 'Variable Rate'}
      ],

      // add employee new hire
      hireStatuses: [
        {value: 'new_hire', name: 'New Hire'},
        {value: 'active', name: 'Active'}
      ],

      // bank account types
      accountTypes: [
        {name: 'Paper Check', value: 'paper'},
        {name: 'Checking', value: 'checking'},
        {name: 'Savings', value: 'savings'}
      ],

      timeZones: [
        {name: 'US/Eastern', value: 'US/Eastern'},
        {name: 'US/Central', value: 'US/Central'},
        {name: 'US/Mountain', value: 'US/Mountain'},
        {name: 'US/Pacific', value: 'US/Pacific'},
        {name: 'US/Alaska', value: 'US/Alaska'},
        {name: 'US/Hawaii', value: 'US/Hawaii'}
      ],

      timeOffLeaveTypes: [
        {name: 'Paid', value: 'paid'},
        {name: 'Unpaid', value: 'unpaid'}
      ],

      accrualMethods: [
        {name: 'Lump Sum', value: 'none'},
        {name: 'Accrual Hours', value: 'hourly'},
        {name: 'Accrual Pay Period', value: 'pay_period'}
      ],

      taxTypes: [
        {name: 'Pre-tax', value: 'pre'},
        {name: 'Post-tax', value: 'post'}
      ],

      payrollFrequency: [
        {name: 'Weekly', value: 'weekly'},
        {name: 'Bi-Weekly', value: 'biweekly'},
        {name: 'Monthly', value: 'monthly'},
        {name: 'Semi-Monthly', value: 'semimonthly'}
      ],

      deductionTypes: [
        {name: 'Health', value: 'health'},
        {name: 'Dental', value: 'dental'},
        {name: 'Vision', value: 'vision'},
        {name: 'HSA', value: 'hsa'},
        {name: 'FSA', value: 'fsa'},
        {name: 'HRA', value: 'hra'},
        {name: '401(k)', value: '401k'},
        {name: '403(b)', value: '403b'},
        {name: '457', value: '457'},
        {name: 'Roth 401(k)', value: 'roth401k'},
        {name: 'Roth 403(b)', value: 'roth403b'},
        {name: 'Simple', value: 'simple'},
        {name: 'Aflac', value: 'aflac'},
        {name: 'STD', value: 'std'},
        {name: 'LTD', value: 'ltd'},
        {name: 'Life', value: 'life'},
        {name: 'SDI', value: 'sdi'},
        {name: 'Loan', value: 'loan'},
        {name: 'Garnishment', value: 'garnishment'},
        {name: 'Other', value: 'other'}
      ],

      earningTaxTypes: [
        {name: 'Fully Taxable', value: 'fullyTaxable'},
        {name: 'Not Taxable', value: 'notTaxable'},
        {name: 'Exempt Income', value: 'exempt'},
        {name: 'Fully Taxable, Supplemental Rate', value: 'suplemRate'}
      ],

      earningPaymentTypes: [
        {name: 'Cash', value: 'cash'},
        {name: 'Non-cash', value: 'nonCash'}
      ],

      earningTypes: [
        {name: 'Base Pay', value: 'base_pay', payment: 'cash', taxType: 'fullyTaxable'},
        {name: 'Bonus', value: 'bonus', payment: 'cash', taxType: 'fullyTaxable'},
        {name: 'Car Allowance', value: 'car_allowance', payment: 'nonCash', taxType: 'fullyTaxable'},
        {name: 'Cash Tips', value: 'cash_tips', payment: 'nonCash', taxType: 'fullyTaxable'},
        {name: 'Commission', value: 'commission', payment: 'cash', taxType: 'fullyTaxable'},
        {name: 'Credit Card Tips', value: 'credit_tips', payment: 'cash', taxType: 'fullyTaxable'},
        {name: 'Expense Reimbursement', value: 'expense_reimbursement', payment: 'cash', taxType: 'notTaxable'},
        {name: 'Group Term Life Insurance', value: 'group_life', payment: 'nonCash', taxType: 'fullyTaxable'},
        {name: 'Holiday Pay', value: 'holiday_pay', payment: 'cash', taxType: 'fullyTaxable'},
        {name: 'Meals', value: 'meals', payment: 'nonCash', taxType: 'fullyTaxable'},
        {name: 'Paid Time Off', value: 'paid_time_off', payment: 'cash', taxType: 'fullyTaxable'},
        {name: 'Secondary Pay', value: 'secondary_pay', payment: 'cash', taxType: 'fullyTaxable'},
        {name: 'Tax Free Fringe', value: 'tax_free_fringe', payment: 'nonCash', taxType: 'notTaxable'},
        {name: 'Tax Free Moving Expense', value: 'tax_free_moving', payment: 'cash', taxType: 'notTaxable'},
        {name: 'Tax Free Other Compensation', value: 'tax_free_other', payment: 'cash', taxType: 'notTaxable'},
        {name: 'Taxable Fringe', value: 'taxable_fringe', payment: 'nonCash', taxType: 'fullyTaxable'},
        {name: 'Taxable Moving Expense', value: 'taxable_moving', payment: 'nonCash', taxType: 'fullyTaxable'}
      ],

      contractorTypes: [
        {name: 'Individual', value: 'individual', plural: 'Individuals'},
        {name: 'Entity', value: 'entity', plural: 'Entities'}
      ],

      contractorETIs: [
        {name: 'Individual Sole Proprietorship', value: 'individual_sole_prop'},
        {name: 'C Corp', value: 'c_corp'},
        {name: 'S Corp', value: 's_corp'},
        {name: 'Partnership', value: 'partnership'},
        {name: 'Trust Estate', value: 'trust_estate'},
        {name: 'LLC C Corp', value: 'llc_c_corp'},
        {name: 'LLC S Corp', value: 'llc_s_corp'},
        {name: 'LLC Partnership', value: 'llc_partnership'}
      ],

      reviewTypes: [
        {name: 'Self Review', value: 'self_review'},
        {name: 'Manager to Employee', value: 'manager_employee'},
        {name: 'Employee to Manager', value: 'employee_manager'},
        {name: 'Peer Review', value: 'peer_review'}
      ],

      languages: [
        {name: 'English', value: 'eng'},
        {name: 'Spanish', value: 'esp'}
      ],

      states: _.map(usStates.states(), function(state) { return {value: state.state, name: state.name}; })

    }, enums = {};

    // generate 'ByValue' objects
    _.chain(arrays).keys().each(function(key) {
      enums[key] = arrays[key];
      enums[key + 'ByValue'] = _.chain(arrays[key]).map(function(item, idx) {
        return _.extend(item, {index: idx});
      }).indexBy('value').value();
    });

    return _.freeze(enums);
  }]);
})();
