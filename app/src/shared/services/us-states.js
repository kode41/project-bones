(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name usStates
   * @module k41Shared
   *
   * @description
   * Provides a list of US states, optionally filtered by some condition and in different structure.
   */
  angular.module('k41-shared').factory('usStates', [function() {

    // TODO: extend with other useful properties?
    var states = [
      {state: 'AL', name: 'Alabama'},
      {state: 'AK', name: 'Alaska'},
      {state: 'AZ', name: 'Arizona'},
      {state: 'AR', name: 'Arkansas'},
      {state: 'CA', name: 'California'},
      {state: 'CO', name: 'Colorado'},
      {state: 'CT', name: 'Connecticut'},
      {state: 'DE', name: 'Delaware'},
      {state: 'FL', name: 'Florida', taxNumberRegex: /^\d{8}$/},
      {state: 'GA', name: 'Georgia'},
      {state: 'HI', name: 'Hawaii'},
      {state: 'ID', name: 'Idaho'},
      {state: 'IL', name: 'Illinois'},
      {state: 'IN', name: 'Indiana'},
      {state: 'IA', name: 'Iowa'},
      {state: 'KS', name: 'Kansas'},
      {state: 'KY', name: 'Kentucky'},
      {state: 'LA', name: 'Louisiana'},
      {state: 'ME', name: 'Maine'},
      {state: 'MD', name: 'Maryland'},
      {state: 'MA', name: 'Massachusetts'},
      {state: 'MI', name: 'Michigan'},
      {state: 'MN', name: 'Minnesota'},
      {state: 'MS', name: 'Mississippi'},
      {state: 'MO', name: 'Missouri'},
      {state: 'MT', name: 'Montana'},
      {state: 'NE', name: 'Nebraska'},
      {state: 'NV', name: 'Nevada'},
      {state: 'NH', name: 'New Hampshire'},
      {state: 'NJ', name: 'New Jersey'},
      {state: 'NM', name: 'New Mexico'},
      {state: 'NY', name: 'New York', taxNumberRegex: /^\d{8}$/},
      {state: 'NC', name: 'North Carolina'},
      {state: 'ND', name: 'North Dakota'},
      {state: 'OH', name: 'Ohio', taxNumberRegex: /^\d{8}$/},
      {state: 'OK', name: 'Oklahoma'},
      {state: 'OR', name: 'Oregon'},
      {state: 'PA', name: 'Pennsylvania'},
      {state: 'RI', name: 'Rhode Island'},
      {state: 'SC', name: 'South Carolina'},
      {state: 'SD', name: 'South Dakota'},
      {state: 'TN', name: 'Tennessee'},
      {state: 'TX', name: 'Texas'},
      {state: 'UT', name: 'Utah'},
      {state: 'VT', name: 'Vermont'},
      {state: 'VA', name: 'Virginia'},
      {state: 'WA', name: 'Washington'},
      {state: 'WV', name: 'West Virginia'},
      {state: 'WI', name: 'Wisconsin'},
      {state: 'WY', name: 'Wyoming'}
    ];

    function isProperCondition(condition) {
      return _.isFunction(condition) || !_.isEmpty(condition);
    }

    function pluckField(field, condition) {
      return isProperCondition(condition) ?
        _.chain(states).filter(condition).pluck(field).value() :
        _.pluck(states, field);
    }

    return _.freeze({
      /**
       * @ngdoc method
       * @name usStates#states
       *
       * @param {Function|Object|string} [condition] - a predicate to filter the states with
       *
       * @description
       * If `condition` is a proper predicate then filters the full list of states.
       * Any predicate that would be accepted by lodash's `_.filter` method is accepted.
       *
       * @returns {Array}
       * A list of US states. Each item has at least two fields:
       * - `state` - a 2-letter abbreviation
       * - `name` - the name of the state
       * - other optional fields are possible as well
       * **Example of one state:**
       * ```javascript
       *  {
       *    "state": "NY",
       *    "name": "New York",
       *  }
       * ```
       */
      states: function(condition) {
        return isProperCondition(condition) ? _.filter(states, condition) : angular.copy(states);
      },

      /**
       * @ngdoc method
       * @name usStates#abbrev
       *
       * @param {Function|Object|string} [condition] - a predicate to filter the states with
       *
       * @description
       * If `condition` is a proper predicate then filters the full list of states.
       * Any predicate that would be accepted by lodash's `_.filter` method is accepted.
       *
       * @returns {string[]} a list of state abbreviations, e.g.: "CA", "NY", "TX", ...
       */
      abbrev: pluckField.bind(null, 'state'),

      /**
       * @ngdoc method
       * @name usStates#names
       *
       * @param {Function|Object|string} [condition] - a predicate to filter the states with
       *
       * @description
       * If `condition` is a proper predicate then filters the full list of states.
       * Any predicate that would be accepted by lodash's `_.filter` method is accepted.
       *
       * @returns {string[]} a list of state name, e.g.: "California", "New York", "Texas", ...
       */
      names: pluckField.bind(null, 'name'),

      /**
       * @ngdoc method
       * @name usStates#nameMap
       *
       * @param {Function|Object|string} [condition] - a predicate to filter the states with
       *
       * @description
       * If `condition` is a proper predicate then filters the full list of states.
       * Any predicate that would be accepted by lodash's `_.filter` method is accepted.
       *
       * @returns {Object}
       * An object that maps state name to is abbreviation, e.g.:
       * ```javascript
       * {
       *   "CA": "California",
       *   "NY": "New York",
       *   "TX": "Texas"
       * }
       * ```
       */
      nameMap: function(condition) {
        var _states = isProperCondition(condition) ? _.filter(states, condition) : states;
        return _.object(_.pluck(_states, 'state'), _.pluck(_states, 'name'));
      }
    });
  }]);
})();
