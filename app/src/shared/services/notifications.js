(function() {
  'use strict';

  angular.module('k41-shared').factory('notifications',
  ['$rootScope', '$q', '$interval', 'profile', 'rest',
  function($rootScope, $q, $interval, profile, rest) {

    var notifications = [], nextNotifications;
    var totalCount = 0;

    function getNotifications() {
    if (profile.get().loggedIn) {
      return rest.notifications.get()
        .then(function(result) {
          notifications = result.results;
          totalCount = result.count;

          _.each(notifications, function(notification) {
              notification.created_at = moment(notification.created_at, moment.ISO_8601).toDate();
            });

          $rootScope.$broadcast('notificationsUpdated', notifications);

          return _.freeze(notifications);
        });
    }
    return $q.reject();

  }

    if (profile.get().loggedIn && !nextNotifications) {
      getNotifications();
      // nextNotifications = $interval(getNotifications, config.notificationsPollingDelay);
    }

    // clear notifications when user logs out
    $rootScope.$on('loggedOut', function() {
      //$interval.cancel(nextNotifications);
      notifications = [];
      $rootScope.$broadcast('notificationsUpdated', notifications);
    });

    // get new notifications when user logs in
    $rootScope.$on('loggedIn', function() {
      $interval.cancel(nextNotifications);
      getNotifications();
      //nextNotifications = $interval(getNotifications, config.notificationsPollingDelay);
    });

    var styles = {
      normal: {'class': 'notification-success', iconClasses: 'fa fa-check'},
      user: {'class': 'notification-user', iconClasses: 'fa fa-user'},
      critical: {'class': 'notification-danger', iconClasses: 'fa fa-bolt'},
      warning: {'class': 'notification-warning', iconClasses: 'fa fa-warning'},
      fatal: {'class': 'notification-danger', iconClasses: 'fa fa-times'}
    };

    return _.freeze({
      style: function(notification) {
        return styles[notification.category] || styles.normal;
      },

      daysAgo: function(notification) {
        var days = Math.floor(moment().diff(notification.created_at, 'days'));
        return days ? days + ' days' : 'Today';
      },

      get: function() {
        return notifications;
      },

      getTotalCount: function() {
        return totalCount;
      },

      getNextPage: function(page) {
        if (profile.get().loggedIn) {
          return rest.notifications.get(null, {page: page})
            .then(function(result) {
              notifications = result.results;
              totalCount = result.count;

              _.each(notifications, function(notification) {
                notification.created_at = moment(notification.created_at, moment.ISO_8601).toDate();
              });

              return notifications;
            });
        }
        return $q.reject();
      },

      setViewed: function(ids) {
        $q.all(_.map(ids, function(id) {
          return rest.notifications.patch(id, {viewed: true});
        }))
          ['finally'](getNotifications);
      }
    });
  }]);
})();
