(function() {
  'use strict';

  angular.module('k41-shared')
  .factory('editable', ['$q', 'utils', 'rest', 'profile', function($q, utils, rest, profile) {

    function invalidateField(field, msg, errors) {
      errors[field] = true;
      return $q.reject(msg);
    }

    function resetErrors(errors, $form) {
      if ($form) {
        _.each($form.$editables, function(_editable) {
          errors[_editable.name.replace(/^.*\./, '')] = false;
        });
      } else {
        _.each(errors, function(error, key) {
          errors[key] = false;
        });
      }
    }

    function updateFieldSingle(name, id, base, field, value, errors, invalidMsg) {
      if (field) {
        if (_.isUndefined(value)) { // should cover all invalid values
          return invalidateField(field, invalidMsg || 'This field is invalid.', errors);
        }
        var _patch = {};
        if (base) {
          _patch[base] = {};
          _patch[base][field] = value;
        } else {
          _patch[field] = value;
        }
        return rest[name].patch(id, _patch)
          .then(function() {
            errors[field] = false;
            utils.successMsg('Data updated succesfully.');
          }, function(err) {
            var msg = 'There was an error updating data.';
            utils.errorMsg(msg);
            return invalidateField(field, err.data[field] ? err.data[field][0] : msg, errors);
          });
      } else {
        return $q.reject('Unknown field.');
      }
    }

    return {
      initErrors: function(scope, errorsField) {
        if (!scope[errorsField]) {
          scope[errorsField] = {};
        }
      },

      initUserRequiredFields: function(scope) {
        if (!scope.userReqFields) {
          scope.userReqFields = {
            job_title: {missing: false, waiting: false},
            department: {missing: false, waiting: false},
            start_date: {missing: false, waiting: false},
            work_location: {missing: false, waiting: false}
          };
        }
      },

      initEditableForm: function(obj, errors, $form, status) {
        $form.$onhide = function() {
          if (status) {
            _.each(status, function(field, key) {
              field.missing = utils.isValueMissing(obj[key]);
            });
          }
          resetErrors(errors, $form);
        };
      },

      updateFieldSingle: updateFieldSingle,

      resetErrors: resetErrors,

      // base function to deal with groups of required fields
      // eg. user
      //  - main details (name, job title, dept, location, start date)
      //  - address
      // name - name of API to use for patch
      // id - id to use for patch
      // base - name of nested property to patch or null/empty string (eg. user.address -> 'address')
      // field - name of property to patch
      // value - value of property
      // invalidMsg - error message when the value is invalid
      // obj - required only for grouped values, object with current values to use for patch
      // status - required only for grouped values, status object
      updateField: function(name, id, base, field, value, errors, invalidMsg, obj, status) {
        if (status && status[field]) {
          status[field].missing = utils.isValueMissing(value);
          if (status[field].missing) {
            // field is required and cannot be empty
            return invalidateField(field, 'This field cannot be empty.', errors);
          } else if (_.any(status, 'missing')) {
            // some required fields are not filled
            status[field].waiting = !utils.isValueMissing(value);
            return $q.when(true);
          } else if (_.any(status, 'waiting') || _.any(status, 'alwaysInclude')) {
            var request;

            if ('id' in obj) {
              var keys = _.chain(status).keys().filter(function(field) {
                  return status[field].waiting || status[field].alwaysInclude;
                }).value(),
                p = _.pick(base ? obj[base] : obj, keys),
                _patch;

              if (base) {
                _patch = {};
                _patch[base] = p;
                _patch[base][field] = value;
              } else {
                _patch = p;
                _patch[field] = value;
              }

              request = rest[name].patch(id, _patch);
            } else { //special case when whole record must be initialized first time
              request = rest[name].post(obj);
            }

            return request
              .then(function() {
                utils.successMsg('Data updated.');
                _.each(status, function(field, key) {
                  field.missing = field.waiting = errors[key] = false;
                });
              }, function(err) {
                _.each(err.data, function(msgs, errField) {
                  if (status[errField]) {
                    errors[errField] = true;
                  }
                });
                var msg = 'There was an error updating the data.';
                utils.errorMsg(msg);
                return $q.reject(msg);
              });
          }
        }

        // field can be saved by itself
        return this.updateFieldSingle(name, id, base, field, value, errors, invalidMsg);
      },

      updateUser: function(user, errors, field, value, status, invalidMsg, endpoint) {
        if (!endpoint) {
          endpoint = 'users';
        }
        return this.updateField(endpoint, user.id, null, field, value, errors, invalidMsg, user, status)
          .then(function() {
            if (user.id === profile.get().user.id) {
              profile.update(true);
            }
            return true;
          });
      },

      updateForm: function(data, errors, form, endpoint, fields, user, idField) {
        var promise;
        var requestData = _.chain({}).extend(data, form.$data).pick(fields).mapValues(function(value) {
          return _.isUndefined(value) ? null : value;
        }).value();
        idField = idField ? idField : 'id';
        if (idField in data) {
          promise = rest[endpoint].patch(data[idField], requestData);
        } else {
          promise = rest[endpoint].post(requestData);
        }

        return promise.then(function(response) {
            _.extend(data, response);
            if (user && user.id === profile.get().user.id) {
              profile.update(true);
            }
            resetErrors(errors, form);
            utils.successMsg('Data updated succesfully.');
            return response;
          }, function(error) {
            form.$show();
            _.each(error.data, function(msg, errField) {
              if (errField in form) {
                form.$setError(errField, msg[0]);
                errors[errField] = true;
              }
            });
            utils.errorMsg('There was an error updating the data.');
            return $q.reject(error);
          });
      }
    };
  }]);
})();
