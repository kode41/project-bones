(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name session
   * @module k41Shared
   *
   * @description
   * Service for managing user's logged in session. Saves and retrieves data from local storage.
   */
  angular.module('k41-shared').factory('session', [
    '$window', 'config',
    function($window, config) {
      var ls = $window.localStorage;

      var sessionServ = {};

      /**
       * @ngdoc method
       * @name session#start
       *
       * @param {number} userId - id of the user that just logged in
       * @param {number} maaId - id of maa that just logged in
       * @param {number} companyId - id of company that just logged in
       * @param {string} token - token to use in following requests
       *
       * @description
       * Saves user's session info into local storage.
       */
      sessionServ.start = function(userId, maaId, companyId, token) {
        ls.setItem(config.localStorageKey, JSON.stringify({
          loggedIn: true,
          userId: userId,
          companyId: companyId,
          token: token,
          timeStamp: Date.now()
        }));
      };

      /**
       * @ngdoc method
       * @name session#get
       *
       * @description
       * If session info exists in local storage and it has not yet expired then the saved data is returned.
       * Otherwise a default session info is returned with user considered not logged in.
       *
       * @return {Object} session data containing user and maa ids, token, timestamp and logged in flag
       */
      sessionServ.get = function() {
        var data = JSON.parse(ls.getItem(config.localStorageKey));
        if (data && ((Date.now() - data.timeStamp) * 0.001) < config.sessionTimeLimit) {
          return data;
        } else {
          return {
            loggedIn: false,
            userId: null,
            companyId: null,
            token: null,
            timeStamp: data ? data.timeStamp : 0
          };
        }
      };

      /**
       * @ngdoc method
       * @name session#ping
       *
       * @description
       * Refreshes current session info if it has not yet expired.
       */
      sessionServ.ping = function() {
        var data = sessionServ.get();
        if (data.loggedIn) {
          sessionServ.start(data.userId, data.companyId, data.token);
        }
      };

      /**
       * @ngdoc method
       * @name session#end
       *
       * @description
       * Ends user's session.
       * Resets the session data into default in local storage.
       */
      sessionServ.end = function() {
        ls.setItem(config.localStorageKey, JSON.stringify({
          loggedIn: false,
          userId: null,
          companyId: null,
          token: null,
          timeStamp: 0
        }));
      };

      return _.freeze(sessionServ);
    }
  ]);
})();
