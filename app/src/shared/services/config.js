(function() {
  'use strict';

  /**
   * @ngdoc object
   * @name config
   * @module k41Shared
   * @description
   * Contains global client-side configuration settings.
   * This module can be used as a dependency for configuration modules.
   */
  angular.module('k41-shared').constant('config', _.freeze({

    api: {
      host: 'YOURDOMAIN',
      path: '/api/' //API prefix
    },

    // Configuration information for error reporting to Sentry
    sentry: {
      bypass: '%SENTRY_BYPASS%'
    },

    localStorageKey: 'data',

    sessionTimeLimit: 1200,

    // approximation of 40 hours a week and 50 weeks in a year
    payRateConvert: 2000,

    // millisecond to wait between keystrokes to consider query complete
    typeSearchDelay: 300,

    // milliseconds to wait between polling requests
    pollingDelay: 5000,

    pollingStatus: {
      success: 'SUCCESS',
      pending: 'PENDING'
    },

    notificationsPollingDelay: 30000,

    defaultPageSize: 20,

    defaultPageSizeSmall: 10,

    userSearchMinLength: 2,

    //Limit number for pagination size.
    paginationSizeLimit: 10,

    signature: {
      image: {
        width: 400,
        height: 200
      }
    }
  }));
})();
