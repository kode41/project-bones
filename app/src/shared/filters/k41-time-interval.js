'use strict';

/**
 * @ngdoc filter
 * @name k41TimeInterval
 * @module k41Shared
 *
 * @param {number} hours - number of hours.
 * @param {string} precision - seconds, minutes or hours, default is minutes.
 *
 * @description
 * Uses `module:k41Shared.service:utils#elapsedHoursMinutes` or
 * `module:k41Shared.service:utils#elapsedHoursMinutesSeconds`
 * (depending on the precision parameter) to get elapsed hours and minutes and seconds from `hours`.
 *
 * @returns {string}
 * A string representation of hours and minutes.
 */
angular.module('k41-shared')
  .filter('emTimeInterval', ['utils', function(utils) {
    return function(hours, precision) {
      if (precision === 'seconds') {
        return utils.elapsedHoursMinutesSeconds(Math.floor(hours * 3600));
      } else if (precision === 'hours') {
        var hrs = Math.floor(hours);
        return hrs + (hrs > 1 ? ' hrs' : ' hr');
      } else {
        return utils.elapsedHoursMinutes(Math.floor(hours * 60));
      }
    };
  }]);
