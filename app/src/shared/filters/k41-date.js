'use strict';

/**
 * @ngdoc filter
 * @name  k41UtcDate
 * @module k41Shared
 * @param  {Date} value - A non-null, localized, date to filter into a UTC based date.
 * @return {Date} A UTC based date to eliminate timezone conversion issues.
 */
angular.module('k41-shared').filter('k41UtcDate', function() {
  return function(value) {
    if (value) {
      var tmp = new Date(value);
      return new Date(
        tmp.getUTCFullYear(),
        tmp.getUTCMonth(),
        tmp.getUTCDate(),
        tmp.getUTCHours(),
        tmp.getUTCMinutes(),
        tmp.getUTCSeconds()
      );
    }
    return null;
  };
});
