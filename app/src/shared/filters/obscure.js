(function() {
  'use strict';

  var OBSCURE_TYPE_ALL = 'all',
      OBSCURE_TYPE_PREFIX = 'prefix',
      OBSCURE_TYPE_SUFFIX = 'suffix';

  angular.module('k41-shared')
    /**
     * @ngdoc object
     * @name obscureConfig
     * @module emShared
     * @description
     * Default configuration and preset options for `obscure` filter. Presets are immutable.
     */
    .value('obscureConfig', {

      defaults: {
        match: /[a-z0-9]/ig,
        replace: '*',
        minLength: 0,
        type: OBSCURE_TYPE_ALL,
        typeArg: null
      },

      presets: {
        accountNumber: {
          match: /\d/g,
          minLength: 4,
          type: OBSCURE_TYPE_PREFIX,
          typeArg: -4
        },
        ssn: {
          match: /^[0-9*]{3}-[0-9*]{2}-(\d{4})$/,
          replace: '***-**-$1'
        }
      }
    })

    /**
     * @ngdoc filter
     * @name obscure
     * @module emShared
     *
     * @param {string} value - A value to obscure
     * @param {string|Object} [options] - Name of the preset to use or the options object specifying the behavior
     * @param {string|RegExp} [options.match=/a-z0-9/ig] - Defines what is replaced in the value
     * @param {string} [options.replace='*'] - Character to use as replacement
     * @param {number} [options.minLength=0] - Minimum length from which the value is obscured
     * @param {string} [options.type='all'] - Available values: `'all'`, `'prefix'`, `'suffix'`
     * @param {*} [options.typeArg=null] - Additional argument for selected `type`
     *
     * @description
     * The `obscure` filter takes input value and obscures some of it based on provided options.
     *
     * ## Types
     *
     * There are currently 3 types supported: `all`, `prefix`, and `suffix`. Other might be added later.
     *
     * **all**
     * Replaces all matching characters in the whole value. `typeArg` option is ignored.
     * > `'Employii 2.0' -> '******** *.*'`.
     *
     * **prefix**
     * Replaces all matching characters from beginning to the end point.
     * `typeArg` _number_ specifies the end point. Default value is 0.
     * Positive values to count from beginning, negative to count from end.
     * > for `typeArg` equal to **7** or **-5**
     * > `'Employii 2.0' -> '*******i 2.0'`.
     *
     * **suffix**
     * Replaces all matching characters from start point to the end.
     * `typeArg` _number_ specifies the start point. Default value is 0.
     * Positive values to count from beginning, negative to count from end.
     * > for `typeArg` equal to **7** or **-5**
     * > `'Employii 2.0' -> 'Employi* *.*'`.
     *
     * @returns {string}
     * Obscured string with specified parts of the original value replaced.
     * Original `value` if `options` refer to an unknown preset or type.
     */
    .filter('obscure', ['obscureConfig', function(obscureConfig) {

      // replaces all matching characters
      function obscureAll(value, options) {
        return value.replace(options.match, options.replace);
      }

      // replaces matching characters from start to `typeArg`
      function obscurePrefix(value, options) {
        var split = options.typeArg || 0;
        return value.slice(0, split).replace(options.match, options.replace) + value.slice(split);
      }

      // replaces matching characters from `typeArg` to end
      function obscureSuffix(value, options) {
        var split = options.typeArg || 0;
        return value.slice(0, split) + value.slice(split).replace(options.match, options.replace);
      }

      return function(value, options) {
        if (_.isEmpty(value) || !_.isString(value)) {
          return '';
        }

        if (_.isString(options)) {
          options = obscureConfig.presets[options];
          if (!options) {
            return value;
          }
        }

        options = _.extend({}, obscureConfig.defaults, options);

        if (value.length <= options.minLength) {
          return value;
        }

        switch (options.type) {
          case OBSCURE_TYPE_ALL:
            return obscureAll(value, options);
          case OBSCURE_TYPE_PREFIX:
            return obscurePrefix(value, options);
          case OBSCURE_TYPE_SUFFIX:
            return obscureSuffix(value, options);
          default:
            return value;
        }
      };

    }]);
})();
