'use strict';

/**
 * @ngdoc filter
 * @name k41Address
 * @module k41Shared
 *
 * @param {Object} [address] - address object
 * @param {string} [address.street_address_1] - street part of the address
 * @param {string} [address.street_address_2] - unit/suite part of the address
 * @param {string} [address.city] - city part of the address
 * @param {string} [address.state] - state part of the address, usually a 2-letter abbreviation
 * @param {string} [address.zipcode] - zipcode part of the address
 * @param {string} [defaultValue=''] - value to return if address cannot be retrieved
 *
 * @description
 * Transforms address object into a string separating its parts with ', '.
 * Falsy `defaultValue` is converted to empty string.
 *
 * @returns {string}
 * String representation of the address or `defaultValue` if it is falsy or none of its parts exist.
 */
angular.module('k41-shared')
  .filter('k41Address', function() {
    return function(address, defaultValue) {
      defaultValue = defaultValue || '';
      if (address) {
        return _.compact([
          address.street_address_1,
          address.street_address_2,
          address.city,
          address.state,
          address.zipcode
        ]).join(', ') || defaultValue;
      } else {
        return defaultValue;
      }
    };
  });
