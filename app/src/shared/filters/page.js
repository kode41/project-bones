'use strict';

/**
 * @ngdoc filter
 * @name page
 * @module k41Shared
 *
 * @param {Array|*} collection - a collection to get page from
 * @param {number} [current=1] - the page number to get, index is 1-based
 * @param {number} [size] - maximum number of items on a page
 *
 * @description
 * Uses `module:emShared.service:utils#page` to get a specified page / subset of a collection.
 *
 * @returns {Array|*}
 * A specified _page_ (subset) of `collection` if it is an array. The original value otherwise.
 */
angular.module('k41-shared')
.filter('page', ['utils', function(utils) {
  return utils.page; // (collection, current, size) { ... }
}]);
