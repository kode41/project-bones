'use strict';
/**
 * @ngdoc module
 * @name emShared
 * @description
 * The `k41-shared` module provides common code for the Employii web application.  This code will be shared between
 * modules, as well as between admin and employee level users.
 */
angular.module('k41-shared', ['restangular']);
