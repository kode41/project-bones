(function() {
  'use strict';

  angular.module('k41-shared').controller('SignupPageController',
  ['$scope', '$global', 'patterns', 'utils',
  function($scope, $global, patterns, utils) {

    $scope.signupData = {};
    $scope.patterns = patterns;

    $scope.submitted = false;
    $scope.showConfirmation = false;

    $scope.signupErrors = [];

    $global.set('fullpage', true);

    $scope.$on('$destroy', function() {
      $global.set('fullpage', false);
    });

    $scope.signupSubmit = function() {
      $scope.signupErrors = [];
      $scope.submitted = true;
      if ($scope.signup_form.$valid) {
        utils.successMsg('Successfully signed up!');
        $scope.showConfirmation = true;
      }
    };

    $scope.fieldHasError = function(field) {
      return {
        'has-error': $scope.submitted && field && field.$invalid
      };
    };

    $scope.showError = function(field, condition) {
      return !!field && field.$error[condition] && (field.$dirty || ($scope.submitted && field.$invalid));
    };

    $scope.closeError = function(idx) {
      if ($scope.signupErrors[idx]) {
        $scope.signupErrors.splice(idx, 1);
      }
    };

  }]);
})();
