(function() {
  'use strict';

  angular.module('k41-shared').controller('ResetPasswordController',
  ['$scope', '$state', '$global', '$stateParams', '$log', 'patterns', 'rest', 'profile',
  '$rootScope', 'errorManager', '$controller',
  function($scope, $state, $global, $stateParams, $log, patterns, rest, profile, $rootScope, errorManager,
    $controller) {
    var _this = this;
    angular.extend(this, $controller('ErrorWatchingController',
      {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

    $log.debug('Form:  ', $scope.form);
    $scope.patterns = patterns;

    $global.set('fullscreen', true);
    $scope.$on('$destroy', function() {
      $global.set('fullscreen', false);
    });

    $scope.data = {};
    $scope.loaded = false;
    $scope.loading = true;

    // get username, so we can log in once user changes password
    _this.watchCall(rest.resetPasswordUsers.get($stateParams.token), rest.resetPasswordUsers.route)
      .then(function(response) {
        _.extend($scope.data, response);
        $scope.loaded = true;
        $scope.loading = false;
      }, function() {
        $scope.loading = false;
        for (var idx in $scope.serverError.data) {
          $scope.customErrorList[$scope.serverError.data[idx]] = true;
        }
      });

    $scope.save = function() {
      $scope.submitted = true;

      // change password
      _this.watchCall(rest.resetPasswordUsers.patch($stateParams.token,
        _.pick($scope.data, ['password', 'password_confirmation'])), rest.resetPasswordUsers.route)
        .then(function() {
          // do login
          profile.logIn($scope.data.username, $scope.data.password)
            .then(function(p) {
              $state.go(p.loggedIn ? p.user.role : 'login');
            });
        });
    };

    $scope.cancel = function() {
      $state.go('login');
    };
  }]);
})();
