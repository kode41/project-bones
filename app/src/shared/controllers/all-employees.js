(function() {
  'use strict';

  angular.module('k41-admin')
  .controller('AllEmployeesController',
  ['$scope', '$state', '$timeout', 'profile', 'config', 'collections', '$modal', '$bootbox', 'utils',
  function($scope, $state, $timeout, profile, config, collections, $modal, $bootbox, utils) {

    collections.initPaging($scope);
    $scope.search = '';
    $scope.loading = false;

    $scope.state = $state.current;
    var _profile = profile.get();

    var searchTimer = null;

    $scope.getList = function() {
      $scope.loading = true;
      //get employees list for current page
      var params = {
        page: $scope.currentPage,
        is_active: $scope.activeEmployees
      };
      if (_.size($scope.search) >= config.userSearchMinLength) {
        params.search = $scope.search;
      }
      $scope.users = [
        {id: 1, first_name: 'John', last_name: 'Smith', email: 'john@mail.com', phone_number: '(569) 845-6988'},
        {id: 2, first_name: 'Mike', last_name: 'Johnson', email: 'mike@mail.com', phone_number: '(569) 845-6988'},
        {id: 3, first_name: 'Maria ', last_name: 'Rodriguez', email: 'maria@mail.com', phone_number: '(569) 845-6988'}
      ];
      $scope.loading = false;
    };

    // initialize employees list
    $scope.getList();

    $scope.seeDetails = function(user) {
      var uiModal = $modal.open({
        scope: $scope,
        templateUrl: 'src/shared/edit-user.html',
        size: 'md',
        controller: ['$scope', 'patterns', function($scope, patterns) {
          $scope.user = angular.copy(user);
          $scope.patterns = patterns;
          $scope.save = function(form) {
            $scope.submitted = true;
            if (form.$valid) {
              $scope.$close($scope.user);
            }

          };

          $scope.cancel = function() {
            $scope.$dismiss();
          };
        }]
      });

      uiModal.result.then(function(edited) {
        user.first_name = edited.first_name;
        user.last_name = edited.last_name;
        user.email = edited.email;
        user.phone_number = edited.phone_number;
      });
    };

    $scope.removeUser = function(user, idx) {
        $bootbox.confirm({
          title: 'Remove Client',
          message: '<p>Do you really want to remove user <strong>' +
          user.first_name + ' ' + user.last_name + '</strong>?</p>',
          buttons: {
            cancel: {
              label: 'Cancel',
              className: 'btn-lg btn-danger'
            },
            confirm: {
              label: 'Yes Remove',
              className: 'btn-lg btn-success'
            }
          },
          callback: function(confirmed) {
            if (confirmed) {
              $scope.users.splice(idx, 1);
              utils.successMsg('Successfully removed user.');
            }
          }
        });
      };

    $scope.searchEmployees = function() {
      if (searchTimer) {
        $timeout.cancel(searchTimer);
      }
      if (_.isEmpty($scope.search) || _.size($scope.search) >= config.userSearchMinLength) {
        searchTimer = $timeout(function() {
          $scope.currentPage = 1;
          $scope.getList();
        }, config.typeSearchDelay);
      }
    };

    $scope.isSelf = function(eid) {
      return _profile.user.id === eid;
    };

  }]);
})();
