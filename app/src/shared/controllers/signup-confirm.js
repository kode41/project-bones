(function() {
  'use strict';

  angular.module('k41-shared').controller('SignupPageDetailsController',
  ['$scope', '$global', 'patterns', '$state', '$stateParams', 'profile', 'rest',
    function($scope, $global, patterns, $state, $stateParams, profile, rest) {

      $global.set('fullscreen', true);
      $scope.$on('$destroy', function() {
        $global.set('fullscreen', false);
      });

      $scope.patterns = patterns;
      $scope.data = {};

      $scope.save = function() {
        if ($scope.form.$valid) {
          rest.usersSignup.patch($stateParams.token, $scope.data)
            .then(function() {
              return profile.logIn($scope.data.username, $scope.data.password);
            })
            .then(function(p) {
              $state.go(p.user.role);
            })
            .catch(function() {
              $scope.form.username.$setValidity('invalid', false);
            });
        }
      };

      $scope.hideUsernameError = function() {
        $scope.form.username.$setValidity('invalid', true);
      };
    }]);
})();
