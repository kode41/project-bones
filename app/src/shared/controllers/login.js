(function() {
  'use strict';

  angular.module('k41-shared').controller('LoginController',
  ['$scope', '$global', '$state', '$rootScope', '$modal', 'profile', 'errorManager', '$controller',
  function($scope, $global, $state, $rootScope, $modal, profile, errorManager, $controller) {
    var _this = this;
    $scope.userLogin = {};
    $scope.userLoginOriginal = angular.copy($scope.userLogin);
    angular.extend(this, $controller('ErrorWatchingController',
      {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

    $scope.reset = function() {
      $scope.userLogin = angular.copy($scope.userLoginOriginal);
    };

    $global.set('fullscreen', true);
    $scope.$on('$destroy', function() {
      $global.set('fullscreen', false);
    });

    $scope.loginSubmit = function(data) {
      _this.watchCall(profile.logIn(data.username, data.password), 'auth/')
        .then(function(p) {
          if (p.loggedIn) {
            $state.go(p.user.role);
          }
        }, function(response) {
          if ($scope.serverError.statusText === '') {
            $scope.serverError.statusText = 'An error occurred, unable to login';
          }
          $rootScope.logOutIfUnauthorized(response);
        });
    };

    // redirect to dashboard if user is already logged in
    var p = profile.get();
    if (p.loggedIn) {
      $state.go(p.user.role);
    }

    $scope.forgotPassword = function() {
      $modal.open({
        templateUrl: 'src/shared/forgot-password.html',
        controller: 'ForgotPasswordController'
      });
    };
  }]);
})();
