(function() {
  'use strict';
  //This controller is only used/initialized from the login.js and
  // inside a modal dialog
  angular.module('k41-shared').controller('ForgotPasswordController',
  ['$scope', '$rootScope', '$modalInstance', '$log', 'rest', 'patterns',
  function($scope, $rootScope, $modalInstance, $log, rest, patterns) {

    $scope.patterns = patterns;
    $scope.data = {};

    //hide irrelevant error messages on the modal contents change
    $scope.hideError = function() {
      $scope.error = false;
    };

    $scope.send = function() {
      $scope.submitted = true;

      $log.debug($scope.resetForm);

      if ($scope.resetForm.$valid) {
        rest.resetPasswordUsers.post($scope.data)
          .then(function() {
              $scope.sent = true;
            },
            function() {
              $scope.error = true;
            }
          );
      }
    };

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };

  }]);
})();
