(function() {
  'use strict';
  angular.module('k41-shared').controller('InstructionalController', [
    '$location', '$scope', 'instructional-text', '$state',
    function($location, $scope, instrServ, $state) {
      if ($scope.source) {
        // if source specified in directive then use just that related text
        $scope.instructionalArray = instrServ.getText($scope.source);
      } else {
        // otherwise keep updated according to current location / state
        $scope.instructionalArray = instrServ.getText($location.$$url) || instrServ.getText($state.current.name);

        $scope.$on('$stateChangeSuccess', function(event, toState) {
          $scope.instructionalArray = instrServ.getText($location.$$url) || instrServ.getText(toState.name);
        });
      }
    }
  ]);
})();
