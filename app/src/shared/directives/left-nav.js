(function() {
  'use strict';

  var mod = angular.module('k41-shared');

  mod.controller('K41LeftNavController',
    ['$scope', 'profile', '$state',
      function($scope, profile, $state) {
        var itemsByState = {},
          statesByName = _.indexBy($state.get(), 'name'),
          selectedItem;
        var p = profile.get();

        //
        $scope.toggleClicked = function(item) {
          var curElem = $('.em-navmenu-div.open');
          var toggledItem = $('#navmenu-' + item.state);
          if (curElem[0] !== toggledItem[0]) {
            curElem.removeClass('open');
            toggledItem.addClass('open');
          } else if (curElem.hasClass('open')) {
            curElem.removeClass('open');
          } else {
            curElem.addClass('open');
          }
          var curElemId = curElem.attr('id');
          if (!curElemId) {
            // check to see if there is an active element.
            curElem = $('.em-navmenu-div.active');
            curElemId = curElem ? curElem.attr('id') : null;
          }
          if (curElemId) {
            if (!statesByName[curElemId.substring(8)] || !statesByName[item.state] ||
              statesByName[curElemId.substring(8)].parent !== statesByName[item.state].parent) {
              $state.go(item.state);
            }
          } else {
            $state.go(item.state);
          }
        };

        $scope.userSatisfiesNavCondition = function(navCond) {
          if (navCond === undefined) {
            return true;
          } else if (typeof(navCond) === 'boolean') {
            return navCond;
          }
          var negated = (navCond.charAt[0] === '!');
          var toReturn = false;
          toReturn = !!p.user[navCond];
          toReturn = negated ? !toReturn : toReturn;

          return toReturn;
        };

        function checkStateOptions(item, profile) {
          if (item.options) {
            var enabled = (item.options.length === 0 || _.all(item.options, function(option) {
              return profile.company[option + '_enabled'];
            }));
            return enabled;
          } else {
            return true;
          }
        }

        // generate menu data from state data
        function createMenu(include, parent) {
          return _.reduce($state.get(), function(memo, state) {
            var data = state.data || {},
              stateParent = data.parent || state.parent;
            if (data.nav && ((!parent && !stateParent) || stateParent === parent) && include(state)) {
              var item = {
                parent: itemsByState[parent],
                label: data.label || data.title || state.name,
                enabled: checkStateOptions(data, p),
                iconClasses: data.icon ? 'fa fa-' + data.icon : '',
                state: state.name,
                options: data.options || [],
                url: state.url,
                navCond: data.nav,
              };

              memo.push(item);
              itemsByState[item.state] = item;
              var children = createMenu(include, state.name);
              if (children.length > 0) {
                item.children = children;
              }

              if (state.abstract && children.length > 0) {
                item.state = children[0].state;
              }
            }
            return memo;
          }, []);
        }

        $scope.adminMenu = createMenu(function(state) {
          return state.data &&
            _.contains(state.data.userRoles, 'admin') && !_.contains(state.data.userRoles, 'user') &&
            $scope.featureDisabledState[state.feature || 'base'] !== 'hidden';
        }, 'main');

        function isEmployeeState(state) {
          return state.data && _.contains(state.data.userRoles, 'user') &&
            $scope.featureDisabledState[state.feature || 'base'] !== 'hidden';
        }

        $scope.employeeMenu = createMenu(isEmployeeState, 'main');

        //toggle between employee mode and admin mode
        $scope.switchMode = function() {
          $scope.$root.adminMode = !$scope.$root.adminMode;
          if ($scope.$root.adminMode) {
            $scope.menu = $scope.adminMenu;
            $state.go('admin');
          } else {
            $scope.menu = $scope.employeeMenu;
            $state.go('user');
          }
        };

        function setEnabled(profile) {
          _.each(itemsByState, function(item) {
            item.enabled = checkStateOptions(item, profile);
          });
        }

        // select/unselect item in menu
        function setSelected(selectedItem, selected) {
          var item = selectedItem;
          while (item) {
            item.selected = selected;
            item = item.parent;
          }
        }

        function findItem(selectedState) {
          var state = selectedState,
            item = itemsByState[state.name];

          while (!item && (state.data.parent || state.parent)) {
            state = statesByName[state.data.parent || state.parent];
            item = itemsByState[state.name];
          }

          if (state.abstract && item && item.children) {
            item = item.children[0];
          }

          return item;
        }

        // update menu according to state change
        function stateChanged(event, toState) {
          var p = profile.get();

          $scope.isAdmin = p.user.isAdmin;

          setSelected(selectedItem, false);


          var item = findItem(toState);
          if (item) {
            selectedItem = item;
            setSelected(selectedItem, true);
          }

          var isEmployee = isEmployeeState(toState);
          if (p.loggedIn) {
            $scope.menu = $scope[(isEmployee ? 'employee' : 'admin') + 'Menu'];
          } else {
            $scope.menu = [];
          }
          $scope.$root.adminMode = !isEmployee;
        }

        $scope.signingUp = function() {
          return $state.is('signup');
        };

        function profileChanged(event, np) {
          p = np;
          setEnabled(p);
          $scope.isMAA = p.user.isMAA;
        }

        $scope.navInit = function() {
          // set initial selection on page load
          stateChanged(null, $state.current);

          // update selection when state changes
          $scope.$on('$stateChangeSuccess', stateChanged);

          // set initial enabled menu items on page load
          profileChanged(null, profile.get());

          // update enabled menu items when profile changes
          $scope.$on('profileChanged', profileChanged);
        };
      }]);

  mod.directive('k41LeftNav',
    [function() {
      return {
        restrict: 'E',
        replace: true,
        // attributes:
        scope: true,
        templateUrl: 'src/shared/directives/left-nav.html',
        controller: 'K41LeftNavController',
        link: function(scope) {
          scope.navInit();
        }
      };
    }
    ]);
})();
