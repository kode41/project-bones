'use strict';

angular.module('k41-shared').directive('k41Wizard', ['$window', function($window) {
  return {
    restrict: 'E',
    scope: {
      step: '=',
      totalSteps: '=?'
    },
    replace: true,
    transclude: true,
    templateUrl: 'src/shared/directives/wizard.html',
    link: function(scope, element) {
      var elements = element.find('fieldset');

      scope.totalSteps = elements.length;

      scope.steps = _.map(elements, function(element) {
        var e = $(element), legend = e.find('legend');
        legend.hide();
        e.hide();
        return {
          title: element.title,
          legend: legend.length > 0 ? legend.text() : '',
          element: e
        };
      });

      function selectStep(idx, select) {
        var step = scope.steps[idx];
        step.active = select;
        step.element[select ? 'show' : 'hide']();
      }

      scope.$watch('step', function(newVal, oldVal) {
        if (newVal > 0 && newVal <= scope.steps.length) {
          selectStep(oldVal - 1, false);
          selectStep(newVal - 1, true);
          $window.scrollTo(0, 0);
        }
      });

      // nav buttons are more trouble than they're worth
      // - lets just hide them and use our own
      element.find('.stepy-navigator').hide();
    }
  };
}]);
