'use strict';

angular.module('k41-shared').directive('k41SignaturePad',
  ['$timeout', function($timeout) {
    return {
      restrict: 'E',
      require: 'ngModel',
      replace: true,
      // attributes:
      //  options: optional parameter, options object to use to initialize SignaturePad object
      scope: {options:'=?'},
      template: '<canvas></canvas>',

      link: function(scope, element, attrs, ngModel) {
        /* global SignaturePad */

        var canvas = element[0],
          signaturePad,
          drew = false;

        function updateModel() {
          $timeout(function() {
            var uri = signaturePad.toDataURL();
            drew = true;
            ngModel.$setViewValue(uri);
          });
        }
        signaturePad = new SignaturePad(canvas, _.extend({onEnd: updateModel}, scope.options)),

        scope.$watch(
          function() {
            return ngModel.$modelValue;
          },
          function() {
            if (!drew) {
              if (ngModel.$viewValue) {
                signaturePad.fromDataURL(ngModel.$viewValue);
              } else {
                signaturePad.clear();
              }
            } else {
              drew = false;
            }
          }
        );

      }
    };
  }
  ]);
