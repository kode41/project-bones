'use strict';

angular.module('k41-shared').directive('k41Breadcrumb',
['$state', function($state) {
    return {
      restrict: 'E',
      replace: true,
      // attributes:
      //  title: override the title of current page. by default the site-map title value is used
      //  heading: override the heading of current page. by default the title value is used
      //  hideInstructions: forces instructions to be hidden even if some should be displayed
      scope: {
        title: '@',
        heading: '@',
        isManager: '=',
        hideInstructions: '='
      },
      templateUrl: 'src/shared/directives/breadcrumb.html',
      link: function(scope) {

        scope.isActive = function(item) {
          return item.name === scope.current().name;
        };

        scope.isCurrent = function(item) {
          return item === _.last(scope.items);
        };

        scope.current = function() {
          return _.last(scope.items);
        };

        function createItem(state) {
          return {
            name: state.abstract ?
              _.find($state.get(), function(s) { return s.parent === state.name; }).name :
              state.name,
            title: state.data.title
          };
        }

        function addItem(state) {
          if (state.data.title && state.data.breadcrumb !== false) {
            scope.items.unshift(createItem(state));
          }
        }

        function stateChanged() {
          var state = $state.current,
            parent = state.data.parent || state.parent;

          scope.items = [];
          addItem(state);

          while (parent) {
            state = $state.get(parent);
            addItem(state);
            parent = state.data.parent || state.parent;
          }
        }
        stateChanged();

        scope.$on('$stateChangeSuccess', stateChanged);
      }
    };
  }
]);
