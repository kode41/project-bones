(function() {
  'use strict';

  /**
   * @ngdoc directive
   * @name emInstructionalText
   * @module emShared
   * @description
   * The emInstructionalText directive creates an instructional text section on the current page which will
   * self-populate with the appropriate instructional text for the section, if any exists.
   *
   * @scope
   */
  angular.module('k41-shared').directive('k41InstructionalText', function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {source: '@?'},
      controller: 'InstructionalController',
      templateUrl: 'src/shared/directives/instructional-text.html'
    };
  });
})();
