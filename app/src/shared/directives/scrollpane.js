(function() {
  'use strict';

  angular.module('k41-shared').directive('k41Scrollpane',
  ['$timeout', function($timeout) {
    return {
      restrict: 'E',
      // attributes:
      //  options: optional options (object) used to configure jscrollpane
      //  update: optional boolean flag. Set to true when content changes, to reinitialise scroll panel
      scope: {options: '=?', update: '=?'},
      transclude: true,
      template: '<div class="scrollthis"><div ng-transclude></div></div>',
      replace: true,
      link: function(scope, element) {

        element.jScrollPane(scope.options);

        var api = element.data('jsp');

        function update() {
          if (api) {
            $timeout(function() {
              element.jScrollPane(scope.options);
              $('.scrollthis').css('width', '320px');
              $('.jspContainer').css('width', '320px');
            });
          }
        }

        update();

        scope.$watch('options', update, true);

        scope.$watch('update', function(newVal) {
          if (newVal) {
            scope.update = false;
            update();
          }
        });
      }
    };
  }]);
})();
