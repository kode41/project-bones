'use strict';

angular.module('k41-shared').directive('k41ServerErrors', ['utils',
  function(utils) {
    return {
      restrict: 'E',
      transclude: true,
      scope: {result:'=', paths:'=?'},
      templateUrl: 'src/shared/directives/server-errors.html',
      link: function(scope) {
        scope.$watch('result', function() {
          scope.errors = utils.serverErrors(scope.result, scope.paths);
        });
      }
    };
  }
]);
