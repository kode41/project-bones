'use strict';

angular.module('k41-shared').directive('k41CompareTo', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      emCompareTo: '='
    },
    link: function(scope, element, attributes, ngModel) {

      function validate(value) {
        ngModel.$setValidity('compareTo', !value || !scope.emCompareTo || value === scope.emCompareTo);
        return value;
      }

      //For DOM -> model validation
      ngModel.$parsers.unshift(validate);

      //For model -> DOM validation
      ngModel.$formatters.unshift(validate);

      scope.$watch('emCompareTo', function() {
        validate(ngModel.$modelValue);
      });
    }
  };
});
