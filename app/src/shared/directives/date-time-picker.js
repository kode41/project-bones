(function() {
  'use strict';


  angular.module('k41-shared').directive('k41DateTimePicker',
  ['dateParser', function(dateParser) {
    return {
      restrict: 'E',
      require: 'ngModel',
      // attributes:
      //  disabledFn: optional function to disable dates
      //  options: date picker options + extra property time
      //   specifies whether or not to allow setting the time
      scope: {
        disabledFn:'&',
        options:'=?',
        ngMinDate:'=?',
        ngMaxDate:'=?',
        ngRequired:'=?',
        ngDisabled:'=?',
        ngChanged:'=?',
        emDatePattern:'=?'
      },
      templateUrl: 'src/shared/directives/date-time-picker.html',
      controller: ['$scope', function($scope) {
        $scope.dateOptions = _.extend({
          startingDay: 0,
          time: false,
          showButtonBar: true,
          showSeconds: false,
          datepickerAppendToBody: false
        }, $scope.options);
      }],
      link: function(scope, element, attr, ngModel) {
        var data = scope.data = {};

        scope.$watch(function() {
              return ngModel.$modelValue;
            },
            function() {
              data.date = ngModel.$modelValue;
            }
          );

        scope.validateDate = function() {
            if (scope.ngMinDate) {
              ngModel.$setValidity('minDate',
                !moment(scope.ngMinDate).isAfter(ngModel.$modelValue));
            }
            if (scope.ngMaxDate) {
              ngModel.$setValidity('maxDate',
                !moment(scope.ngMaxDate).isBefore(ngModel.$modelValue));
            }
          };

        scope.$watchGroup(['ngMinDate', 'ngMaxDate'], function() {
            scope.validateDate();
          });

        if (!scope.dateOptions.time) {
          if (!scope.emDatePattern) {
            scope.emDatePattern = 'yyyy-MM-dd';
          }
          scope.datepickerElement = element.find('input.datepicker')[0];
          ngModel.$validators.pattern = function() {
            return _.isEmpty(scope.datepickerElement.value) ||
              !!dateParser.parse(scope.datepickerElement.value, scope.emDatePattern, data.date);
          };
        }

        scope.$watch(
          'data.date',
          function() {
            ngModel.$setViewValue(data.date);
            scope.validateDate();
          }
        );

        scope.dateDisabled = function(date, mode) {
          return ('disabledFn' in attr) ? scope.disabledFn({date: date, mode: mode}) : false;
        };

        scope.picker = function($event, state) {
            $event.preventDefault();
            $event.stopPropagation();
            scope.opened = state;
          };

        scope.togglePicker = function($event) {
            scope.picker($event, !scope.opened);
          };
      }
    };
  }]);
})();
