(function() {
  'use strict';

  /**
   * @ngdoc directive
   * @name emObscure
   * @module emShared
   *
   * @description
   * The `emObscure` directive obscures the value on blur and restores it on focus.
   * It should be used mainly on `input[text]` and similar controls.
   *
   * @restrict A
   * @requires module:emShared.filter:obscure
   * @scope
   */
  angular.module('k41-shared').directive('k41Obscure', [
    '$filter',
    function($filter) {
      return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
          emObscure: '='
        },
        link: function(scope, element, attrs, ngModel) {
          var modelValueCopy = _.clone(ngModel.$modelValue);
          var validatePattern = ngModel.$validators.pattern;
          var obscured = false;

          function obscureViewValue() {
            if (ngModel.$valid) {
              obscured = true;
              modelValueCopy = _.clone(ngModel.$modelValue);
              // affect only view, do not propagate elsewhere
              ngModel.$viewValue = $filter('obscure')(modelValueCopy, scope.emObscure);
              ngModel.$render();
            }
          }

          function restoreViewValue() {
            if (ngModel.$valid) {
              obscured = false;
              // making sure it is properly restored with all parsers, formatters and other stuff applied
              ngModel.$viewValue = modelValueCopy;
              ngModel.$render();
            }
          }

          //override a pattern validation to validate only model value if
          // element was obscured
          ngModel.$validators.pattern = function(modelValue, viewValue) {
            var valid = validatePattern(modelValue, viewValue);
            if (!valid) {
              modelValue = obscured ? modelValueCopy : modelValue;
              viewValue = modelValue;
              valid = validatePattern(modelValue, viewValue);
            }
            return valid;
          };


          element.on('blur', obscureViewValue);

          element.on('focus', restoreViewValue);

          // make sure the value gets obscured on initialization if there is any
          var initWatcher = scope.$watch(
            function() { return ngModel.$modelValue; },
            function(val) {
              if (val) {
                if (ngModel.$pristine) {
                  obscureViewValue();
                  ngModel.$setPristine();
                }
                initWatcher();
              }
            }
          );

        }
      };
    }
  ]);
})();
