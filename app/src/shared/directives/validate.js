'use strict';

angular.module('k41-shared').directive('k41Validate', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      var validators = scope.$eval(attrs.emValidate);

      function validate(value) {
        var valid = true;
        _.each(validators, function(validator) {
          if (valid) {
            valid = validator.evaluator(value);
            ngModel.$setValidity(validator.name, valid);
            return;
          }
          ngModel.$setValidity(validator.name, true);
        });

        return value;
      }

      //For DOM -> model validation
      ngModel.$parsers.unshift(validate);

      //For model -> DOM validation
      ngModel.$formatters.unshift(validate);
    }
  };
});
