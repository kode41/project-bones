(function() {
  'use strict';

  angular.module('k41-shared').directive('k41LoaderOverlay',
  function() {
    return {
      templateUrl: function(element, attrs) {
        if (attrs.hasOwnProperty('transparent')) {
          return 'src/shared/directives/loader-overlay-transparent.html';
        } else {
          return 'src/shared/directives/loader-overlay' + (attrs.useIf ? '-if' : '') + '.html';
        }
      },
      restrict: 'E',
      transclude: true,
      scope: {
        message: '=',
        inProgress: '=',
        bgColor: '=?'
      }
    };
  });
})();
