(function() {
  'use strict';
  var module = angular.module('k41-settings');

  module.controller('K41Controller',
    ['$scope', '$rootScope', '$controller', 'errorManager',
    function($scope, $rootScope, $controller, errorManager) {
      var self = this; //Controller as syntax
      angular.extend(self, $controller('ErrorWatchingController',
        {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

    }]);
})();
