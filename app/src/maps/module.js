(function() {
  'use strict';
  /**
   * @ngdoc module
   * @name  k41Maps
   * @description
   * The k41-maps module provides functionality for utilizing maps as part of web pages
   * in the Employii web application.  This functionality includes embedding a Google Map, with
   * location Markers, geocoding a street address into latitude / longitude pairs and initializing
   * the Google Maps Javascript API, v3.
   */
  angular.module('k41-maps', []);
})();
