(function() {
  'use strict';
  /* global google */
  var mapMod = angular.module('k41-maps');

  /**
   * @ngdoc service
   * @name  GoogleMaps
   * @module k41Maps
   * @description
   * The GoogleMapsService provides functionality to permit the easy usage of Google maps
   * in an AngularJS application.  This includes setting up the appropriate callback functionality
   * for dynamically loading the google maps APIs, etc.
   *
   * @return {Object} - The GoogleMapsService for AngualrJS web applications
   */
  mapMod.factory('GoogleMaps', ['rest', '$state', function(rest, $state) {
    var mapsService = {
      map: null,
      marker: null,
      addressInfo: null,
      stringAddress: null,
      mapListener: null
    };
    var defaultMapOptions = {
      center: {lat: 45, lng: -73},
      zoom: 12,
    };
    var defaultMarkerOptions = {
      draggable: true,
      clickable: false,
      title: 'Current Location'
    };
    /**
     * @ngdoc method
     * @name  GoogleMaps#setAddressInfo
     * @description
     * Sets the address information object which will be used to perform operations on the map. Any
     * existing string representation of the address will be removed when the address is set.
     *
     * @param {Object} ainfo - An Employii address object.
     */
    mapsService.setAddressInfo = function(ainfo) {
      mapsService.addressInfo = ainfo;
      mapsService.stringAddress = null;
    };
    /**
     * @ngdoc method
     * @name  GoogleMaps#getAddressInfo
     * @description
     * Retrieves the address information object, if any, that will be used to perform operations on the map.
     * @return {Object} The address information object.
     */
    mapsService.getAddressInfo = function() {
      return mapsService.addressInfo;
    };
    /**
     * @ngdoc method
     * @name  GoogleMaps#getStringAddress
     * @description
     * Retrieves the string representation of the address information object.
     * @return {string} The string representation of the current address information object.
     */
    mapsService.getStringAddress = function() {
      if (!mapsService.stringAddress && mapsService.addressInfo) {
        var addr = mapsService.addressInfo;
        var address = addr.street_address_1;
        if (addr.street_address_2 && addr.street_address_2.length > 0) {
          address += ', ' + addr.street_address_2;
        }
        address += ', ' + addr.city + ', ' + addr.state + ', ' + addr.zipcode;
        mapsService.stringAddress = address;
      }

      return mapsService.stringAddress;
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#createMap
     * @description
     * Creates a new map to be held / maintained by this service for the purpose of adding markers, etc.
     * If there is any pre-existing map, it will be removed, along with any listeners and markers associated with
     * it in preparation for the creation of a new map.
     *
     * @param  {HttpElement} element - The element to put the new map into.
     * @param  {google.maps.LatLng} latLong - The latitude and longitude to use to center the map.
     */
    mapsService.createMap = function(element, latLong) {
      var opts = angular.copy(defaultMapOptions);
      if (mapsService.map) {
        mapsService.removeMap();
      }
      if (latLong) {
        opts.center = latLong;
      }
      mapsService.map = new google.maps.Map(element, opts);
      mapsService.mapListener = google.maps.event.addListener(mapsService.map, 'click', mapsService.clickListener);
      return mapsService.map;
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#clickListener
     * @description
     * The click listener drops a pin on the map at the latitude / longitude under the mouse cursor when a
     * user clicks the map.
     *
     * @param  {Object} event - A MouseEvent from a google map instance.
     */
    mapsService.clickListener = function(event) {
      if (mapsService.hasMarker()) {
        mapsService.updateMarker(event.latLng);
      } else {
        mapsService.createMarker(event.latLng);
      }
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#hasMap
     * @description
     * Returns whether or not the service currently contains a map instance.
     *
     * @return {Boolean} True if the service currently holds a map, false otherwise.
     */
    mapsService.hasMap = function() {
      return (mapsService.map ? true : false);
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#getMap
     * @description
     * Returns the map instance currently held by the service or null if no
     * such instance exists.
     * @return {Object} The map instance or null if no map instance exists.
     */
    mapsService.getMap = function() {
      return mapsService.map;
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#removeMap
     * @description
     * Removes the current instance of the map held by this service, as well as any
     * listeners active on it and any markers placed on it.
     */
    mapsService.removeMap = function() {
      if (mapsService.map) {
        google.maps.event.removeListener(mapsService.mapListener);
        delete mapsService.map;
      }
      if (mapsService.marker) {
        mapsService.marker.setMap(null);
        delete mapsService.marker;
      }
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#updateMapPosition
     * @param  {google.map.LatLng} latLong - The new latitude and longitude for the center of the map.
     * @description
     * Moves the center of the map to the specified latitude and longitude.
     */
    mapsService.updateMapPosition = function(latLong) {
      mapsService.map.panTo(latLong);
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#hasMarker
     * @description
     * Returns whether or not there are currently any markers on this map.
     * @return {Boolean} True if there is a marker, false otherwise.
     */
    mapsService.hasMarker = function() {
      return (mapsService.marker ? true : false);
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#getMarker
     * @description
     * Returns the current marker instance for this map.
     *
     * @return {google.maps.Marker} The Marker which is currently on the map or null if no such marker exists.
     */
    mapsService.getMarker = function() {
      return mapsService.marker;
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#updateMarker
     * @description
     * Moves the existing Marker instance to the specified latitude and longitude.
     *
     * @param  {google.maps.LatLng} latLong - The new latitude and longitude for the existing marker.
     */
    mapsService.updateMarker = function(latLong) {
      mapsService.marker.setPosition(latLong);
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#createMarker
     * @description
     * Creates a new Marker instance at the specified latitude and longitude.
     *
     * @param  {google.maps.LatLng} latLong - The new latitude and longitude for the marker.
     */
    mapsService.createMarker = function(latLong) {
      var opts = angular.copy(defaultMarkerOptions);
      opts.position = latLong;
      opts.draggable = true;
      opts.title = 'Current Location';
      opts.map = mapsService.map;
      mapsService.marker = new google.maps.Marker(opts);
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#addCallback
     * @description
     * Adds the supplied root callback name to the window object and has it call _callback_.  Once this callback has
     * been called, it will delete itself from the window scope.
     *
     * @param {string} rootName - The name of the function which should be called once the google scripts are loaded.
     * @param {Function} callback - The callback function to call once the google maps service has loaded.
     * @return {boolean} True if the callback could be added, false if the callback already existed.
     */
    mapsService.addCallback = function(rootName, callback) {
      window[rootName] = function() {
        callback();
      };
    };

    function createGeocoder() {
      mapsService.geocoder = new google.maps.Geocoder();
    }

    /**
     * @ngdoc method
     * @name  GoogleMaps#geolocateAddress
     * @param  {string} address    - The string representation of the address to geo-locate.
     * @param  {Function} callback - The callback function to use as the callback from the geocoding attempt.
     * @description
     * Attempts to use the Google geocoding service to translate the provided `address` value into a
     * Latitude/Longitude pair for use in geocoding the provided location.
     *
     * The `callback` function must have the signature:
     * ```js
     * function callback(result, status)
     * ```
     * where the corresponding
     * [Geocoding Result](https://developers.google.com/maps/documentation/javascript/geocoding#GeocodingResults)
     * object will be passed as result with status having a value of `google.maps.GeocoderStatus.OK` or the error
     * message if the geocoding failed.
     */
    mapsService.geolocateAddress = function(address, callback) {
      if (!mapsService.geocoder) {
        createGeocoder();
      }
      mapsService.geocoder.geocode({address: address}, callback);
    };

    /**
     * @ngdoc method
     * @name  GoogleMaps#addressLookup
     * @param  {google.maps.LatLng}   latlong  - The Latitude/Longitude pair to reverse geocode.
     * @param  {Function} callback - The callback function to pass the return of the reverse geocoding.
     * @description
     * Attempts to use the Google geocoding service to translate the provided `latlong` value into a
     * human readable street address for use in geocoding the provided location.
     *
     * The `callback` function must have the signature:
     * ```js
     * function callback(result, status)
     * ```
     * where the corresponding
     * [Geocoding Result](https://developers.google.com/maps/documentation/javascript/geocoding#GeocodingResults)
     * object will be passed as result with status having a value of `google.maps.GeocoderStatus.OK` or the error
     * message if the reverse geocoding failed.
     */
    mapsService.addressLookup = function(latlong, callback) {
      if (!mapsService.geocoder) {
        createGeocoder();
      }
      mapsService.geocoder.geocode({latLng:latlong}, callback);
    };

    // This following bit is horrible and should be split into its own service, however,
    // in the interest of being able to release, leaving for now, with this todo:
    // TODO:  Split into own service, see if there is any way to fix return path retrieval.
    mapsService.returnToPrevious = function($stateParams) {
      if (mapsService.geolocationType === 'workGeolocate') {
        $state.go('locations');
      } else if (mapsService.geolocationType === 'userGeolocation') {
        $state.go('profileDetailsGeneral');
      } else {
        $state.go('employeeDetailsGeneral', {id: $stateParams.id});
      }
    };

    mapsService.setAddressId = function(id) {
      mapsService.addressId = id;
    };

    mapsService.setType = function(type) {
      mapsService.geolocationType = type;
    };

    mapsService.loadAddress = function(callback) {
      if (mapsService.geolocationType === 'workGeolocate') {
        rest.workLocations.get(mapsService.addressId)
          .then(function(response) {
            mapsService.setAddressInfo(response.address);
            mapsService.getStringAddress();
            callback();
          });
      } else {
        rest.users.get(mapsService.addressId)
          .then(function(response) {
            mapsService.setAddressInfo(response.address);
            mapsService.getStringAddress();
            callback();
          });
      }
    };

    return mapsService;
  }]);

})();
