(function() {
  /* global google */
  /* global Raven */
  'use strict';
  var mapMod = angular.module('k41-maps');

  /**
   * @ngdoc controller
   * @name  GeolocationController
   * @module emMaps
   * @param  {$rootScope.Scope} $scope      - The basic scope for this controller.
   * @param  {ng.$log} $log                 - The AngularJS logging service.
   * @param  {emMaps.GoogleMaps} GoogleMaps - The GoogleMaps service.
   * @param  {emShared.rest} rest           - The rest service for posting to Employii endpoints.
   * @param  {ng.$raven} $raven - The AngularJS raven service.
   * @description
   * Provides control functions for geolocation functionality in the Employii web application.
   */
  mapMod.controller('GeolocationController', ['$scope', '$rootScope', '$log', 'GoogleMaps', 'rest', '$raven',
    '$timeout', 'utils', '$stateParams', '$state', 'errorManager', '$controller',
    function($scope, $rootScope, $log, GoogleMaps, rest, $raven, $timeout, utils, $stateParams, $state,
      errorManager, $controller) {
      var self = this;
      angular.extend(self, $controller('ErrorWatchingController', {
        $rootScope: $rootScope,
        $scope: $scope,
        errorManager: errorManager
      }));
      $scope.mapServ = GoogleMaps;

      /**
       * @ngdoc method
       * @name  GeolocationController#updateMarker
       * @description
       * **INTERNAL**
       *
       * Creates a new `google.maps.Marker` if no current Marker exists.  On an existing
       * or new Marker, then sets the position to be the current geo location according to lat/long.
       */
      function updateMarker(latLong) {
        if (GoogleMaps.hasMarker()) {
          GoogleMaps.updateMarker(latLong);
        } else {
          GoogleMaps.createMarker(latLong);
        }
      }

      /**
       * @ngdoc method
       * @name  GeolocationController#geolocationCallback
       * @param  {GeolocationResults} results - The results from a Google Maps geolocation attempt.
       * @param  {GeocoderStatus} status  - The status of the Google Maps geocoding attempt.
       * @description
       * **INTERNAL**
       *
       * Waits for a callback from the
       * [Google Geocoder](https://developers.google.com/maps/documentation/javascript/geocoding) service
       * and then acts upon the results to populate the latitude and longitude of an address.
       */
      $scope.geolocationCallback = function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var loc = results[0].geometry.location;
          if (GoogleMaps.hasMap()) {
            GoogleMaps.updateMapPosition(loc);
          } else {
            GoogleMaps.createMap(document.getElementById('map_canvas'), loc);
          }
          updateMarker(loc);
        } else {
          var data = {};
          switch (status) {
            case google.maps.GeocoderStatus.ZERO_RESULTS:
              data.msg = 'No results Returned';
              data.address = GoogleMaps.getStringAddress();
              $raven.captureMessage('Employii Geocoding Exception', {extra: data});
              $scope.geolocationError = 'The address ' + GoogleMaps.getStringAddress();
              break;
            case google.maps.GeocoderStatus.OVER_QUERY_LIMIT:
              data.msg = 'Geocoding Request Denied - Over Quota';
              $raven.captureMessage('Employii Geocoding Exception', {extra: data});
              $scope.geolocationError = 'Unable to complete your geocoding request at this time.  We are aware of' +
                ' this issue but please keep reference number ' + Raven.lastEventId + ' for future reference.';
              break;
            case google.maps.GeocoderStatus.REQUEST_DENIED:
              data.msg = 'Request denied by Google';
              $raven.captureMessage('Employii Geocoding Exception', {extra: data});
              $scope.geolocationError = 'Address lookup request was denied by Google.' +
                '  Please contact Employii with incident value: ' + Raven.lastEventId();
              break;
            case google.maps.GeocoderStatus.INVALID_REQUEST:
              data.msg = 'Invalid request received by google.';
              data.address = GoogleMaps.getStringAddress();
              $scope.geolocationError = 'Address lookup failed, an address must be present.';
              break;
            default:
              data.status = status;
              $raven.captureMessage('Unknown Geocoding Error:', {extra: data});
              break;
          }
          if (!GoogleMaps.hasMap()) {
            GoogleMaps.createMap(document.getElementById('maps_canvas'));
          }
        }
      };

      /**
       * @ngdoc method
       * @name  GeolocationController#updateAddressLatLong
       * @param  {google.maps.LatLng} latLong - The latitude / longitude literal for the address location.
       * @description
       * **INTERNAL**
       *
       * Patches the expected address with the updated latitude and longitude information.
       */
      function updateAddressLatLong(latLong) {
        var data = {latitude: latLong.lat(), longitude: latLong.lng()};
        return self.watchCall(rest.addresses.patch(GoogleMaps.getAddressInfo().id, data), rest.addresses.route);
      }

      /**
       * @ngdoc method
       * @name  GeolocationController#updateLocation
       * @description
       * Retrieves the latitude and longitude of the current `$scope.address` as reported by the
       * Google Maps Geocoding service, centers the map on that position and moves the marker there.
       */
      function updateLocation() {
        var address = GoogleMaps.getStringAddress();
        if (!address || address.length === 0) {
          // No address loaded from the service yet, wait 1/4 second and try again.
          $timeout(updateLocation, 250);
          return;
        }
        GoogleMaps.geolocateAddress(address, $scope.geolocationCallback);
      }

      /**
       * @ngdoc method
       * @name  GeolocationController#initialize
       * @description
       * The callback function to finish setting up the map options once the Google Maps Javascript API
       * has been loaded.  This function will be called from the window callback function and update the
       * map location to be that of the current `$scope.address`.
       */
      $scope.initialize = function() {
        GoogleMaps.setType($state.current.name);
        GoogleMaps.setAddressId($stateParams.id);
        GoogleMaps.createMap(document.getElementById('map_canvas'));
        GoogleMaps.loadAddress(updateLocation);
      };

      $scope.updateLocation = function() {
        updateLocation();
      };

      $scope.saveLocation = function() {
        if (GoogleMaps.hasMarker()) {
          var latLong = GoogleMaps.getMarker().getPosition();
          updateAddressLatLong(latLong).then(function() {
            utils.successMsg('Successfully updated location via map.');
            GoogleMaps.returnToPrevious($stateParams);
          }, function() {
            utils.errorMsg('There was an error while updating the location via map.');
          });
        } else {
          utils.noticeMsg('There is no marker currently set.');
        }
      };

      /**
       * @ngdoc method
       * @name  GeolocationController#cancel
       * @description
       * Aborts the geolocation procedure and moves the user back to their original path.
       */
      $scope.cancel = function() {
        GoogleMaps.returnToPrevious($stateParams);
      };

      $scope.$on('$destroy', function() {GoogleMaps.removeMap();$scope.initialize = null;});
      // Kick everything off once the Google Maps JavaScript API has been loaded.
      GoogleMaps.addCallback('mapsCallbackFunction', $scope.initialize);
    }]);
})();
