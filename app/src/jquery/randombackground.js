(function($) {
  'use strict';

  /**
   * Toggles a background class on the calling jQuery HTML element to be on.  This class
   * should have the appropriate settings for an element with a background (e.g. media queries, etc.).
   *
   * The default options assume that there are 10 background classes, named bg1 through bg10, which can
   * be toggled on the calling elements. Any of these options may be changed by passing a JSON object
   * with the appropriate keys into the function.  E.g. to change the max and backgroundPrefix values,
   * pass in:  {max: 5, backgroundPrefix: 'newBG'}.
   * @param  {Object} options - A JSON object containing 0 or more of min, max and backgroundPrefix.
   * @return {jQuery Element}   The modified calling jQuery element.
   */
  $.fn.randomizeElementBackgrounds = function(options) {
    var settings = _.extend({
      min: 1,
      max: 10,
      backgroundPrefix: 'bg'
    }, options);
    return this.each(function() {
      var imgId = Math.floor(Math.random() * ((settings.max + 1) - settings.min)) + settings.min;
      $(this).toggleClass(settings.backgroundPrefix + imgId);
    });
  };
  /* global jQuery */
})(jQuery);
