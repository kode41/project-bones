(function() {
  'use strict';
  /* global k41CalendarCallback */
  var mod = angular.module('k41-calendar');

  /**
   * @ngdoc service
   * @name calendar
   * @module k41-calendar
   * @return {calendar} - The calendar service which can be used to create event sources and configure calendars.
   */
  mod.factory('calendar', ['$log', '$q', '$rootScope',
    function($log, $q, $rootScope) {
    var service = {};

    service.createCalendarCallback = function(doc, callback) {
      if (doc) {
        window['k41CalendarCallback'] = function() {
          callback();
        };

        doc.ready(function() {
          setTimeout(k41CalendarCallback, 1000);
        });
      } else {
        $log.warn('No document received, not creating callback.');
      }
    };

    /**
     * @ngdoc noOpFunction
     * @name  calendar#noOpFunction
     * @param  {moment}   start    The moment for the start of the display for the calendar.
     * @param  {moment}   end      The moment for the end of the display for the calendar.
     * @param  {string}   timezone The string describing the current timezone.
     * @param  {Function} callback The function to call with an array of new Event objects.
     * @description
     * The `noOpFunction` will simply log that it is being called and return an empty array.
     */
    service.noOpFunction = function(start, end, timezone, callback) {
      $log.debug('calendar.noOpFunction(', start, ',', end, ',', timezone, ',', callback, ')');
      callback([]);
    };

    /**
     * @ngdoc method
     * @name  calendar#notifyCalendarDataLoaded
     * @param  {Object} data  The data which has been loaded for the calendar
     * @param  {String} event The event name for loaded calendar data
     * @return {Object}       The data which has been loaded.
     * @description
     * The `notifyCalendarDataLoaded` method must be called at the end of any data loading chain
     * in order to notify calendar views that data is ready to be displayed.
     */
    service.notifyCalendarDataLoaded = function(data, event) {
      $rootScope.$emit('calendarDataLoaded', event);
      return data;
    };

    /**
     * @ngdoc method
     * @name  calendar#createCalendarConfig
     * @param  {Array} eventFunctions An array of `fullCalendar` event sources.
     * @return {Dictionary}                A configuration dictionary for the fullCalendar instance.
     */
    service.createCalendarConfig = function(eventFunctions) {
      var config = {
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        selectOverlap: false,
        selectHelper: true,
        editable: true,
        eventLimit: true,
        eventSources: eventFunctions,
        buttonText: {
          today:    'Today',
          month:    'Month',
          week:     'Week',
          day:      'Day'
        }
      };
      return config;
    };

    return service;
  }]);
})();
