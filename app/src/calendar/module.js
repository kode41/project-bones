(function() {
  'use strict';
  /**
   * @ngdoc overview
   * @name  emCalendar
   * @description
   * The `k41-calendar` module contains controller and service implementations necessary to
   * utilize the full calendar module in the Kode41 web application.
   */
  angular.module('k41-calendar', []);
})();
