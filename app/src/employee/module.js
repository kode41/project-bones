'use strict';

/**
 * @ngdoc module
 * @name emEmployee
 * @description
 * The `k41-employee` module provides code for employee level users.
 */
angular.module('k41-employee', []);
