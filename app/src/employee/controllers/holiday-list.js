(function() {
  'use strict';
  var empMod = angular.module('k41-employee');
  empMod.controller('EmployeeHolidayController',
    ['$scope', 'rest', 'collections', '$rootScope', 'errorManager', '$controller',
    function($scope, rest, collections, $rootScope, errorManager, $controller) {
      var self = this;
      angular.extend(this, $controller('ErrorWatchingController',
        {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

      self.companyHolidays = [];
      var response = [
        {
          id:1,
          enabled:true,
          date:'01-01',
          name:'New Years Day'
        },
        {
          id:2,
          enabled:true,
          date:'01-18',
          name:'Martin Luther King Day'
        },
        {
          id:3,
          enabled:true,
          date:'02-15',
          name:'Presidents\' Day'
        },
        {
          id:4,
          enabled:true,
          date:'03-17',
          name:'St. Patrick\'s Day'
        },
        {
          id:5,
          enabled:true,
          date:'03-25',
          name:'Good Friday'
        },
        {
          id:6,
          enabled:true,
          date:'03-27',
          name:'Easter'
        },
        {
          id:7,
          enabled:true,
          date:'05-30',
          name:'Memorial Day'
        },
        {
          id:8,
          enabled:true,
          date:'07-04',
          name:'Independence Day'
        },
        {
          id:9,
          enabled:true,
          date:null,
          name:'Independence Day Observed'
        },
        {
          id:10,
          enabled:true,
          date:'09-05',
          name:'Labor Day'
        },
        {
          id:11,
          enabled:true,
          date:'10-12',
          name:'Yom Kippur'
        },
        {
          id:12,
          enabled:true,
          date:'10-10',
          name:'Columbus Day'
        },
        {
          id:13,
          enabled:true,
          date:'10-31',
          name:'Halloween'
        },
        {
          id:14,
          enabled:false,
          date:'11-11',
          name:'Veterans Day'
        },
        {
          id:15,
          enabled:true,
          date:null,
          name:'Veterans Day Observed'
        },
        {
          id:16,
          enabled:false,
          date:11 - 23,
          name:'Day Before Thanksgiving'
        },
        {
          id:17,
          enabled:false,
          date:'11-24',
          name:'Thanksgiving'
        },
        {
          id:18,
          enabled:false,
          date:'11-25',
          name:'Day After Thanksgiving'
        },
        {
          id:19,
          enabled:false,
          date:'12-24',
          name:'Christmas Eve'
        },
        {
          id:20,
          enabled:false,
          date:'12-25',
          name:'Christmas'
        },
         {
          id:21,
          enabled:true,
          date:'12-26',
          name:'Day After Christmas'
        },
        {
          id:22,
          enabled:true,
          date:'12-31',
          name:'New Years Eve'
        }
      ];
      collections.initPaging(self);

      self.noHolidaysChanged = function() {
        return !_.any(self.companyHolidays, 'changed');
      };
      self.save = function() {
        _.each(self.companyHolidays, function(holiday) {
          delete holiday.changed;
        });
      };
      self.companyHolidays = response;
      self.pageSize = 20;
      self.itemCount = self.companyHolidays.length;
    }]);
})();
