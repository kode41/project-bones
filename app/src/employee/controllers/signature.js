'use strict';

/**
 * @ngdoc controller
 * @name SignatureController
 * @module k41Employee
 * @description
 * A controller for the `Employee > Your Details > Signature` view.
 */
angular.module('k41-employee').controller('SignatureController',
['$scope', '$log', 'profile', 'utils', 'config',
function($scope, $log, profile, utils, config) {

  var user = $scope.user = profile.get().user;

  $scope.signature = _.extend({}, config.signature, {data: ''});
  $scope.isNew = $scope.isEditing = !user.signature_url;

  /**
   * @ngdoc method
   * @name SignatureController#save
   *
   * @param {ng.type:form.FormController} form - form containing signature info
   *
   * @description
   * If `form` is valid transforms signature into appropriate format and uploads it.
   * Uses polling to wait for the result of the upload.
   * Shows notification messages on both success and failure.
   */
  $scope.save = function(form) {
    $scope.submitted = true;

    if (form.$valid) {
      utils.successMsg('Signature saved successfully.');
      form.$setPristine();
    } else {
      utils.errorMsg('An error occurred saving the signature. Please try again');
    }
  };

  /**
   * @ngdoc method
   * @name SignatureController#reset
   *
   * @param {ng.type:form.FormController} form - form containing signature info
   *
   * @description
   * Sets signature data to empty string, `submitted` flag to false, and `form` to pristine.
   */
  $scope.reset = function(form) {
    $scope.signature.data = '';
    $scope.submitted = false;
    form.$setPristine();
  };

  /**
   * @ngdoc method
   * @name SignatureController#edit
   *
   * @description
   * Sets `isEditing` flag to true.
   */
  $scope.edit = function() {
    $scope.isEditing = true;
  };

  /**
   * @ngdoc method
   * @name SignatureController#cancel
   *
   * @param {ng.type:form.FormController} form - form containing signature info
   *
   * @description
   * Sets `isEditing` flag to false and resets `form`.
   */
  $scope.cancel = function(form) {
    $scope.isEditing = false;
    $scope.reset(form);
  };
}]);
