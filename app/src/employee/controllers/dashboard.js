'use strict';
/**
 * @ngdoc controller
 * @name DashboardController
 * @module k41Employee
 * @description
 * A controller for user's dashboard, both employee and admin side.
 */
/* global Raven */
angular.module('k41-employee').controller('DashboardController',
['$scope', '$q', '$timeout', '$state', 'profile', 'calendar', '$rootScope',
'errorManager', '$controller', '$modal', '$log',
function($scope, $q, $timeout, $state, profile, calendar, $rootScope,
  errorManager, $controller, $modal, $log) {
  var self = this;
  self.modalMethods = {};
  angular.extend(this, $controller('ErrorWatchingController',
    {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

  $scope.admin = !_.contains($state.current.data.userRoles, 'employee');

  // Tasks
  // -----

  // If we ever get to the point of adding customizable shortcut support,
  // this needs to be read from an API endpoint
  this.adminShortcuts = [
    {
      name: 'add-a-note',
      text: 'Add A Note',
      shortcut_type: 'modal',
      ref: 'addANote',
      slot: 4,
    },
    {
      name: 'add-employee',
      text: 'Add Employee',
      shortcut_type: 'link',
      ref: 'addEmployee',
      slot: 3,
    },
    {
      name: 'run-payroll',
      text: 'Run Payroll',
      shortcut_type: 'link',
      ref: 'payroll',
      slot: 1,
    },
    {
      name: 'run-a-report',
      text: 'Run A Report',
      shortcut_type: 'link',
      ref: 'payrollReports',
      slot: 2,
    },
  ];

  this.handleShortcut = function(sc) {
    if (!sc.shortcut_type) {
      $log.warn('No shortcut type for provided shortcut. Nothing will happen.');
      return;
    } else if (!sc.ref) {
      $log.warn('No reference for provided shortcut.  Nothing will happen.');
      return;
    }
    switch (sc.shortcut_type) {
      case 'link':
        $state.go(sc.ref);
        break;
      case 'modal':
        if (!this.modalMethods[sc.ref]) {
          $log.warn('Unknown modal reference.  Nothing will happen.');
        } else {
          this.modalMethods[sc.ref]();
        }
        break;
      default:
        $log.warn('Unknown shortcut type', sc.shortcut_type, 'doing nothing.');
    }
  };

  this.remainingTiles = {};

  // Notifications
  // -------------

  self.notifications = {};

  // Shortcuts
  // ---------
  this.modalMethods.addANote = function() {
    if (!$scope.admin) {
      Raven.captureMessage('Dashboard Controller - Non-admin attempting to add a note');
      return;
    }
    $modal.open({
      templateUrl: 'src/notes/note_modal.html',
      size: 'md',
      controller: 'DashboardNoteModalController'
    });
  };


  // Calendar
  // --------
  function loadCalendar() {
    $('#calendar').fullCalendar($scope.calendarConfig);
  }

  $scope.context = {isLoading: true, isEditing: false};

  this.isInAdminMode = function() {
    return $rootScope.adminMode;
  };

  this.eventClick = function(event) {
    switch (event.type) {
      case 'time_off_request':
        self.timeOffRequestClick(event);
        break;
      case 'pay_period:check_date':
        _.noop;
        break;
      case 'pay_period:due_date':
        _.noop;
        break;
    }
  };

  // Calls the loadCalendar function, and creates a new calendar object once the document is ready.
  calendar.createCalendarCallback($(document), loadCalendar);
  // calendarDataLoaded event gets fired every time one of the promises returned by the EventSourceFunctions
  // get resolved.

  var calendarDataLoaded = {
    timeOffRequest : false,
    payrollSchedule : false
  };

  $rootScope.$on('calendarDataLoaded', function(data, event) {
    calendarDataLoaded[event] = true;
    if (calendarDataLoaded.timeOffRequest && calendarDataLoaded.payrollSchedule) {
      $scope.context.isLoading = false;
    }
  });

  // $scope.calendarConfig = _.extend({},
  //   calendar.createCalendarConfig([
  //     calendar.getEventSourceFunction($scope.admin, 'pto', self, {approved: [true, null]}),
  //     calendar.getEventSourceFunction($scope.admin, 'payrollSchedule', self, {run_status: 'runnable'}),
  //   ]),
  //   {
  //     eventClick: self.eventClick
  //   });
}]);
