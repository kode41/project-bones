'use strict';

angular.module('k41-employee').controller('UserPasswordController',
['$scope', 'profile',
function($scope, profile) {

  $scope.user = profile.get().user;
  $scope.canEditAdminData = false;

  $scope.resetEditErrors = _.noop;
}]);
