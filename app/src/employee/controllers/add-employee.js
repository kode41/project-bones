'use strict';

angular.module('k41-admin')
.controller('AddEmployeeManualController',
['$scope', '$log', '$state', '$bootbox', 'rest', 'utils', 'profile', 'patterns', 'enums', 'collections',
'$controller', '$rootScope', 'errorManager',
function($scope, $log, $state, $bootbox, rest, utils, profile, patterns, enums, collections, $controller,
  $rootScope, errorManager) {
  var self = this;
  angular.extend(this, $controller('ErrorWatchingController',
    {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

  self.patterns = patterns;
  self.enums = enums;

  self.completedStep = 0;
  self.step = 1;
  self.totalSteps = 0;
  $scope.user = {
    first_name: 'Kode41',
    last_name: 'User',
    email: 'info@kode41.com',
    work_email: 'work@kode41.com',
    address: {street_address_1: '64 Arlington Place', city: 'Buffalo', zipcode: 14201, state: 'NY'}
  };
  self.computed = {};


  self.next = function(form) {
    self['submitted' + self.step] = true;

    if (!form || form.$valid) {
      if (self.step > self.completedStep) {
        self.completedStep = self.step;
      }
      self.step = self.step + 1;
    }
  };
  self.back = function() {
    self.step = self.step - 1;
  };

  self.skip = function() {
    self.step = self.step + 1;
  };

  self.view = function() {
    $state.go('users');
  };

  self.cancel = function() {
    $bootbox.dialog({
        title: 'Cancel Add User',
        message: '<p>A new user has already been created. Would you like to go back to the wizard, ' +
        'cancel without deleting the user or cancel and delete the user?</p>',
        buttons: {
          back: {
            label: 'Back',
            className: 'btn-default btn-lg',
            callback: function() {}
          },
          cancel: {
            label: 'Don\'t Delete',
            className: 'btn-default btn-lg',
            callback: function() {$state.go('users');}
          },
          success: {
            label: 'Yes Delete',
            className: 'btn-danger btn-lg',
            callback: function() {
              utils.successMsg('Successfully removed');
              $state.go('users');
            }
          }
        }
      });
  };

}]);
