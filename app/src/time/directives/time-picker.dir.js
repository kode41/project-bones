(function() {
  'use strict';

  angular.module('k41-time')
  .directive('k41Timepicker', function() {
    return {
      restrict: 'EA',
      require: ['k41Timepicker', '?^ngModel'],
      controller:'TimepickerController',
      replace: true,
      scope: {},
      templateUrl: 'src/time/directives/time-picker.html',
      link: function(scope, element, attrs, ctrls) {
        var timepickerCtrl = ctrls[0], ngModelCtrl = ctrls[1];

        if (ngModelCtrl) {
          timepickerCtrl.init(ngModelCtrl, element.find('input'));
        }
      }
    };
  });
})();
