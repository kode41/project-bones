'use strict';
/**
 * @ngdoc module
 * @name emTime
 * @description
 * The `k41-time` module provides functionality for utilizing time entries as part of web pages
 * in the Employii web application.
 */
angular.module('k41-time', []);
