(function() {
  'use strict';
  angular.module('k41-time')

  .constant('timepickerConfig', {
    hourStep: 1,
    minuteStep: 1,
    secondStep: 1,
    showMeridian: true,
    showSeconds: false,
    meridians: null,
    readonlyInput: false,
    mousewheel: true,
    arrowkeys: true,
    showSpinners: true
  })

  .controller('TimepickerController', ['$scope', '$attrs', '$parse', '$log', '$locale', 'timepickerConfig',
    function($scope, $attrs, $parse, $log, $locale, timepickerConfig) {
    var selected = new Date(),
        ngModelCtrl = {$setViewValue: angular.noop},
        meridians = angular.isDefined($attrs.meridians) ? $scope.$parent.$eval($attrs.meridians)
        : timepickerConfig.meridians || $locale.DATETIME_FORMATS.AMPMS;


    function getMinutesFromTemplate() {
      var minutes = parseInt($scope.minutes, 10);
      return (minutes >= 0 && minutes < 60) ? minutes : undefined;
    }

    function getSecondsFromTemplate() {
      var seconds = +$scope.seconds;
      return seconds >= 0 && seconds < 60 ? seconds : undefined;
    }

    this.init = function(ngModelCtrl_, inputs) {
      ngModelCtrl = ngModelCtrl_;
      ngModelCtrl.$render = this.render;

      ngModelCtrl.$formatters.unshift(function(modelValue) {
        return modelValue ? new Date(modelValue) : null;
      });

      var hoursInputEl = inputs.eq(0),
          minutesInputEl = inputs.eq(1),
          secondsInputEl = inputs.eq(2);

      var mousewheel = angular.isDefined($attrs.mousewheel) ?
      $scope.$parent.$eval($attrs.mousewheel) : timepickerConfig.mousewheel;
      if (mousewheel) {
        this.setupMousewheelEvents(hoursInputEl, minutesInputEl, secondsInputEl);
      }

      var arrowkeys = angular.isDefined($attrs.arrowkeys) ?
      $scope.$parent.$eval($attrs.arrowkeys) : timepickerConfig.arrowkeys;
      if (arrowkeys) {
        this.setupArrowkeyEvents(hoursInputEl, minutesInputEl, secondsInputEl);
      }

      $scope.readonlyInput = angular.isDefined($attrs.readonlyInput) ?
      $scope.$parent.$eval($attrs.readonlyInput) : timepickerConfig.readonlyInput;
      this.setupInputEvents(hoursInputEl, minutesInputEl, secondsInputEl);
    };

    var hourStep = timepickerConfig.hourStep;
    if ($attrs.hourStep) {
      $scope.$parent.$watch($parse($attrs.hourStep), function(value) {
        hourStep = parseInt(value, 10);
      });
    }

    var minuteStep = timepickerConfig.minuteStep;
    if ($attrs.minuteStep) {
      $scope.$parent.$watch($parse($attrs.minuteStep), function(value) {
        minuteStep = parseInt(value, 10);
      });
    }

    var secondStep = timepickerConfig.secondStep;
    if ($attrs.secondStep) {
      $scope.$parent.$watch($parse($attrs.secondStep), function(value) {
        secondStep = +value;
      });
    }

    $scope.showSeconds = timepickerConfig.showSeconds;
    if ($attrs.showSeconds) {
      $scope.$parent.$watch($parse($attrs.showSeconds), function(value) {
        $scope.showSeconds = !!value;
      });
    }

    // Get $scope.hours in 24H mode if valid
    function getHoursFromTemplate() {
      var hours = parseInt($scope.hours, 10);
      var valid = ($scope.showMeridian) ? (hours > 0 && hours < 13) : (hours >= 0 && hours < 24);
      if (!valid) {
        return undefined;
      }

      if ($scope.showMeridian) {
        if (hours === 12) {
          hours = 0;
        }
        if ($scope.meridian === meridians[1]) {
          hours = hours + 12;
        }
      }
      return hours;
    }

    // Call internally when we know that model is valid.
    function makeValid() {
      ngModelCtrl.$setValidity('time', true);
      $scope.invalidHours = false;
      $scope.invalidMinutes = false;
      $scope.invalidSeconds = false;
    }

    function pad(value) {
      return (angular.isDefined(value) && value.toString().length < 2) ? '0' + value : value.toString();
    }

    function updateTemplate(keyboardChange) {
      if (!ngModelCtrl.$modelValue) {
        $scope.hours = null;
        $scope.minutes = null;
        $scope.seconds = null;
        $scope.meridian = meridians[0];
      } else {
        var hours = selected.getHours(),
          minutes = selected.getMinutes(),
          seconds = selected.getSeconds();

        if ($scope.showMeridian) {
          hours = hours === 0 || hours === 12 ? 12 : hours % 12; // Convert 24 to 12 hour system
        }

        $scope.hours = keyboardChange === 'h' ? hours : pad(hours);
        if (keyboardChange !== 'm') {
          $scope.minutes = pad(minutes);
        }
        $scope.meridian = selected.getHours() < 12 ? meridians[0] : meridians[1];

        if (keyboardChange !== 's') {
          $scope.seconds = pad(seconds);
        }
        $scope.meridian = selected.getHours() < 12 ? meridians[0] : meridians[1];
      }
    }

    function refresh(keyboardChange) {
      makeValid();
      ngModelCtrl.$setViewValue(new Date(selected));
      updateTemplate(keyboardChange);
    }

    // 12H / 24H mode
    $scope.showMeridian = timepickerConfig.showMeridian;
    if ($attrs.showMeridian) {
      $scope.$parent.$watch($parse($attrs.showMeridian), function(value) {
        $scope.showMeridian = !!value;

        if (ngModelCtrl.$error.time) {
          // Evaluate from template
          var hours = getHoursFromTemplate(), minutes = getMinutesFromTemplate();
          if (angular.isDefined(hours) && angular.isDefined(minutes)) {
            selected.setHours(hours);
            refresh();
          }
        } else {
          updateTemplate();
        }
      });
    }

    // Respond on mousewheel spin
    this.setupMousewheelEvents = function(hoursInputEl, minutesInputEl, secondsInputEl) {
      var isScrollingUp = function(e) {
        if (e.originalEvent) {
          e = e.originalEvent;
        }
        //pick correct delta variable depending on event
        var delta = (e.wheelDelta) ? e.wheelDelta : -e.deltaY;
        return (e.detail || delta > 0);
      };

      hoursInputEl.bind('mousewheel wheel', function(e) {
        $scope.$apply((isScrollingUp(e)) ? $scope.incrementHours() : $scope.decrementHours());
        e.preventDefault();
      });

      minutesInputEl.bind('mousewheel wheel', function(e) {
        $scope.$apply((isScrollingUp(e)) ? $scope.incrementMinutes() : $scope.decrementMinutes());
        e.preventDefault();
      });

      secondsInputEl.bind('mousewheel wheel', function(e) {
        $scope.$apply(isScrollingUp(e) ? $scope.incrementSeconds() : $scope.decrementSeconds());
        e.preventDefault();
      });

    };

    // Respond on up/down arrowkeys
    this.setupArrowkeyEvents = function(hoursInputEl, minutesInputEl, secondsInputEl) {
      hoursInputEl.bind('keydown', function(e) {
        if (e.which === 38) { // up
          e.preventDefault();
          $scope.incrementHours();
          $scope.$apply();
        } else if (e.which === 40) { // down
          e.preventDefault();
          $scope.decrementHours();
          $scope.$apply();
        }
      });

      minutesInputEl.bind('keydown', function(e) {
        if (e.which === 38) {// up
          e.preventDefault();
          $scope.incrementMinutes();
          $scope.$apply();
        } else if (e.which === 40) {// down
          e.preventDefault();
          $scope.decrementMinutes();
          $scope.$apply();
        }
      });

      secondsInputEl.bind('keydown', function(e) {
        if (e.which === 38) {// up
          e.preventDefault();
          $scope.incrementSeconds();
          $scope.$apply();
        } else if (e.which === 40) {// down
          e.preventDefault();
          $scope.decrementSeconds();
          $scope.$apply();
        }
      });
    };

    this.setupInputEvents = function() {
      if ($scope.readonlyInput) {
        $scope.updateHours = angular.noop;
        $scope.updateMinutes = angular.noop;
        $scope.updateSeconds = angular.noop;
        return;
      }

      var invalidate = function(invalidHours, invalidMinutes, invalidSeconds) {
        ngModelCtrl.$setViewValue(null);
        ngModelCtrl.$setValidity('time', false);
        if (angular.isDefined(invalidHours)) {
          $scope.invalidHours = invalidHours;
        }
        if (angular.isDefined(invalidMinutes)) {
          $scope.invalidMinutes = invalidMinutes;
        }
        if (angular.isDefined(invalidSeconds)) {
          $scope.invalidSeconds = invalidSeconds;
        }
      };

      $scope.updateHours = function() {
        var hours = getHoursFromTemplate();

        ngModelCtrl.$setDirty();

        if (angular.isDefined(hours)) {
          selected.setHours(hours);
          refresh('h');
        } else {
          invalidate(true);
        }
      };

      $scope.updateMinutes = function() {
        var minutes = getMinutesFromTemplate();

        ngModelCtrl.$setDirty();

        if (angular.isDefined(minutes)) {
          selected.setMinutes(minutes);
          refresh('m');
        } else {
          invalidate(undefined, true);
        }
      };

      $scope.updateSeconds = function() {
        var seconds = getSecondsFromTemplate();

        ngModelCtrl.$setDirty();

        if (angular.isDefined(seconds)) {
          selected.setSeconds(seconds);
          refresh('s');
        } else {
          invalidate(undefined, undefined, true);
        }
      };

    };

    this.render = function() {
      var date = ngModelCtrl.$viewValue;

      if (isNaN(date)) {
        ngModelCtrl.$setValidity('time', false);
        $log.error('Employii Timepicker directive: "ng-model" value must be a Date object');
      } else {
        if (date) {
          selected = date;
        }

        makeValid();
        updateTemplate();
      }
    };

    function addSeconds(date, seconds) {
      var dt = new Date(date.getTime() + seconds * 1000);
      var newDate = new Date(date);
      newDate.setHours(dt.getHours(), dt.getMinutes(), dt.getSeconds());
      return newDate;
    }

    function addSecondsToSelected(seconds) {
      selected = addSeconds(selected, seconds);
      refresh();
    }

    $scope.showSpinners = angular.isDefined($attrs.showSpinners) ?
    $scope.$parent.$eval($attrs.showSpinners) : timepickerConfig.showSpinners;

    $scope.incrementHours = function() {
      addSecondsToSelected(hourStep * 60 * 60);
    };

    $scope.decrementHours = function() {
      addSecondsToSelected(-hourStep * 60 * 60);
    };

    $scope.incrementMinutes = function() {
      addSecondsToSelected(minuteStep * 60);
    };

    $scope.decrementMinutes = function() {
      addSecondsToSelected(-minuteStep * 60);
    };

    $scope.incrementSeconds = function() {
      addSecondsToSelected(secondStep);
    };

    $scope.decrementSeconds = function() {
      addSecondsToSelected(-secondStep);
    };

    $scope.toggleMeridian = function() {
      var minutes = getMinutesFromTemplate(),
          hours = getHoursFromTemplate();

      if (angular.isDefined(minutes) && angular.isDefined(hours)) {
        addSecondsToSelected(12 * 60 * (selected.getHours() < 12 ? 60 : -60));
      } else {
        $scope.meridian = $scope.meridian === meridians[0] ? meridians[1] : meridians[0];
      }
    };
    // 12H / 24H mode
    $scope.showMeridian = timepickerConfig.showMeridian;
    if ($attrs.showMeridian) {
      $scope.$parent.$watch($parse($attrs.showMeridian), function(value) {
        $scope.showMeridian = !!value;

        if (ngModelCtrl.$error.time) {
          // Evaluate from template
          var hours = getHoursFromTemplate(), minutes = getMinutesFromTemplate();
          if (angular.isDefined(hours) && angular.isDefined(minutes)) {
            selected.setHours(hours);
            refresh();
          }
        } else {
          updateTemplate();
        }
      });
    }
  }]);
})();
