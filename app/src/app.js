'use strict';

// mixin which 'deep freezes' an object to make it immutable
// defined as mixin, to make it available for use with constant modules
_.mixin({
  freeze: function(obj) {
    Object.freeze(obj);
    _.each(obj, function(prop) {
      if (typeof prop === 'object' && prop !== null && !Object.isFrozen(prop)) {
        _.freeze(prop);
      }
    });
    return obj;
  }
});


/**
 * @ngdoc module
 * @name Kode41App
 * @description
 * The main module of the K41Theme application.
 */
angular.module('K41ThemeApp', [
  'ngRaven',
  'toggle-switch',
  'ui.bootstrap',
  'xeditable',
  'emguo.poller',
  'theme.services',
  'theme.directives',
  'ngFileUpload',
  'theme.templates',
  'theme.template-overrides',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngAnimate',
  'restangular',
  'ui.router',
  'ui.calendar',
  'ngMaterial',
  // 'angular-highlightjs',
  'hljs',
  /*----------App Specific-----*/
  'k41-admin',
  'k41-bootstrap',
  'k41-calendar',
  'k41-employee',
  'k41-shared',
  'k41-templates',
  'k41-error-handling',
  'k41-maps',
  'k41-feature-handling',
  'k41-data-entry',
  'k41-navigation',
  'k41-settings',
  'k41-material',
  'k41-time'
])
/**
 * @ngdoc controller
 * @name MainController
 * @module Kode41App
 * @description
 * A controller for the main layout of the app - top bar, left bar, some other related stuff.
 */
.controller('MainController',
['$scope', '$global', '$timeout', 'errorManager', 'browser-detection', 'profile', '$state',
function($scope, $global, $timeout, errorManager, browserDetection, profile, $state) {

  function getUserInfo(user) {
    return _.pick(user, ['first_name', 'last_name', 'isLoggedIn', 'avatar_url', 'isAdmin', 'logo_url']);
  }

  $scope.userInfo = getUserInfo(profile.get().user);

  $scope.supportedBrowser = browserDetection.isBrowserSupported();
  if (!$scope.supportedBrowser) {
    $scope.browserDownloadLink = browserDetection.getBrowserUpdateLink();
    $scope.browserName = browserDetection.getBrowserName();
    $scope.browserVersion = browserDetection.getBrowserVersion();
    $scope.browserMinVersion = browserDetection.getMinimumSupportedVersion();
  }
  $scope.$on('profileChanged', errorManager.broadcastListener);
  $scope.$on('profileChanged', function(event, newProfile) {
    $scope.userInfo = getUserInfo(newProfile.user);
  });

  $scope.getUrl = function() {
    return 'assets/img/kode41.png';
  };

  $scope.style_fixedHeader = $global.get('fixedHeader');
  $scope.style_headerBarHidden = $global.get('headerBarHidden');
  $scope.style_layoutBoxed = $global.get('layoutBoxed');
  $scope.style_fullscreen = $global.get('fullscreen');
  $scope.style_leftbarCollapsed = $global.get('leftbarCollapsed');
  $scope.style_leftbarShown = $global.get('leftbarShown');
  $scope.style_rightbarCollapsed = $global.get('rightbarCollapsed');
  $scope.style_isSmallScreen = $global.get('isSmallScreen');
  $scope.style_showSearchCollapsed = $global.get('showSearchCollapsed');
  $scope.style_fullpage = $global.get('fullpage');

  $scope.removeMargin = function() {
    return $state.is('signup') || $state.is('login') || $state.is('resetPassword');
  };

  // $scope.hideSearchBar = function() {
  //   $global.set('showSearchCollapsed', false);
  // };

  // $scope.hideHeaderBar = function() {
  //   $global.set('headerBarHidden', true);
  // };

  // $scope.showHeaderBar = function($event) {
  //   $event.stopPropagation();
  //   $global.set('headerBarHidden', false);
  // };

  // $scope.toggleLeftBar = function() {
  //   if ($scope.style_isSmallScreen) {
  //     $global.set('leftbarShown', !$scope.style_leftbarShown);
  //     return;
  //   }
  //   $global.set('leftbarCollapsed', !$scope.style_leftbarCollapsed);
  // };

  $scope.toggleRightBar = function() {
    $global.set('rightbarCollapsed', !$scope.style_rightbarCollapsed);
  };

  $scope.$on('globalStyles:changed', function(event, newVal) {
    $scope['style_' + newVal.key] = newVal.value;
  });

  $scope.$on('globalStyles:maxWidth767', function(event, newVal) {
    console.log('Creating timeout function');
    $timeout(function() {
      $scope.style_isSmallScreen = newVal;
      if (!newVal) {
        $global.set('leftbarShown', false);
      } else {
        $global.set('leftbarCollapsed', false);
      }
    });
  });

  //update this work with the mode switch
  $scope.rightbarAccordionsShowOne = false;
  $scope.rightbarAccordions = [
    {open: true}, {open: true}, {open: true}, {open: true},
    {open: true}, {open: true}, {open: true}
  ];

  $scope.currentYear = new Date().getFullYear();
}])
.config(['RestangularProvider', '$stateProvider', '$urlRouterProvider', 'siteMap', 'config', 'hljsServiceProvider',
function(RestangularProvider, $stateProvider, $urlRouterProvider, siteMap, config, hljsServiceProvider) {

  hljsServiceProvider.setOptions({tabReplace: '  '});
  // initialize base URL for API access
  RestangularProvider.setBaseUrl(config.api.host + config.api.path);
  RestangularProvider.setRequestSuffix('/');
  RestangularProvider.setRequestInterceptor(function(element, operation) {
    return operation !== 'remove' ? element : null;
  });

  //default
  $urlRouterProvider.otherwise('/login');

  // the abstract main state ensures a resolved user profile and main view template
  $stateProvider.state('main', {
    'abstract': true,
    url: '',
    templateUrl: 'src/main.html',
    data: {userRoles: []},
    resolve: {
      Profile: ['profile', function(profile) { return profile.update(); }]
    }
  });

  _.each(siteMap, function(view) {
    $stateProvider.state(view.state, _.extend({parent: 'main'}, _.omit(view, 'state')));
  });
}])
.config(['pollerConfig', function(pollerConfig) {
  pollerConfig.stopOnStateChange = true;
  pollerConfig.stopOnRouteChange = true;
}])
.run(['editableThemes', 'editableOptions', function(editableThemes, editableOptions) {
  editableOptions.blurElem = 'ignore';

  editableThemes['default'].formTpl = '<form class="editable-wrap" novalidate></form>';

  editableThemes['default'].submitTpl =
    '<button type="submit" class="editable-buttons-submit"><i class="fa fa-check fa-fw"></i></button>';

  editableThemes['default'].cancelTpl =
    '<button type="button" class="editable-buttons-cancel" ng-click="$form.$cancel()">' +
    '<i class="fa fa-times fa-fw"></i></button>';
}])
.run([
  '$rootScope', '$state', '$window', 'session', 'profile', 'progressLoader',
  'utils', 'polling', 'errorManager', 'feature-state', 'config', 'paginationConfig',
  function($rootScope, $state, $window, session, profile, progressLoader,
           utils, polling, errorManager, featureState, config , paginationConfig) {
    paginationConfig.maxSize = config.paginationSizeLimit;
    $rootScope.featureDisabledState = featureState.getFeatureStates();

    $rootScope.logOut = function(keepState) {
      polling.clear();
      profile.logOut();
      errorManager.clearUser();
      if (!keepState) {
        $state.go('login');
      }
    };

    $rootScope.logOutIfUnauthorized = function(response) {
      if (response.statusText === 'UNAUTHORIZED' || response.status === 401) {
        $rootScope.logOut();
        return true;
      }
      return false;
    };

    angular.element($window).on('storage', function(event) {
      var storageEvent = event.originalEvent || {};
      if (!session.get().loggedIn && storageEvent.key === config.localStorageKey &&
          storageEvent.oldValue !== storageEvent.newValue) {
        $rootScope.logOut();
      }
    });

    $rootScope.$on('$stateChangeStart', function() {
      progressLoader.start();
      progressLoader.set(50);
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState) {
      var p = profile.get();

      //check for role and options
      if (!profile.hasRoleAccess(toState) || !utils.stateHasOptions(toState, p)) {
        event.preventDefault();
        $state.go(fromState.name || 'login');
        return;
      }

      progressLoader.end();

      // update timestamp to keep session alive
      session.ping();
    });

    $rootScope.$on('$stateChangeError', progressLoader.end);
  }
]);
