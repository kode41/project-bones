#Project Bones #

This is a custom Angular theme built using Bootstrap 3 and Material Design.


For local development:

* The server is Node.js [NVM][6]
* The package managers are npm and Bower [Bower][7]
* The task runner is Gulp [Gulp][8]
* The unit test runner is karma
* The unit test framework is mocha with chai and chai-as-promised plugins


#### Steps to set up once your dev environment is ready:

- Clone Repo
- Run npm
```sh
npm install
```
- Run bower
```sh
bower install
```
- Start Local Server
```sh
gulp serve
```


#### Deployment:

Run
```sh
gulp build
```

Builds the app into `dist` directory. Performs following steps:

- converts HTML into Angular templates (except `index.html`)
- generates and minifies CSS, both vendor and app ones
- copies some JS to support older IE
- concatenates and minifies JS, both vendor and app ones
- copies all related files to `dist`
- adds hash prefix to some assets (CSS, JS)
- updates `index.html` to use correct paths for built files

The final structure should be similar to this:
```
.
└── dist
    ├── assets
    │   ├── css
    │   │   └── ...
    │   ├── fonts
    │   │   └── ...
    │   └── img
    │       └── ...
    ├── scripts
    │   ├── ie8.js
    │   ├── scripts.js
    │   └── vendor.js
    ├── index.html
    └── ...
```

#### Testing Information

##### Organization:

Tests are located under the test folder to ensure
their separation from deployable code.  The test directory
contains unit, end-to-end (e2e) and integration tests, with the test specs
separated into the appropriate folder (unit or integration).

The folder contents should be as follows:


|Folder Prefix|Type of Test|Notes|
--------------|------------|-----|
|test/unit|Unit Tests|<ul><li>Tests one unit of code (e.g. a single controller, service).</li><li>Greybox testing as much as possible (mostly black box but some code inspection permitted to find edge cases)</li><li>Extensive use of mocks</li><li>NO hitting outside servers</li></ul>|
|test/e2e|End To End Tests|<ul><li>Tests entire system from front-end to back-end</li><li>NO mocking permitted</li><li>Should be black box tests based on expected use case results.</li></ul>|

The unit test folder is structured to mirror the app/src/ folder
and allow developers to quickly find the individual test
specifications.  The e2e and integration test folder structure is intended
to allow easy categorization of the specifications according to use
cases (e.g. _Authentication of an employee_,
_An administrator adds a user_, etc.).

The final structure of the test folder layout should be similar to this:
```
.
└── test
    ├── unit
    │   ├── mocks
    │   │   └── ...
    │   ├── shared
    │   │   ├── controllers
    │   │   |   ├── login.spec.js
    │   │   |   └── ...
    │   │   ├── services
    │   │   |   ├── profile.spec.js
    │   │   |   └── ...
    │   └── app.spec.js
    ├── karma.conf.js
    ├── e2e
    │   ├── authentication
    |   |   ├── employee
    │   │   |   └── ...
    |   ├── administration
    |   |   ├── user_management
    │   │   |   ├── add_user.spec.js
    │   │   |   └── ...
    │   │   └── ...
    │   └── ...
```

In general, e2e and integration test specs should be almost, if not entirely,
interchangeable depending on how mocking is accomplished in the integration test case.

##### Framework(s):

###### Unit Testing:

Unit testing is performed with the karma test runner using the [Mocha][1] with [Chai][2]
testing framework.  To facilitate the testing of promises, [Chai As Promised][3] is used,
along with [Sinon][4] for method spying.

###### End-To-End Testing:

End-to-end testing is performed with the [Protractor][5] test runner
using the the testing framework [Jasmine][9].


##### Style:

###### Unit Testing:

The Mocha + Chai frameworks permit the use of _behaviour driven development_ (BDD) and
_test driven development_ (TDD) unit testing styles.  Our tests are written using
the BDD "should" style to facilitate easy translation from use case to test case.

For example, the use case:

> When a user has entered login data and presses the reset button, the login data should
> be cleared.

Can be written as follows (assuming an empty object indicates no login data):

```javascript
// Data entered previously
scope.reset();
scope.userLogin.should.eql({});
```

To see more, read the ChaiJS [style guide](http://chaijs.com/guide/styles/#should).

###### End-To-End Testing:

In order to ensure proper test coverage for use cases, the e2e tests should use the
BDD "should" style of assertions to facilitate the easy translation to normal language.

In addition, for e2e testing, there must be no use of mocks as these are intended
to be a complete test of use cases from UI to server.

###### Integration Testing:

The integration tests, like unit and e2e, should make use of the BDD "should" style
of assertions to facilitate the easy translation into normal language.

In addition, for integration tests in the front-end, there should be mocked server responses
but no internal mocking of front-end components.  This is to provide a quick, CI friendly
way to fully test the UI, but avoid un-necessary server hits and overloading.

##### Execution and Reporting:

###### Unit Testing:

The unit tests will execute as part of the normal build cycle.  The reports
generated as part of the test cycle will be placed in `buildreports/unit_tests/`
for the unit test pass/fail/skip report and `buildreports/coverage/<browser>/`
for the coverage reports depending what browser(s) karma is configured to execute
as.

If it is desired to do a build up to the end of the unit test cycle only, the
command is:

```sh
gulp test
```

###### End-To-End Testing:

The e2e tests will **NOT** execute as part of the normal build cycle to keep build times minimized.

The e2e tests can be run against the different staging environments with the following commands:

|Environment | Endpoint Host | Build Command|
|------------|----------------|--------------|
| Production  |producition host | **not available... redirects to dev** |
| Staging (**default**) | staging host | `gulp e2e --staging` (or `grunt e2e`)|
| Dev | localhost:9000 | `gulp e2e - dev` |

This will build the appropriate config files into dist/ and then start the HTTP server from the dist/ folder.

###### Integration Testing:

_TBD_

##### Caveats and Footnotes:

###### Unit Testing:

- **$httpBackend Mock**: When you are testing with the $httpBackend mock, it is advisable to *not* use the
    $httpBackend.verifyNoOutstandingRequests() method, as our system has some calls which are triggered
    off of certain data being populated and this may lead to random test failures if timings change slightly.

- **Initial module import**: *DO NOT* do a beforeEach(module('Kode41App')) at the start of your unit test.  This *WILL*
    cause a timing dependent false negative to appear randomly and at the worst possible time.  Instead, import the
    module you are testing in, and a comma seperated list of dependencies for it.

- **$globalProvider Not Found**: If you follow the step above and get an error message stating that $globalProvider
    is not found (or something like that), ensure that _ui.bootstrap_ and _theme.services_ are in your module import
    list.

###### End-To-End Testing:

Due to the way in which protractor creates promises (it uses the webdriver promises), this causes a compatability
issue with [Chai][2] for the use of BDD should semantics.  As such, a global function is provided for
protractor e2e testing which allows the use of these promises.  For example:

```javascript
  browser.getCurrentUrl().should.eventually.equal('http://localhost:9000/#/login');
```

becomes:

```javascript
  aPromiseFor(browser.getCurrentUrl()).should.eventually.equal('http://localhost:9000/#/login');
```

[1]: http://mochajs.org "Mocha Homepage"
[2]: http://chaijs.com "Chai Homepage"
[3]: http://chaijs.com/plugins/chai-as-promised "Chai As Promised"
[4]: http://sinonjs.org "Sinon"
[5]: https://angular.github.io/protractor/ "Protractor"
[6]: https://www.digitalocean.com/community/tutorials/how-to-install-node-js-with-nvm-node-version-manager-on-a-vps "NVM"
[7]: http://bower.io/ "Bower"
[8]: http://gulpjs.com/ "Gulp"
[9]: http://jasmine.github.io/ "Jasmine"